# -*- coding: utf-8 -*-
"""
Created on Tue Feb 12 15:24:46 2019
This script parses the NAMD log files for the SMD and TCL values.
It writes this output into two .csv files


@author: Colin
"""

import re

# Extract names of the logfile with smd and tcl data
basename = 'mmcdh23EC17-25_0CA_smd'
begind   = 7
endind   = 7
letterind= ''

# Setup headers for the two smd and tcl text files
smdlines = ['SMD  Timestep COMx(A) COMy COMz Fx(pN) Fy Fz\n']
tcllines = ['TCL Timestep COMx(A) COMy COMz Fx(69.479pN)\n']

# Extract the data
for k in range(begind,endind+1):
    file = None
    if k <= 9:
        file = open(basename + '0' + str(k) + letterind + '.log','r')
    else:
        file = open(basename + str(k) + letterind + '.log','r')
    # Extract the smd and tcl lines
    lines = None
    lines = file.readlines()
    for line in lines:
        # Check for SMD
        check = None
        check = re.match(r'SMD\s*[^a-zA-Z\s]',line)
        if check != None:
            smdlines.append(line)
        else:
        # Check for TCL    
            check = None
            check = re.match(r'TCL\:\s*[^a-zA-Z\s]',line)
            if check != None:
                tcllines.append(line)
    file.close()

# Open and write the new files    
smdfile = open(basename + letterind + '.csv','w')
tclfile = open(basename[0:-3] + 'tcl' + letterind + '.csv','w')        

smdfile.writelines(smdlines)
smdfile.close()

tclfile.writelines(tcllines)
tclfile.close()