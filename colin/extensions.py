# -*- coding: utf-8 -*-
"""
Created on Mon May 11 15:54:21 2020

@author: Ismahene
"""

from prody import *
import numpy as np
from parse_SMD_log import *
import matplotlib.pyplot as plt
import scipy.signal as signal


def extension_domain(dcd_file, structure, ts, f, coord1, coord2, **kwargs):
    """
    parameters:
        - DCD file of trajectory: it is the file that contains all atom positions at each time step
        - PDB file of structure: downloadale from PDB (Protein Data Bank)
        - Log file of SMD simulation
        - coord1 (string parameter) is the start coordinate of the selection (e.g. start of domain )
        - coord2 (string parameter) is the end coordinate of the selection (e.g end of domain)
        
    returns:
        This function returns 3 parameters:
            - e2e (type: numpy array) : extension from element to element in nanometer.
            - timeInS: time in seconds
            -f: force in pN
    """
    
    #extract force and time in seconds from log file
   # e,df=extract_info_from_smd_log(log_file) 
    
    dcd1 = kwargs.get('additional dcd file 1', None)
    dcd2 = kwargs.get('additional dcd file 2', None)
    dcd3 = kwargs.get('additional dcd file 3', None)
    dcd4 = kwargs.get('additional dcd file 4', None)

    
    # f=df['F']
    # ts=df['timeInS']

    #load dcd file of trajectory
    traj = Trajectory(dcd_file) 

    #load the structure
   # structure = parsePDB(pdb_file) #load pdb file
    
   # alpha_carbons = structure.select('calpha')
  #  print(type(str(alpha_carbons.getResnums()[0])))

    #initialize the extension matrix
    e2e = np.zeros(traj.numFrames()) 
    
    #link pdb file of structure to dcd
    traj.link(structure)    
    
    if dcd1 != None :
        traj.add(dcd1)
        
    if dcd2 !=None : 
        traj.add(dcd2)

    if dcd3 !=None : 
        traj.add(dcd3)
        
    if dcd4 !=None : 
        traj.add(dcd4)
    
     #set coordinates of pdb to dcd
    traj.setCoords(structure)    
    coords=traj.getCoordsets()

    
    
    #get first coord of residue
    nter = structure.select('name CA and resnum '+coord1)   
    #get seconds coord of residue
    cter = structure.select('name CA and resnum '+coord2) 

    #loop over frames from dcd
    for i, frame in enumerate(traj): 
        e2e[i] = calcDistance(nter, cter) #compute euclidean distance between residues 


    
    n =  len(ts) // len(e2e)
    window = ( 1/ n) * np.ones(n,)
    timeInS = np.convolve(ts, window, mode='valid')[::n]
    
        
    n =  len(f) // len(e2e)
    window = ( 1/ n) * np.ones(n,)
    force = np.convolve(f, window, mode='valid')[::n]
    
    # timeInS= []

    # for i in range(0, len(ts), int(len(ts)/len(e2e))):
    #     timeInS.append(ts[i])

    # timeInS= pd.Series(timeInS)
    
    return e2e*10**-1 , timeInS , force , coords # return extension in nm



def e2e_PDB(pdb_file):
    """
    Parameters
    ----------
    pdb_file : pdb file of structure
        

    Returns
    -------
    start : int
        start coordinate.
    end : int
       end coordinate.

    """
    #load the structure
    structure = parsePDB(pdb_file) #load pdb file
    
    alpha_carbons = structure.select('calpha')
    
    start= str(alpha_carbons.getResnums()[0])
    
    end=  str(alpha_carbons.getResnums()[-1])


    return start, end, structure


import parse_SMD_log as pr
import matplotlib.pylab as plt




# log_file= '/home/mesbah/smd_software/smd_simulations/I27/smda/titinwbismd01.log'
# pdb_file='/home/mesbah/smd_software/smd_simulations/I27/titinwbi.pdb'
# dcd_file= "/home/mesbah/smd_software/smd_simulations/I27/smda/titinwbismd01.dcd"


# log_file="/home/mesbah/smd_software/smd_simulations/Cdh23EC1/smda/Cdh23EC1wbismd01.log"
# pdb_file="/home/mesbah/smd_software/smd_simulations/Cdh23EC1/Cdh23_EC1wbi.pdb"
# dcd_file="/home/mesbah/smd_software/smd_simulations/Cdh23EC1/smda/Cdh23EC1wbismd01.dcd"

# e,df =  pr.extract_info_from_smd_log(log_file)

# ts= df["timeStep"]
# f=df['F']

# start, end, structure= e2e_PDB(pdb_file)

# extension, TS, force=extension_domain(dcd_file, structure, ts, f, start, end)

# plt.plot(extension )

#plt.plot( extension)
#plt.plot(force*10**-2)


# log_file= '/home/mesbah/smd_software/smd_simulations/I27/smda/titinwbismd01.log'
# e,df =  pr.extract_info_from_smd_log(log_file)
# plt.plot(df['timeInS']*10**9, df['F'], label='I27')


# log_file='/home/mesbah/smd_software/smd_simulations/ICAM1-D4/smda/ICAM1_D4wbismd01.log'
# e,df =  pr.extract_info_from_smd_log(log_file)
# plt.plot(df['timeInS']*10**9, df['F'], label='ICAM1_D4', color='red')


# log_file='/home/mesbah/smd_software/smd_simulations/VCAM_v6/smda/VCAM1_D2wbismd01.log'
# e,df =  pr.extract_info_from_smd_log(log_file)
# plt.plot(df['timeInS']*10**9, df['F'], label='VCAM1_D2', color='green')


# log_file= "/home/mesbah/smd_software/smd_simulations/Cdh23EC1/smdb/Cdh23EC1wbismd01.log"
# e,df =  pr.extract_info_from_smd_log(log_file)
# plt.plot(df['timeInS']*10**9, df['F'], label='Cdh23_EC1', color="orange")


# plt.xlabel('Time (ns)')
# plt.ylabel('Force (pN)')
# plt.legend()
#plt.savefig('Cdh23EC1_1nm-ns.pdf')


# import matplotlib.pylab as plt
# import parse_SMD_log as pr

# #log`_file = "/home/mesbah/smd_software/mmcdh23EC17-25_3Ca/smda/mmcdh23EC17-25_3Ca_smd10.log"

# log_file= "/home/mesbah/smd_software/mmcdh23EC17-25_3Ca/smda/mergedlogs.log"

# #dcd="/home/mesbah/smd_software/mmcdh23EC17-25_3Ca/smda/mmCDH23EC17-25_3Ca_smd10_noh20.dcd"
# pdb_file= "/home/mesbah/smd_software/mmcdh23EC17-25_3Ca/smda/mmCDH23EC17-25_altlocA_autopsf_axisaligned_wbi_noh20.pdb"

# dcd= "/home/mesbah/smd_software/mmcdh23EC17-25_3Ca/smda/merged_dcd.dcd"


# # log_file= "/home/mesbah/smd_software/mmcdh23EC17-25_3Ca/smda/mergedlogs.log"
# # #dcd1= "/home/mesbah/smd_software/mmcdh23EC17-25_3Ca/smda/mmCDH23EC17-25_3Ca_smd11_noh20.dcd"


# # # dcd="/home/mesbah/smd_software/inputs/D1D5_1000mms_1_0-70ns_dry.dcd"
# # # pdb_file= '/home/mesbah/smd_software/inputs/D1D5_eq1_dry.pdb'
# # # log_file= "/home/mesbah/smd_software/inputs/D1D5_1000mms_1_0-70ns.log"


# e,df =  pr.extract_info_from_smd_log(log_file)


# start, end, structure= e2e_PDB(pdb_file)

# extension , ts , force= extension_domain(dcd, structure, df['timeInS'], df['F'], str(start), str(end))

    
# # n =  len(df['F']) // len(extension)
# # window = ( 1/ n) * np.ones(n,)
# # force = np.convolve(df['F'], window, mode='valid')[::n]
    
    
# #plt.plot( extension)
# plt.plot(extension, force)


# print(len(df['timeInS']))
# print(len(extension))


# print(len(df['timeInS']) / len(extension))


# n =  len(df['timeInS']) // len(extension)


# window = ( 1/ n) * np.ones(n,)
# print(window)
# res = np.convolve(df['timeInS'], window, mode='valid')[::n]

# #f= np.convolve(df['F'], window, mode='valid')[::n]


# print(res)

# print(len(res))


# plt.plot(res, extension)


# log_file2= "/home/mesbah/smd_software/scripts/all_logs.log"
# extension2 , ts1, f1 = extension_domain(dcd, structure, log_file2, start , end)



# print(len(ts))
# print(len(ts1))
# print(len(extension)*13.09)


# new_ts= []

# for i in range(0, len(ts1), int(len(ts1)/len(extension))):
#     new_ts.append(ts1[i])





# #plt.plot(ts, f)
# plt.plot(ts1, f1)

# plt.plot(new_ts[1:], extension)

# dcd= DCDFile(dcd)

# print(dcd.getTimestep)


#plt.plot(extension)


# =============================================================================
# 

# dcd_fidan="/home/mesbah/smd_software/inputs/D1D5_1000mms_1_0-70ns_dry.dcd"
# pdb_fidan= '/home/mesbah/smd_software/inputs/D1D5_eq1_dry.pdb'
# log_file= "/home/mesbah/smd_software/inputs/D1D5_1000mms_1_0-70ns.log"

# start, end, structure= e2e_PDB(pdb_fidan)


# extension , ts, f = extension_domain(dcd_fidan, structure, log_file, start, end)


# new_ts= []

# for i in range(0, len(ts), int(len(ts)/len(extension))):
#     new_ts.append(ts[i])

# l=len(extension)

# plt.plot (new_ts[:l], extension)
# plt.plot(ts, f*10**-1)

# =============================================================================


# plt.plot(ts, f)

def compute_derivative(e2e, ts):

    """"
    parameters:
        -  e2e (matrix) : extension from element to element in nanometer
        - ts (matrix): time in seconds
    
    
    returns derivative of extension (from element2element) and x-axis (time in seconds)
    """
    np.seterr(divide='ignore')
    factor=1
    l=len(e2e)
    ts=ts[0:l]
    derivative = np.zeros(l) 
    h=0.001

    for i in range(2,l-2):

        derivative[i]=(float(1*e2e[i-2]-8*e2e[i-1]+0*e2e[i+0]+8*e2e[i+1]-1*e2e[i+2])/float(1*12*h))
    x = (ts[:-1] + ts[1:]) / 2
    return x, derivative, ts, factor #returns :x-axis of derivative (timeinseconds), the derivative, original time in seconds



def get_heights_peaks(derivative):
    
    """
    Parameters
    ----------
    derivative : numpy.array
        The derivative of extension

    Returns
    -------
    heights : numpy.array
        Heights of all peaks of the derivative of extension
    """

    selected_peaks=[]
    index_=[]

    for i in range(len(derivative)):
        peaks, properties=signal.find_peaks(derivative[i], height=0)
    heights=sorted(list(properties['peak_heights']))

    return heights