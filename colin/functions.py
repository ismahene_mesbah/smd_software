# -*- coding: utf-8 -*-
"""
Created on Wed May 27 13:53:41 2020

@author: Ismahene
"""

import numpy as np
import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.linear_model import LinearRegression
from sklearn.pipeline import Pipeline
from parse_SMD_log import *  
import scipy.signal as signal
import numpy as np
import math
from scipy.stats import linregress
import time
import collections 


def get_RMS(data):
    """
    Parameters
    ----------
    data : array 
        array of data

    Returns
    -------
    RMS: Root mean square of data
    """
    rms = np.sqrt(np.mean(np.array(data)**2))

    return  rms

def downsampling(data):
    
    """
    Parameters
    ----------
    data : array
        array of data.

    Returns
    -------
    Panda data frame of downsampled data
        

    """
    data=np.array(data)
    l=len(data) #length of data
    new_sample=[]
    for i in range(0, l , 10):
        new_sample.append(data[i:i+10])
    new_sample=pd.DataFrame(new_sample)
    return new_sample[0]
    

def get_natural_cubic_spline_model(x, y, minval=None, maxval=None, n_knots=None, knots=None):
    """
    Get a natural cubic spline model for the data.

    For the knots, give (a) `knots` (as an array) or (b) minval, maxval and n_knots.

    If the knots are not directly specified, the resulting knots are equally
    space within the *interior* of (max, min).  That is, the endpoints are
    *not* included as knots.

    Parameters
    ----------
    x: np.array of float
        The input data
    y: np.array of float
        The outpur data
    minval: float 
        Minimum of interval containing the knots.
    maxval: float 
        Maximum of the interval containing the knots.
    n_knots: positive integer 
        The number of knots to create.
    knots: array or list of floats 
        The knots.

    Returns
    --------
    model: a model object
        The returned model will have following method:
        - predict(x):
            x is a numpy array. This will return the predicted y-values.
    """

    if knots:
        spline = NaturalCubicSpline(knots=knots)
    else:
        spline = NaturalCubicSpline(max=maxval, min=minval, n_knots=n_knots)

    p = Pipeline([
        ('nat_cubic', spline),
        ('regression', LinearRegression(fit_intercept=True))
    ])

    p.fit(x, y)

    return p


class AbstractSpline(BaseEstimator, TransformerMixin):
    """Base class for all spline basis expansions."""

    def __init__(self, max=None, min=None, n_knots=None, n_params=None, knots=None):
        if knots is None:
            if not n_knots:
                n_knots = self._compute_n_knots(n_params)
            knots = np.linspace(min, max, num=(n_knots + 2))[1:-1]
            max, min = np.max(knots), np.min(knots)
        self.knots = np.asarray(knots)

    @property
    def n_knots(self):
        return len(self.knots)

    def fit(self, *args, **kwargs):
        return self


class NaturalCubicSpline(AbstractSpline):
    """Apply a natural cubic basis expansion to an array.
    The features created with this basis expansion can be used to fit a
    piecewise cubic function under the constraint that the fitted curve is
    linear *outside* the range of the knots..  The fitted curve is continuously
    differentiable to the second order at all of the knots.
    This transformer can be created in two ways:
      - By specifying the maximum, minimum, and number of knots.
      - By specifying the cutpoints directly.  

    If the knots are not directly specified, the resulting knots are equally
    space within the *interior* of (max, min).  That is, the endpoints are
    *not* included as knots.
    Parameters
    ----------
    min: float 
        Minimum of interval containing the knots.
    max: float 
        Maximum of the interval containing the knots.
    n_knots: positive integer 
        The number of knots to create.
    knots: array or list of floats 
        The knots.
    """

    def _compute_n_knots(self, n_params):
        return n_params

    @property
    def n_params(self):
        return self.n_knots - 1

    def transform(self, X, **transform_params):
        X_spl = self._transform_array(X)
        if isinstance(X, pd.Series):
            col_names = self._make_names(X)
            X_spl = pd.DataFrame(X_spl, columns=col_names, index=X.index)
        return X_spl

    def _make_names(self, X):
        first_name = "{}_spline_linear".format(X.name)
        rest_names = ["{}_spline_{}".format(X.name, idx)
                      for idx in range(self.n_knots - 2)]
        return [first_name] + rest_names

    def _transform_array(self, X, **transform_params):
        X = X.squeeze()
        try:
            X_spl = np.zeros((X.shape[0], self.n_knots - 1))
        except IndexError: # For arrays with only one element
            X_spl = np.zeros((1, self.n_knots - 1))
        X_spl[:, 0] = X.squeeze()

        def d(knot_idx, x):
            def ppart(t): return np.maximum(0, t)

            def cube(t): return t*t*t
            numerator = (cube(ppart(x - self.knots[knot_idx]))
                         - cube(ppart(x - self.knots[self.n_knots - 1])))
            denominator = self.knots[self.n_knots - 1] - self.knots[knot_idx]
            return numerator / denominator

        for i in range(0, self.n_knots - 2):
            X_spl[:, i+1] = (d(i, X) - d(self.n_knots - 2, X)).squeeze()
        return X_spl




def find_peaks(f,ts, knots, **kwargs):
    """
    Parameters
    ----------
    ts : table of info extracted from log (type= pandas.df)
    knots : int
        The knots
    **kwargs : int
        The threshold of prominence.

    Returns
    -------
    df : data frame
        data frame of data.
    smooth : array of smooth data
        
    peaks:list
        list of all detected peaks
    prominences : array
        array of all prominences 

    """
    
   # energy_parms,df=extract_info_from_smd_log(input_file)
    prominence = kwargs.get('prominence', None)
    model = get_natural_cubic_spline_model(ts, f, minval=min(ts), maxval=max(ts), n_knots=knots)
    smooth = model.predict(ts)
    #peaks detection using find_peaks function from scipy signal library
    peaks=signal.find_peaks(smooth,prominence=prominence)[0] #using find peaks from signal
    prominences = signal.peak_prominences(smooth, peaks)[0]
    
    return smooth, list(peaks), prominences

def get_slopes(displacement, ts, f, peaks, distance):
    """
    Parameters
    ----------
    displacement : pd dataframe
        contains the displacement in nanometer.
    ts : pd dataframe
        time in seconds .
    f : pd dataframe
        force in pN.
    peaks : list
        list of peaks ( the indexes of peaks).
    distance : float
        distance in nanometer set in the entry of the interface by the user.

    Returns
    -------
    d : dict
        dictionnary of information.
    slope : list
        list containing the loading rates the slopes.
    intercept : list
        list containing the intercepts of the slopes.

    """

    displacement=np.array(displacement)
    d={'f':[], 'ts':[], 'dis1':[], 'disTMP':[], 'dis2':[], 'index_dis2':[], 'peaks':[]}
    #iterate over all peaks
    for i in peaks:
        d['f'].append(f[i])
        d['ts'].append(ts[i])
        #compute displacement using corresponding peak index
        d['dis1'].append(displacement[i])
    #compute selected value = displacement[peak_index]-distance given by the user in nm
    for i in range(len(peaks)):
        d['disTMP'].append(d['dis1'][i]-distance*1)

    #search for closest value to selected one (selected one is distance before peak)
    for i in range(len(peaks)):
        d['dis2'].append(displacement.flat[np.abs(displacement - d['disTMP'][i]).argmin()])
    #put found values in dictionnary of info
    for i in range(len(peaks)):
        d['index_dis2'].append(int(list(displacement).index(d['dis2'][i])))
        
    for i in peaks:
        d['peaks'].append(int(i))

    slope=[]
    intercept=[]
    
    #slopes and intercept computing
    for i in range(len(peaks)):
        slope.append(np.polyfit(np.array(ts[d['index_dis2'][i]:d['peaks'][i]]),np.array(f[d['index_dis2'][i]:d['peaks'][i]]), 1)[0])
        intercept.append(np.polyfit(np.array(ts[d['index_dis2'][i]:d['peaks'][i]]),np.array(f[d['index_dis2'][i]:d['peaks'][i]]), 1)[1])
    return d, slope, intercept


def slopes_at_peaks(displacement, ts, f, peaks, distance, timeStep):
    """
    

    Parameters
    ----------
    displacement : pd dataframe
        displacement in nanometer from the log file of SMD simulation
    ts : pd dataframe
        time in seconds from the log file of simulation.
    f : pd dataframe
        force in pN.
    peaks : list
        list containing all peaks.
    distance : float
        distance in nanometer (set by the user in the interface).
    timeStep : timesteps
         time steps from the log file of simultion.

    Returns
    -------
    d: pd dataframe
    dataframe of information, ready to export as csv

    """
    slopes=get_slopes(displacement, ts, f, peaks, distance)[1]

    d={'':[], 'timeStep': [], 'timeInS':[], 'UnfoldingForce[pN]':[], 'UnfoldingLoadingRate':'', 'UnfoldingDisplacement': [] }
    d['UnfoldingLoadingRate']=slopes
    for i in range(len(peaks)):
        d[''].append(str(i+1))
#COMPUTING DISTANCES
    d['UnfoldingForce[pN]']=np.array(f[peaks])

    d['UnfoldingDisplacement']=np.array(displacement[peaks])
    distances = [displacement[peaks[i]] - displacement[peaks[i-1]] for i in np.arange(1, len(peaks))] 
    d_dist={}
    d_dist['InterDomainDistance(um)']=distances
    d['timeStep']=np.array(timeStep[peaks])
    d['timeInS']=np.array(ts[peaks])
   # print(d)
    d = pd.DataFrame.from_dict(d)
    d.loc[:,'InterDomainDistance(um)'] = pd.Series(d_dist['InterDomainDistance(um)'])

    return d 


            
    
def compute_slopes_second_detection(input_file, peak_index_list, selection_list):
    """
    Parameters
    ----------
    input_file : log file of SMD simulation.
    
    peak_index_list :  list
        the indexes selected by the user.

    Returns
    -------
    - dico: data frame of information (timeStep, timeInS, UnfoldingForce[pN], UnfoldingLoadingRate, UnfoldingDisplacement)
    -slope: array : loading rates
    -intercept: array: intercept of slopes

    """
    energy_parms,df=extract_info_from_smd_log(input_file)
    peak_index_list=sorted(peak_index_list)
    velocity=df['velocityInums'][0]
    displacement=np.array(df['displacement'])
    f=df['F']
    timeStep=df['timeStep']
    #print(velocity)
    ts=df['timeInS']
    d={'timeInS': [], 'slopes':[], 'distance':[], 'disTMP':[],  'dis2':[], 'index_dis2':[], 'peaks':[], 'f':[], 'dis1':[]}
    for i in range(len(peak_index_list)):
        d['timeInS'].append(ts[peak_index_list[i]])
        d['f'].append(f[peak_index_list[i]])
        d['dis1'].append(displacement[peak_index_list[i]])
    for i in range(len(d['timeInS'])):
        d['distance'].append(d['timeInS'][i]* velocity) #distance is in um
        
    for i in range(len(peak_index_list)):
        d['disTMP'].append(d['dis1'][i]-d['distance'][i])

    #search for closest value to selected one (selected one is distance before peak)
    for i in range(len(peak_index_list)):
        d['dis2'].append(displacement.flat[np.abs(displacement - d['disTMP'][i]).argmin()])
    #put found values in dictionnary of info
    for i in range(len(peak_index_list)):
        d['index_dis2'].append(int(list(displacement).index(d['dis2'][i])))
        
    for i in peak_index_list:
        d['peaks'].append(int(i))

    slope=[]
    intercept=[]
    
    #slopes and intercept computing
    for i in range(len(peak_index_list)):
        slope.append(np.polyfit(np.array(ts[d['index_dis2'][i]:d['peaks'][i]]),np.array(f[d['index_dis2'][i]:d['peaks'][i]]), 1)[0])
        intercept.append(np.polyfit(np.array(ts[d['index_dis2'][i]:d['peaks'][i]]),np.array(f[d['index_dis2'][i]:d['peaks'][i]]), 1)[1])
        
    dico={'Unfolding':[], 'timeStep': [], 'timeInS':[], 'UnfoldingForce[pN]':[], 'UnfoldingLoadingRate[pNsec]':'', 'UnfoldingDisplacement[nm]': [], 'selection':[], 'UnfoldingNumber':[] }
    dico['UnfoldingLoadingRate[pNsec]']=slope
    keys=[]
    l=[]
    for i in range(len(peak_index_list)):
        dico['Unfolding'].append(str(i+1))
    for i in range(len(selection_list)):
        dico['selection'].append(selection_list[i])
        
    numerator = {i:0 for i in set(selection_list)}
    denominator = collections.Counter(selection_list)

  
    for v in selection_list:
        numerator[v] += 1
        dico['UnfoldingNumber'].append(str(numerator[v]) + '-' + str(denominator[v]))

    
#COMPUTING DISTANCES
    dico['UnfoldingForce[pN]']=np.array(f[peak_index_list])

    dico['UnfoldingDisplacement[nm]']=np.array(displacement[peak_index_list]*10**3)
    distances = [displacement[peak_index_list[i]] - displacement[peak_index_list[i-1]] for i in np.arange(1, len(peak_index_list))] 
    d_dist={}
    d_dist['InterDomainDistance(nm)']=distances
    dico['timeStep']=np.array(timeStep[peak_index_list])
    dico['timeInS']=np.array(ts[peak_index_list])
    dico = pd.DataFrame.from_dict(dico)
    dico.loc[:,'InterDomainDistance(nm)'] = pd.Series(d_dist['InterDomainDistance(nm)']*10**3)

    return dico , slope, intercept

def find_roots(x, y):
    """
    This function finds the intersection points between two 
    curves x and y

    Parameters
    ----------
    x : numpy matrix
        the first function ( all the points of the function).
    y : numpy matrix
        the second function (curve) .

    Returns
    -------
    roots
        numpy matrix containing roots .

    """
    s = np.abs(np.diff(np.sign(y))).astype(bool)
    return x[:-1][s] + np.diff(x)[s] / (np.abs(y[1:][s] / y[:-1][s]) + 1)