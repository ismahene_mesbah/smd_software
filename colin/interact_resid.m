function [CondExp,l2err,RESFIT,RESNAME] = interact_resid()
%Find residues pairs which best explain force rupture curve
%   Detailed explanation goes here

%% Initalize parameters and files
% dcd file loading
dcd = 'mmcdh23EC19-20_CASite1_noh20_snip4.dcd';

% Pulling data loading
%  File should be output from Fpeak.py parsing the log file
%  Assumed file ends with either smdx.csv or tclx.csv with x any symbol
plog = 'mmcdh23EC19-20_CASite1_smdc.csv';

% Residue atom lists
%  File should be output from preinteract_resid.tcl
res_list = 'Res_Atoms.csv';

% Number of atoms in system
natoms = 3736;

% Frame range under study
%  Should match the rupture peak in the extension curve being considered,
%   eg trace your mouse over the curve in VMD and look at TK output
%   Note: You are excepted to have loaded the under-question dcd into VMD 
%	  from the first to last frame with a stride fr_str.  Then you run 
%         your mouse over the rupture plots to find fr_beg and fr_end while
%	  within that same VMD session. These are numbers you enter into 
%	  below section. These are needed to match to the times printed in
%	  the smd/tcl csv files.
%   Note: Only trick is that you have to also use catdcd to truncate the dcd
%	  and for catdcd your inputs should beginning frame is fr_beg*fr_str, end
%	  frame is fr_end*fr_str, and the stride should be fr_str. Since VMD 
%	  printed frames that in VMD were separated by 1 but in dcd were
%	  separated by fr_str.
fr_str = 20;
fr_beg = 5636;
fr_end = 5964;

% Movie step sizes
%  Corresponds to per how many steps were the outputs saved
%  Pulling step size not needed because output by Fpeak.py in csv
step_dcd = 500;

% Cutoff distance for deciding if residues interact
cutoff = 12; % Angstroms

% Percentage mass of dist residue values to assign each bin for cond exp
mass = .05;

% Number of curves used for plotting fits against raw data
ncurves = 20;

% Shape of subplots (best to less in reading order)
sb_plt = [4 5];
%% Extract residue lists for indexing into dcd file
% Every row is a residue with end possible padded by NaN to account for
%  variable number of atoms in a residue
T_res = readtable(res_list);

nresid = size(T_res,1); % Matlab takes 1st row csv as header and not 
			% included in row count. No shift in res is 
			% is needed.
resid_sz = size(T_res,2)-1;

%% Extract and format dcd data
oldpath = addpath('matdcd-1.0\');
at_coor = readdcd(dcd,1:natoms);

% shift index for fr_beg translated to pos 1 and total number dcd frames
shift_fr = fr_beg - 1;
nfr = fr_end - shift_fr;

% Compute trajectories of residues' center of mass
%  Each row is residue matched to row of T_res and number of columns is
%  number of frames in movie.  Third index is the x,y,z coordinate
res_traj = zeros(nresid,nfr,3);

for j=nfr:-1:1
    % Reshape this frame so each column is an atomic position
    at_now = reshape(at_coor(j,:),...
                     3,length(at_coor(j,:))/3);
    for i=nresid:-1:1
        % Extract the positions associated to this residue
        pos_nan = find(isnan(T_res{i,2:end}),1);
        if isempty(pos_nan)
            pos_nan = resid_sz+1;
        end

        %  Shift position up 1 bc T table has a 1st column of row headers
        pos_nan = pos_nan+1;
       
        idx = T_res{i,2:pos_nan-1};
        
        %  Index into this array
        at_resid = at_now(:,idx);
        
        % Store into trajectory
        res_traj(i,j,:) = mean(at_resid,2);
    end
end

%% Compute distances for all residue pairings
% List all pairings
list1 = 1:nresid;
list2 = (1:nresid)';
list3 = reshape(1:nfr,1,1,nfr);
list1 = repmat(list1,nresid,1,nfr);
list2 = repmat(list2,1,nresid,nfr);
list3 = repmat(list3,nresid,nresid,1);

pairlist = [list1(:) list2(:) list3(:)];

% Loop over and compute distances
pos = 0;
distmat = zeros(nresid,nresid,nfr);
for i=1:length(pairlist)
        idi = pairlist(i,1);
        idj = pairlist(i,2);
        idk = pairlist(i,3);
        
        distmat(idi,idj,idk) = ...
                           norm(reshape(res_traj(idi,idk,:),3,1) - ...
                                reshape(res_traj(idj,idk,:),3,1));
    if i/length(pairlist) >= pos
            disp([num2str(100*pos) '% done with resid dist calculation ...'])
            pos = pos + .05;
    end
end

%% Extract Pulling Data
T_pull = readtable(plog);

% First column is time value in steps. Second is force in pN
if strcmp(plog(end-7:end-5),'smd')
    pull = [T_pull{:,2} T_pull{:,end-2}];
elseif strcmp(plog(end-7:end-5),'tcl')
    % Tcl forces must be rescaled to pN
    pull = [T_pull{:,2} T_pull{:,end}*69.479];
else
    error('The pulling forces were neither smd nor tcl');
end

%% Construct bins for cond exp based on all distmat data
bins = mybins(distmat(:),cutoff,mass);

%% Compute the cond exp Pull|ResDist for all residue pairs
% Do the common matching of pull to its movie frame time bins
[matchpull,wts] = matching_pull2dcd(pull,fr_beg,fr_end,fr_str,step_dcd);

% Truncate pairlist
%  Grab all pairs belonging to a same frame since that will grab
%  single cycle through (1:nresid)x(1:nresid)
flag_pairs = (pairlist(:,3) == 1);
pairlist = pairlist(flag_pairs,1:2);

l2err = zeros(nresid,nresid);
CondExp = cell(nresid,nresid);
pos = 0;
for i=1:length(pairlist)
        % Extract residue ids
        idi = pairlist(i,1);
        idj = pairlist(i,2);
        
        if idi >= idj
            % Store Cond Exp Structure
            CondExp{idi,idj} = NaN;
        
            % Compute l2 error
            l2err(idi,idj) = NaN;
        
            continue
        end
        
        distvec = distmat(idi,idj,:);
        
        % Match pulling forces with their residue distances
        match = matching(matchpull,wts,distvec);
        
        % Flag data points within movie time window
        flag_query = ~isnan(match(:,1));
        
        % Compute conditional expectation
        [Y,val] = mycondexp(match,pull,bins,match(flag_query,1));
        
        % Store Cond Exp Structure
        CondExp{idi,idj} = Y;
        
        % Compute l2 error
        l2err(idi,idj) = sqrt(sum(...
                               (val' - pull(flag_query,2)).^2 ) );  
                           
        if i/length(pairlist) >= pos
            disp([num2str(100*pos) '% done with cond exp ...'])
            pos = pos + .05;
        end
end

%% Order the residue pairs from most to least successful at explaining data
ram = l2err(:);
ram(isnan(ram)) = Inf;
[ram,id] = sort(ram);
[I,J] = ind2sub([nresid nresid],id);

RESFIT = [I J ram];
RESNAME = [T_res{I,1} repmat({' '},length(I),1) T_res{J,1}];
%% Plot most successful residues pairs for explaining data
F = figure;hold on %#ok<*NASGU>

% Plot most successful residue pairs for explaining data
for i=1:ncurves
    subplot(sb_plt(1),sb_plt(2),ind2sub(sb_plt,i));
    idi = RESFIT(i,1);
    idj = RESFIT(i,2);
    
    Y = CondExp{idi,idj};
    
    % Extract dist values between residues
    distvec = distmat(idi,idj,:);
    
    % Prepare the x-axis data
    xdata = linspace(0,1,100)';
    
    %  100 pt samples per bin stacked into consecutive columns
    %   Bc bins(end) = Inf replace Inf with max separation this 
    %   residue pair     
    xdata = [1-xdata xdata]*[bins(1:end-1);...
                             [bins(2:end-1) max(distvec(:))]];
    
    % Match pulling forces with their residue distances
    match = matching(matchpull,wts,distvec);
    flag_query = ~isnan(match(:,1));
    
    % Plot raw data
    plot(match(flag_query,1),pull(flag_query,2),'+','Tag','Pull_Data')
    hold on
    
    % Replicate condexp y data for all bin samples
    ydata = (Y).*(ones(size(xdata)));
    
    % Conclude plot
    plot(xdata(:),ydata(:),'Tag',['Best_' num2str(i)],'LineWidth',3);  
    title(['Best #' num2str(i) ':' ...
           T_res{idi,1}{1} ' ' ...
           T_res{idj,1}{1}]);
end

% Save the data needed to plot
save('interact_pltdat','xdata','bins','RESFIT','CondExp','distmat',...
     'matchpull','pull','wts','T_res','sb_plt','ncurves',...
     '-v7.3');
%% Write the results to CSV file
fid = fopen('Resid_Rank.csv','w');
fprintf(fid,'First_Resid,Second_Resid,RMS_err\n');
for i=1:length(RESFIT)
    fprintf(fid,[RESNAME{i,1} ',' RESNAME{i,3} ',%8.3f\n'],RESFIT(i,3));
end
fclose(fid);

%% Reset path
path(oldpath);
end

function bins = mybins(smp,cutoff,mass)
% Mk bins with >= mass % of 1d data in smp\cap[0,cutoff] with xtra lump bin
%   bins(1:end-1) is left end points of bins 
%   bins(2:end) is right end points of bins

%% Preprocess data
% Restrict smp to values under cutoff
smp = smp(smp <= cutoff);

% Initialize CDF with multiplicites
smp = smp(:);
X = sort(smp);
X = [ X';...
      1:length(X) ];
X(2,:) = X(2,:)/size(X,2);

% Trim down to a CDF with no multiplicities
[CDF,ia] = unique(X(1,:),'last');
CDF = [CDF;...
       X(2,ia)];

%% Construct the bins with fixed mass for cond exp
pos = mass;
bins = CDF(1,:);
bins(1) = NaN;
flag_break = false;
while true
    id = find(CDF(2,:) >= pos,1);
    bins(id) = NaN;
    if flag_break
        break;
    end
    
    pos = CDF(2,id)+mass;
    if pos > 1
        pos = 1;
        flag_break = true;
    end     
end

bins = CDF(1,isnan(bins));

%% Add final bin for lump summing all other data
bins = [bins Inf];
end

function [match,wts] = matching_pull2dcd(pull,fr_beg,fr_end,fr_str,step_dcd)
% Output dcd frame interval pull-force point belongs with lin interpolate wts
%  match gives the a-value index, of the [a,b) interval, in fr_beg:fr_end pt
%  belongs
%  pull is the main body's imported TCL/SMD table, 1st row timestep and 2nd
%   row is force value
%  Remaining params are explained in script main body at param initialization

% Setup time axis for distvec
%  In VMD you spanned (fr_beg:fr_end) with a stride of fr_str
%  within the dcd which was sampled at step_dcd intervals.
%  So multiply to get timestep count.
time = step_dcd*fr_str*(fr_beg:fr_end)';

wts = NaN(length(pull),2);
% Loop and match distvec 
for i=length(pull):-1:1
    id = find( ...
            ( pull(i,1) >= time(1:end-1) )&( pull(i,1) < time(2:end) ), ...
              1);
    if isempty(id)
        match(i,1) = NaN;
    else
        % Linearly interpolate the value
        s = ( pull(i,1)-time(id) )/( time(id+1) - time(id) );
        wts(i,:) = [(1-s) s];
        match(i,1) = id;
    end
end

end

function match = matching(matchpull,wts,distvec)
% Match each pulling force in frame window with residue distances
%  matchpull is output of matching_pull2dcd
%  wts is output of matching_pull2dcd
%  distvec is the distmat(idi,idj,:) array, ie the distance between
%   the two residues across all frames 
%  match is 2 column array with 1st column the distance btwn residues
%   2nd column the tcl/smd force matched to that distance


%% Preprocess
distvec = distvec(:);

%% Match data points in pull with those in distvec

flag_win = ~isnan(matchpull);
% 1st column will store the matching distance btwn residues
%  Initially it stores the index in fr_beg:fr_end dist falls
match(:,1) = matchpull;
%  Everywhere the index is not NaN replace it with the linear
%   interpolation of distvec. Recall match(flag_win,1) is 
%   returning the index i for findinging left end 
%   it belongs [fr_beg:fr_end](i) in half-closed interval. Bc
%   distvec entires correspond to [fr_beg:fr_end], there is match
%    The interpolation is nontrivial bc tcl/smd are sampled more 
%    finely than the dcd trajectory. The wts reflect pos of tcl/smd
%    time within the movie frame interval
match(flag_win,1) = wts(flag_win,1).*distvec(match(flag_win,1)) + ...
                     wts(flag_win,2).*distvec(1+match(flag_win,1));

end

function [Y,val] = mycondexp(match,pull,bins,query)
% Compute the conditional expectation of pull force wrt residue dist
%  match is output by matching above
%  pull is the TCL/SMD Matlab import from main body
%  bins is output by mybins above
%  query are points at which we are evaluating the cond exp
%% Identify bins to which distances in frame intervals belong
% Truncate match and pull to relevant for this movie snip
pull  = pull(~isnan(match(:,1)),:);
match = match(~isnan(match(:,1)),:);

for i=length(bins)-1:-1:1
    if i==length(bins)-1
        flag_bins(:,i) = (match(:,1) >= bins(i))&...
                         (match(:,1) <= bins(i+1));
    else
        flag_bins(:,i) = (match(:,1) >= bins(i))&...
                         (match(:,1) < bins(i+1));
    end
end
    
%% Compute condexp in each bin
% Y(i) stores cond exp for [bin(i) bin(i+1))
for i=(length(bins)-1):-1:1
    % Extract pull values in bin and average
    vals = pull(flag_bins(:,i),2);
    Y(i) = mean(vals);
end

%% Compute cond exp at query points
if nargin == 4
    id = (bins(1:end-1) <= query(:))&(query(:) < bins(2:end));
    for i=length(query):-1:1
        try
            % If point was contained in a half-closed bin, take that
            % expectation
            val(i) = Y(id(i,:));
        catch ME
            switch ME.identifier
                case 'MATLAB:subsassigndimmismatch'
                    % id(i,:) logical was empty and couldnt assign to val
                    if query(i) == bins(end)
                        val(i) = Y(end);
                    else
                        val(i) = NaN;
                    end
                otherwise
                    rethrow(ME);
            end
                
        end
    end
else
    val = NaN;
end

end
