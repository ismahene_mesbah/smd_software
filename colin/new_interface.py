# -*- coding: utf-8 -*-
"""
Created on Wed May 27 17:40:13 2020

@author: Ismahene
"""

from itertools import groupby
import tkinter.messagebox  
from tkinter import * 
from tkinter.filedialog import askopenfilename 
from tkinter.filedialog import askopenfilenames 
from tkinter import filedialog
from tkinter import simpledialog
import parse_SMD_log as pr
from scipy.ndimage import gaussian_filter
from tkinter import ttk
from matplotlib.backends.backend_tkagg import ( FigureCanvasTkAgg,NavigationToolbar2Tk) 
import extensions as e
from functions import *
import webbrowser
from matplotlib.figure import Figure
import residrk as corr
import seaborn as sns
import matplotlib
matplotlib.use('TkAgg')

class App:
    def __init__(self):

        self.path=None
        self.df =None
        self.widget= None
        self.toolbar= None
        
        self.widget_main= None
        self.toolbar_main= None
        
        self.widget_tab2=None
        self.toolbar_tab2= None
        
        self.widget_2nd_detection= None
        self.toolbar_2nd_detection= None
        
        self.widget_slopes= None
        self.toolbar_slopes= None
        
        self.widget_refolding= None
        self.toolbar_refolding= None
        
              
        self.widget_correlations= None
        self.toolbar_correlations= None
        
        self.window = Tk() 
        self.style = ttk.Style(self.window)
     #   self.style.configure("lefttab.TNotebook", tabposition="nw")
        self.notebook = ttk.Notebook(self.window)
 
        self.tab1 = Frame(self.notebook,  width=600, height=200)
        self.tab2 = Frame(self.notebook,  width=600, height=200)
        self.tab3 = Frame(self.notebook,  width=600, height=200)
        self.tab4 = Frame(self.notebook,  width=600, height=200)
        self.tab5 = Frame(self.notebook,  width=600, height=200)
        self.tab6 = Frame(self.notebook,  width=600, height=200)
        self.tab7 = Frame(self.notebook,  width=600, height=200)

        
        self.notebook.add(self.tab1, text="Main")
        self.notebook.add(self.tab2, text="Peak detection")
        self.notebook.add(self.tab4, text="extension")
        self.notebook.add(self.tab5, text="peak detection from extension")
        self.notebook.add(self.tab6, text="Refolding detection")
        self.notebook.add(self.tab3, text="LoadingRates-UnfoldingForce-UnfoldingDistances")
        self.notebook.add(self.tab7, text="Correlations")


        # initialization 
        self.frame=Frame(self.tab1)
        self.secndframe=Frame(self.tab2)
        self.thirdframe=Frame(self.tab3)
        self.fourthframe=Frame(self.tab4)
        self.fifthframe=Frame(self.tab5)
        self.sixframe=Frame(self.tab6)
        self.sevenframe=Frame(self.tab7)

        self.notebook.pack(side='top')
        self.window.title("SMD software")
        self.window.minsize(600, 200)
        self.window.config()

# =============================================================================
#         columns
# =============================================================================
        
        #column names
        self.colnames=('time [s]', 'derivative at peak' , 'selection')
        self.columns=('peak index', 'time [s]', 'force [pN]' )

# =============================================================================
# subframes 1st tab
# =============================================================================
        self.subframe = Frame(self.frame)
        self.plot_frame_main=Frame(self.frame)
# =============================================================================
# subframes 2nd tab
# =============================================================================
        self.subframe_tab2 = Frame(self.secndframe)
        self.plot_frame_tab2=Frame(self.secndframe)
# =============================================================================
# subframes tab (extension)
# =============================================================================
        self.subframe_tab3= Frame(self.fourthframe)
        self.plot_frame_tab3= Frame(self.fourthframe)
# =============================================================================
# subframes slopes
# =============================================================================
        self.subframe_tab_slopes= Frame(self.thirdframe)
        self.plot_frame_slopes= Frame(self.thirdframe)
# =============================================================================
# subframes peak detection from the extension
# =============================================================================
        self.subframe_tab_2nd_detection= Frame(self.fifthframe)
        self.plot_frame_2nd_detection= Frame(self.fifthframe)
# =============================================================================
# subframes refolding detection
# =============================================================================
        self.subframe_tab_refolding= Frame(self.sixframe)
        self.plot_frame_refolding= Frame(self.sixframe)

# =============================================================================
# # subframes correlations of pairs of residues
# =============================================================================
        self.subframe_tab_correlation= Frame(self.sevenframe)
        self.plot_frame_correlation= Frame(self.sevenframe)
# =============================================================================
# scollbars
# =============================================================================

        self.s=Scrollbar(self.subframe_tab_2nd_detection)
        self.scroll_bar=Scrollbar(self.subframe_tab2)
        self.scroll=Scrollbar(self.sixframe)

        # creation of composants
        self.create_widgets()

# =============================================================================
#         1st tab with two subframes : parameters and plots
# =============================================================================
        self.frame.pack(expand=YES)
        self.subframe.grid(row=0, column=1)
        self.plot_frame_main.grid(row=0, column=0)
# =============================================================================
#     2nd tab with two subframes: parameters, plots, table    
# =============================================================================  
        self.secndframe.pack(expand=YES)
        self.subframe_tab2.grid(row=0, column=1)
        self.plot_frame_tab2.grid(row=0, column=0)   
# =============================================================================
#         tab: extensions with 2 subframes 
# =============================================================================
        self.fourthframe.pack(expand=YES)
        self.subframe_tab3.grid(row=0, column=1)
        self.plot_frame_tab3.grid(row=0, column=0)
# =============================================================================
#     tab: slopes     
# =============================================================================
        self.thirdframe.pack(expand=YES)
        self.subframe_tab_slopes.grid(row=0, column=1)
        self.plot_frame_slopes.grid(row=0, column=0)

# =============================================================================
#   tab: 2 nd peak detection (from the extension)
# =============================================================================
        self.fifthframe.pack(expand=YES)
        self.subframe_tab_2nd_detection.grid(row=0, column=1)
        self.plot_frame_2nd_detection.grid(row=0, column=0)
# =============================================================================
#   tab refolding
# =============================================================================
        self.sixframe.pack(expand=YES)
        self.subframe_tab_refolding.grid(row=0, column=1)
        self.plot_frame_refolding.grid(row=0, column=0)


# =============================================================================
#         correlations
# =============================================================================
        self.sevenframe.pack(expand=YES)
        self.subframe_tab_correlation.grid(row=0, column=1)
        self.plot_frame_correlation.grid(row=0, column=0)




# =============================================================================
#     Treeview of 1st peak detection (tab2)
# =============================================================================
        #creation of treeview to  visualize detected peak detection 
        self.tab_peak_detection = ttk.Treeview(self.subframe_tab2,columns=self.columns,selectmode="extended",yscrollcommand=self.scroll_bar.set)
        
        #defenition of headers and size of columns of peak detection tab
        self.tab_peak_detection.heading("peak index", text="peak index")
        self.tab_peak_detection.column("peak index",minwidth=0,width=100, stretch=NO)
        self.tab_peak_detection.heading("time [s]", text="time [s]")
        self.tab_peak_detection.column("time [s]",minwidth=0,width=200, stretch=NO)
        self.tab_peak_detection.heading("force [pN]", text="force [pN]")
        self.tab_peak_detection.column("force [pN]",minwidth=0,width=200, stretch=NO)
        self.scroll_bar.config(command=self.tab_peak_detection.yview)
  
# =============================================================================
#    Treeview of 2nd peak detection (from the extension ) 
# =============================================================================
 
        #creation of treeview to  visualize detected unfolding peaks from the derivative of extension
        self.tab = ttk.Treeview(self.subframe_tab_2nd_detection,columns=self.colnames,selectmode="extended",yscrollcommand=self.s.set)

        self.tab.heading("time [s]", text="time [s]")
        self.tab.column("time [s]",minwidth=0,width=200, stretch=NO)
        self.tab.heading("selection", text="selection")
        self.tab.column("selection",minwidth=0,width=200, stretch=NO)
        self.tab.heading("derivative at peak", text="derivative at peak")
        self.tab.column("derivative at peak",minwidth=0,width=200, stretch=NO)
        self.s.config(command=self.tab.yview)
# =============================================================================
#   Treeview of refolding detection
# =============================================================================

        #creation of the treeview to vizualize the detected refolding peaks from the derivative of extension
        self.tab_refolding = ttk.Treeview(self.subframe_tab_refolding,columns=self.colnames,selectmode="extended",yscrollcommand=self.scroll.set)
        
        self.tab_refolding.heading("time [s]", text="time [s]")
        self.tab_refolding.column("time [s]",minwidth=0,width=200, stretch=NO)
        self.tab_refolding.heading("selection", text="selection")
        self.tab_refolding.column("selection",minwidth=0,width=200, stretch=NO)
        self.tab_refolding.heading("derivative at peak", text="derivative at peak")
        self.tab_refolding.column("derivative at peak",minwidth=0,width=200, stretch=NO)
        self.scroll.config(command=self.tab_refolding.yview)
        
# =============================================================================
# # widgets
# =============================================================================

    def create_widgets(self):
        "loads all widgets "
        self.create_buttons_maintab()
        self.create_buttons_tab2()
        self.create_buttons_tab3()
        self.create_buttons_slopes()
        self.create_buttons_detection_from_extension()
        self.create_buttons_refolding()
        self.create_buttons_correlation()

# =============================================================================
# 1st tab widgets and functions
# =============================================================================

    def fileDialog(self):
        """
        

        This function opens a window to select the log fle of SMD simulation
        Once the file selected force vs displacement (MainTab) 
        and force vs time (tab 2) are plotted 
        -------
        

        """
        path=askopenfilenames(initialdir =  "", title = "Select A File",
                                              filetypes =[("log file", "*.log"),("Text", "*.txt"),("CSV","*.csv")] ) 
        
        
        if len(path) >1:
            
            # Open file in write mode
            with open('all_logs.log', 'w') as all_logs:
      
                # Iterate through list
                for name in path:
      
                    # Open each file in read mode
                    with open(name) as infile:
      
                        # read the data from file1 and
                        # file2 and write it in file3
                        all_logs.write(infile.read())
      
                    # Add '\n' to enter data of file2
                    # from next line
                        all_logs.write("\n")
        
        if len(path)== 1:
            self.path= path[0]
            label_path=self.path
          #  label_path= (path[0]).split( "/")[-1]

        
        elif len(path) != 1:
            self.path= "all_logs.log"
            
            label_file1= (path[0]).split( "/")[-1]
            label_file2= (path[1]).split( "/")[-1]
            common = os.path.commonprefix([label_file1, label_file2])
            
            label_path= str("merged_")+ str(len(path)) + common + str('_total') 
            
        Label(self.subframe, text=label_path,font=("Courrier", 10),width=80).grid(row=1, column=1)
        
        self.pb1 = ttk.Progressbar(self.subframe, orient=HORIZONTAL, length=300,  mode='determinate')
        self.pb1.grid(row=2, columnspan=3, pady=20)
        for i in range(3):
            self.subframe.update_idletasks()
            self.pb1['value'] += 20
            time.sleep(1)
        
        
        self.FvsDis.grid(row=2,column=1)
        #plot force vs displacement
        self.plot_FvsDisplacement()
        knots=self.knots.get()
        #plot force vs time
        self.plot_FvsTime()
       

    def get_information(self):
        """
        This function exports the information
        of the log file into a csv file using
        tab separations

        Returns
        -------
        None.

        """
        export_file_path = filedialog.asksaveasfilename(defaultextension='.csv')
        self.df.to_csv(export_file_path, sep='\t',header=True, index=False, mode='w')
    
        
    def create_buttons_maintab(self):
        """
        creates all buttons of the first tab (main tab)

        Returns
        -------
        None.

        """
# =============================================================================
#         button to upload the log file
# =============================================================================
        upload_button = Button(self.subframe, text="1- Upload log file or a Tab-Separated file (csv or txt)", font=("Courrier", 8)
                          ,width=60, command=self.fileDialog)
        upload_button.grid(row=0,column=1)
        
        self.link_tab_file = Label(self.subframe, text="Tab-Sep file", font=("Courrier", 10), bg='lightblue', cursor="hand2") 

        

        self.link_tab_file.bind("<Enter>", self.on_enter_csv)
        self.link_tab_file.bind("<Leave>", self.on_leave_csv)
        self.link_tab_file.grid(row=0, column=2)
        

# =============================================================================
# button to export information of log file to csv
# =============================================================================
        self.export_info_b= Button(self.subframe, text="4- export information to file",
                       font=("Courrier", 10),width=30,command= self.get_information)
        

# =============================================================================
# button   :plot force vs displacement 
# =============================================================================
        self.FvsDis = Button(self.subframe, text="2- plot force vs displacement",
                        font=("Courrier", 10),width=30, command=self.plot_FvsDisplacement)

# =============================================================================
# button: plot energy parameters
# =============================================================================
        self.eparms_button= Button(self.subframe, text="3- plot energy parameters" ,
                        font=("Courrier", 10),width=30,command=self.plot_energyVStimeStep)
# =============================================================================
# Checkbuttons of energy parms
# =============================================================================
        self.radiobutton_eparms = StringVar()
        self.radiobutton_g = IntVar()
        
        self.radiobutton_tcl_force= IntVar()

        self.radiobutton_bond= Radiobutton(self.subframe, text="BOND", variable= self.radiobutton_eparms, value= "BOND",  width=10)
        self.radiobutton_angle= Radiobutton(self.subframe, text="ANGLE", variable= self.radiobutton_eparms, value= "ANGLE",  width=10)
        self.radiobutton_dihed= Radiobutton(self.subframe, text="DIHED", variable= self.radiobutton_eparms, value= "DIHED" ,width=10)
        self.radiobutton_imprp= Radiobutton(self.subframe, text="IMPRP", variable= self.radiobutton_eparms, value= "IMPRP",  width=10)
        self.radiobutton_elect= Radiobutton(self.subframe, text="ELECT", variable= self.radiobutton_eparms, value= "ELECT",  width=10)
        self.radiobutton_VDW= Radiobutton(self.subframe, text="VDW", variable= self.radiobutton_eparms, value= "VDW",  width=10)
        self.radiobutton_bound= Radiobutton(self.subframe, text="BOUNDARY", variable= self.radiobutton_eparms, value= "BOUNDARY", width=10)
        self.radiobutton_misc= Radiobutton(self.subframe, text="MISC", variable= self.radiobutton_eparms, value= "MISC",  width=10)
        self.radiobutton_kinetic= Radiobutton(self.subframe, text="KINETIC", variable= self.radiobutton_eparms, value= "KINETIC", width=10)
        self.radiobutton_total= Radiobutton(self.subframe, text="TOTAL", variable= self.radiobutton_eparms, value= "TOTAL", width=10)
        self.radiobutton_temp= Radiobutton(self.subframe, text="TEMP", variable= self.radiobutton_eparms, value= "TEMP", width=10)
        self.radiobutton_potential= Radiobutton(self.subframe, text="POTENTIAL", variable= self.radiobutton_eparms, value= "POTENTIAL", width=10)
        self.radiobutton_total3= Radiobutton(self.subframe, text="TOTAL3", variable= self.radiobutton_eparms, value= "TOTAL3", width=10)
        self.radiobutton_tempavg= Radiobutton(self.subframe, text="TEMPAVG", variable= self.radiobutton_eparms, value= "TEMPAVG", width=10)
        self.radiobutton_pressure= Radiobutton(self.subframe, text="PRESSURE", variable= self.radiobutton_eparms, value= "PRESSURE", width=10)
        self.radiobutton_gpressure= Radiobutton(self.subframe, text="GPRESSURE", variable= self.radiobutton_eparms, value= "GPRESSURE", width=10)
        self.radiobutton_volume= Radiobutton(self.subframe, text="VOLUME", variable= self.radiobutton_eparms, value= "VOLUME", width=10)
        self.radiobutton_pressavg= Radiobutton(self.subframe, text="PRESSAVG", variable= self.radiobutton_eparms, value= "PRESSAVG", width=10)
        self.radiobutton_gpressavg= Radiobutton(self.subframe, text="GPRESSAVG", variable= self.radiobutton_eparms, value= "GPRESSAVG", width=10)
        
        
        self.radiobutton_grid = ttk.Checkbutton(self.subframe,  text='Add Grid',variable= self.radiobutton_g, onvalue=1, offvalue=0,  width=10)
        self.radiobutton_tcl_force_button= ttk.Checkbutton(self.subframe,  text='See F(-x)',variable= self.radiobutton_tcl_force, onvalue=1, offvalue=0,  width=10)



    #´   self.radiobutton_grid= Radiobutton(self.subframe, text="Add grid", variable= self.radiobutton_g, width=10, font=("Courrier", 10))
        
#        self.radiobutton_tcl_force_button= Radiobutton(self.subframe, text="See F(-x)", variable= self.radiobutton_tcl_force, value=2,  width=10, font=("Courrier", 10))


    
    def plot_FvsDisplacement(self):
        """
        This function plot force vs displacement
        After the 'plot force vs displacement' button 
        is pressed, radiobuttons of energy parameters appear

        Returns
        -------
        None.

        """
        start=time.time()
                # remove old widgets
        if self.widget_main:
            self.widget_main.destroy()

        if self.toolbar_main:
            self.toolbar_main.destroy()
            
        figure = Figure(figsize=(6,5), dpi=100)
        ax=figure.add_subplot(111)
        ax.set_xlabel('Displacement [nm]')
        ax.set_ylabel('Force [pN]')
        canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_main)
        canvas.draw()
        self.widget_main= canvas.get_tk_widget()
        self.widget_main.grid(row=10, column=1)
        # navigation toolbar
        toolbarFrame = Frame(master= self.plot_frame_main)
        toolbarFrame.grid(row=11,column=1)
        self.toolbar_main = NavigationToolbar2Tk(canvas, 
                                           toolbarFrame) 
        figure.tight_layout()
        self.toolbar_main.update()
        if self.radiobutton_g.get() ==1:
            ax.grid()

        if  os.path.splitext(self.path)[-1].lower() =='.csv' or  os.path.splitext(self.path)[-1].lower() == '.txt':
            
            self.df= read_data_csv(self.path)
            
            
        else: 
            self.df_energy_parms, self.df= pr.extract_info_from_smd_log(self.path)
            # try:
            #     self.df_energy_parms, self.df= pr.extract_info_from_smd_log(self.path)
            # except:
            #     log_temp_file= '/tmp/log.log'
            #     with open(self.path, "rb") as data, open( log_temp_file, 'w') as log_file:
            #         text = data.read().decode('utf-16-le')
            #         log_file.write(text)
            #         self.df_energy_parms, self.df= pr.extract_info_from_smd_log(log_temp_file)

            # # try:
            #     self.df_energy_parms, self.df
            # except:
            
                
        
        self.pb1.destroy()
            
            
        try:
            self.df['-F']
            if self.radiobutton_tcl_force.get() ==1:
                ax.plot(self.df['displacement'], self.df['F'])
                ax.plot(self.df['displacement'], self.df['-F'], color='red')
            else:
                ax.plot(self.df['displacement'], self.df['F'])

        except:
            ax.plot(self.df['displacement'], self.df['F'])
        

        #1st column
        self.eparms_button.grid(row=15,column=1)
        self.radiobutton_bond.grid(row=3, column=0)
        self.radiobutton_angle.grid(row=4, column=0)
        self.radiobutton_dihed.grid(row=5, column=0)
        self.radiobutton_imprp.grid(row=6, column=0)
        self.radiobutton_misc.grid(row=7, column=0)
        self.radiobutton_pressavg.grid(row=8, column=0)

        # 3rd column
        self.radiobutton_total3.grid(row=3, column=2)
        self.radiobutton_elect.grid(row=4, column=2)
        self.radiobutton_VDW.grid(row=5, column=2)
        self.radiobutton_tempavg.grid(row=6, column=2)
        self.radiobutton_pressure.grid(row=7, column=2)
        self.radiobutton_gpressure.grid(row=8, column=2)


        # 2nd column
        self.radiobutton_bound.grid(row=3, column=1)
        self.radiobutton_kinetic.grid(row=4, column=1)
        self.radiobutton_total.grid(row=5, column=1)
        self.radiobutton_temp.grid(row=6, column=1)
        self.radiobutton_potential.grid(row=7, column=1)
        self.radiobutton_volume.grid(row=8, column=1)
        self.radiobutton_gpressavg.grid(row=9, column=1)
        self.radiobutton_grid.grid(row=2 , column=2)
        self.export_info_b.grid(row=20,column=1)  
       
        #self.radiobutton_g.set(0)
        #self.radiobutton_tcl_force.set(0)

        
        self.radiobutton_tcl_force_button.grid(row= 3, column =2)
        
 


        end=time.time()
        print('plot force vs displacement ', round(end-start, 2), ' seconds')

        
    def plot_energyVStimeStep(self):
        """
        This function plot an energy parameters
        selected by the user from the radiobutton once
        the button is pressed

        Returns
        -------
        None.

        """
        start=time.time()
        # remove old widgets
        if self.widget_main:
            self.widget_main.destroy()

        if self.toolbar_main:
            self.toolbar_main.destroy()
            
        figure = Figure(figsize=(7,5), dpi=100)
        ax=figure.add_subplot(111)
        ax.set_xlabel("time in seconds")
        canvas= FigureCanvasTkAgg(figure, self.plot_frame_main)
        canvas.draw()
        self.widget_main= canvas.get_tk_widget()
        self.widget_main.grid(row=10, column=1)

        toolbarFrame = Frame(master= self.plot_frame_main)
        # navigation toolbar
        toolbarFrame = Frame(master= self.plot_frame_main)
        toolbarFrame.grid(row=11,column=1)
        self.toolbar_main = NavigationToolbar2Tk(canvas, 
                                   toolbarFrame) 
        self.toolbar_main.update()
        if self.radiobutton_g.get() ==1:
            ax.grid()

        if self.radiobutton_eparms.get()=="BOND":
           # df_energy_parms,self.df=extract_info_from_smd_log(self.path)
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['BOND'])
            ax.set_ylabel("BOND")
                   
        elif self.radiobutton_eparms.get()=="ANGLE":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['ANGLE'])
            ax.set_ylabel("ANGLE")

        elif self.radiobutton_eparms.get()=="DIHED":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['DIHED'])
            ax.set_ylabel("DIHED")

        elif self.radiobutton_eparms.get()=="IMPRP":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['IMPRP'])
            ax.set_ylabel("IMPRP")

        elif self.radiobutton_eparms.get()=="ELECT":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['ELECT'])
            ax.set_ylabel("ELECT")
                        
        elif self.radiobutton_eparms.get()=="VDW":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['VDW'])
            ax.set_ylabel("VDW")

        elif self.radiobutton_eparms.get()=="BOUNDARY":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['BOUNDARY'])
            ax.set_ylabel("BOUNDARY")
        
        elif self.radiobutton_eparms.get()=="MISC":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['MISC'])
            ax.set_ylabel("MISC")
        
        elif self.radiobutton_eparms.get()=="KINETIC":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['KINETIC'])
            ax.set_ylabel("KINETIC")
        
        elif self.radiobutton_eparms.get()=="TOTAL":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['TOTAL'])
            ax.set_ylabel("TOTAL")
        
        elif self.radiobutton_eparms.get()=="TEMP":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['TEMP'])
            ax.set_ylabel("TEMP")

        elif self.radiobutton_eparms.get()=="POTENTIAL":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['POTENTIAL'])
            ax.set_ylabel("POTENTIAL")

        elif self.radiobutton_eparms.get()=="TOTAL3":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['TOTAL3'])
            ax.set_ylabel("TOTAL3")

        elif self.radiobutton_eparms.get()=="TEMPAVG":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['TEMPAVG'])
            ax.set_ylabel("TEMPAVG")

        elif self.radiobutton_eparms.get()=="PRESSURE":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['PRESSURE'])
            ax.set_ylabel("PRESSURE")

        elif self.radiobutton_eparms.get()=="GPRESSURE":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['GPRESSURE'])
            ax.set_ylabel("GPRESSURE")

        elif self.radiobutton_eparms.get()=="VOLUME":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['VOLUME'])
            ax.set_ylabel("VOLUME")

        elif self.radiobutton_eparms.get()=="PRESSAVG":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['PRESSAVG'])
            ax.set_ylabel("PRESSAVG")

        elif self.radiobutton_eparms.get()=="GPRESSAVG":
            ax.plot(self.df_energy_parms['timeInS'] , self.df_energy_parms['GPRESSAVG'])
            ax.set_ylabel("GPRESSAVG")

        elif not  self.radiobutton_eparms.get():
            tkinter.messagebox.showinfo("Warning ", "Please select an energy parameter")
        
        self.radiobutton_g.set(0)

        end=time.time()
        print('plot energy parameters ', round(end-start, 2), ' seconds')


# =============================================================================
# 2nd tab buttons and functions
# =============================================================================
    def plot_FvsTime(self):
        """
        This function plots force vs time curve
        once the button is pressed, knots and entries appear
        the 'detect peaks' button also appears
        
        Returns
        -------
        None.

        """
        start=time.time()
        if self.path==None :
            tkinter.messagebox.showinfo("Warning ", "Please upload the log file ")
                # remove old widgets
        if self.widget_tab2:
            self.widget_tab2.destroy()

        if self.toolbar_tab2:
            self.toolbar_tab2.destroy()

        figure = Figure(figsize=(7,5), dpi=100)
        ax=figure.add_subplot(111)
        canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_tab2)
        canvas.draw()
        self.widget_tab2= canvas.get_tk_widget()
        self.widget_tab2.grid(row=10, column=1)
                # navigation toolbar
        toolbarFrame = Frame(master= self.plot_frame_tab2)
        toolbarFrame.grid(row=11,column=1)
        self.toolbar_tab2 = NavigationToolbar2Tk(canvas, toolbarFrame)
        ax.set_xlabel('time [s]')
        ax.set_ylabel('Force [pN]')
        
        
        
        if self.radiobutton_g_tab2.get()==1:
            ax.grid()
        
            
        if self.df is None:
            self.energy_parms, self.df= pr.extract_info_from_smd_log(self.path)
          #  ax.plot(self.df['timeInS'], self.df['F'])
        
            
                                            
        elif not self.knots.get() and self.df is not None:
            ax.plot(self.df['timeInS'], self.df['F'])

        elif self.knots.get() and self.df is not None:
            model=get_natural_cubic_spline_model(self.df['timeInS'], self.df['F'], minval=min(self.df['timeInS']), maxval=max(self.df['timeInS']), n_knots=self.knots.get())
            smooth=model.predict(self.df['timeInS'])
            ax.plot(self.df['timeInS'], self.df['F'])
            ax.plot(self.df['timeInS'], smooth)
        
        #if file contains TCL forces
        try:
            self.df['-F']
            #if the user choosed to see these TCL forces
            if self.force_tcl_tab2.get() ==1 : 
                ax.plot(self.df['timeInS'], self.df['-F'], color='red')
                #make sure the dataframe is already loaded
                if self.df is None:
                    self.energy_parms, self.df= pr.extract_info_from_smd_log(self.path)
                
                    
                #if there is NO knot value, just df values
                elif not self.knots.get() and self.df is not None:
                    ax.plot(self.df['timeInS'], self.df['-F'], color='red')
                # if there is knot value : smoothing
                elif self.knots.get() and self.df is not None:
                    model=get_natural_cubic_spline_model(self.df['timeInS'], self.df['-F'], minval=min(self.df['timeInS']), maxval=max(self.df['timeInS']), n_knots=self.knots.get())
                    smooth=model.predict(self.df['timeInS'])
                    ax.plot(self.df['timeInS'], self.df['-F'], color='red')
                    ax.plot(self.df['timeInS'], smooth)
        #if the file does not contain TCL forces: untick the checkbutton    
        except:
            self.force_tcl_tab2.set(0) 

                

        self.label.grid(row=1,column=1) #give smoothing value
        self.label1.grid(row=2,column=1) #click for more details
        self.entry_knots.grid(row=3,column=1)
        self.knots_button.grid(row=4, column=1) # button: use default value of knots
        self.knots_infos.grid(row=5, column=1) #info knots
        self.prominence_entry.grid(row=7,column=1)
        self.prominence_b.grid(row=8,column=1) # button: see prominence distribution
        self.reset.grid(row=11, column=1)
        self.label_prominence.grid(row=6, column=1)
        self.link1.grid(row=7,column=1, sticky=E) #link prominence ('?')
        self.peaks_button.grid(row=9,column=1) 
        
        self.radiobutton_grid_tab2.grid(row=11, column=1)
        self.f_tcl_tab2.grid(row=3, column=2)

        
        self.link2.grid(row=11 ,column=1, sticky=E) #link grid('?')
        

        
        end=time.time()
        print('plot force vs time ', round(end-start, 2), ' seconds')
        



    def create_buttons_tab2(self):
        """
        creates all the buttons of the 2nd tab

        Returns
        -------
        None.

        """
        self.radiobutton_g_tab2= IntVar()
        self.force_tcl_tab2= IntVar()
# =============================================================================
# button force vs time in seconds
# =============================================================================
        FvsT = Button(self.subframe_tab2, text="1- plot force vs time",font=("Courrier", 10),
                      width=30,command=self.plot_FvsTime)
        FvsT.grid(row=0,column=1)
        
       # self.radiobutton_grid_tab2= Radiobutton(self.subframe_tab2, text="Add a grid to the plot", variable= self.radiobutton_g_tab2, value= 1, width=30, font=("Courrier", 10))
        self.radiobutton_grid_tab2 = ttk.Checkbutton(self.subframe_tab2,  text='Add Grid',variable= self.radiobutton_g_tab2, onvalue=1, offvalue=0,  width=10)
        self.f_tcl_tab2 = ttk.Checkbutton(self.subframe_tab2,  text='Plot F(-x)',variable= self.force_tcl_tab2, onvalue=1, offvalue=0,  width=10)



        
        #save selected peaks
        self.save_peaks = Button(self.subframe_tab2, text="Save selected peaks",font=("Courrier", 10),
                      width=30, command=self.add_selected_peaks)
        self.export_data_1st_detection=Button(self.subframe_tab2, text= "export data", font=("Courrier", 10),
                                              width=30, command=self.export_data_1st_peak_detection)

# =============================================================================
# prominence button
# =============================================================================
        self.prominence_b = Button(self.subframe_tab2, text="2- See prominence distribution",font=("Courrier", 10),
                      width=30,command=self.plot_prominences)

# =============================================================================
# links
# =============================================================================
        self.link1 = Label(self.subframe_tab2, text="?", font=("Courrier", 10), bg='lightblue', cursor="hand2") 
        self.link2 = Label(self.subframe_tab2, text="?", font=("Courrier", 10), bg='lightblue', cursor="hand2") 

        self.link2.bind("<Enter>", self.on_enter_link2)
        self.link2.bind("<Leave>", self.on_leave_link2)
        

        self.link1.bind("<Enter>", self.on_enter)
        self.link1.bind("<Leave>", self.on_leave)
        self.link1.bind("<Button-1>", lambda e: self.callback("https://en.wikipedia.org/wiki/Topographic_prominence"))
# =============================================================================
#   defaults knots buttons
# =============================================================================
        self.knots_button=Button(self.subframe_tab2, text='Use default value of smoothing (knots)', width=30,font=("Courrier", 10),
                            command=self.compute_defaults_knots)
        self.knots_infos=Button(self.subframe_tab2, text='Information about knots computing ', 
                           width=30,font=("Courrier", 10), command=self.information_knots)

        self.knots=IntVar()
        self.label1=Label(self.subframe_tab2, text="Click for details", bg='lightblue', cursor="hand2")
        self.label=Label(self.subframe_tab2, text="Give value (knots)"+ '\n'+" to smooth data (starting from 2)")

        self.label1.bind("<Button-1>", lambda e: self.callback("https://towardsdatascience.com/numerical-interpolation-natural-cubic-spline-52c1157b98ac"))
        self.entry_knots=Entry(self.subframe_tab2, textvariable=self.knots, width=30, font=("Courrier", 10))
        
        self.reset=Button(self.subframe_tab2, text="Reset",  font=("Courrier", 10), width=30,command=self.clear_button)
        
        self.prominence =IntVar()

        self.label_prominence=Label(self.subframe_tab2, text= 'Prominence',  width=30, font=("Courrier", 10))
        self.prominence_entry= Entry(self.subframe_tab2, textvariable=self.prominence, font=("Courrier", 10), width=30)
        
        self.peaks_button = Button(self.subframe_tab2, text= "3- Detect peaks",
                        font=("Courrier", 10),width=30,command=self.plot_peaks_detection)



    def clear_button(self):
        """
        This function is called when the user
        presses the 'Reset' button of the 2nd tab
        It clears the knots entry

        Returns
        -------
        None.

        """
        self.knots.set(0)
        
        
        
    def callback(self, url):
        """
        opens a web page 

        Parameters
        ----------
        url :string
            An url for more details.

        Returns
        -------
        None.

        """
        webbrowser.open_new(url)
        
    def on_enter_csv(self, event):
        """
        Gives a definition of prominence when the user hovers

        Parameters
        ----------
        event : string
            <Enter>.

        Returns
        -------
        None.

        """
        self.link_tab_file.configure(text="Header must be:" +'\n'+"timeStep, timeInS[sec], displacement[nm], "+'\n'+"F[pN], -F[pN]", bg='white', font='Courrier')
    
    def on_enter(self, event):
        """
        Gives a definition of prominence when the user hovers

        Parameters
        ----------
        event : string
            <Enter>.

        Returns
        -------
        None.

        """
        self.link1.configure(text="The prominence of a peak  measures"+'\n'+" how much the peak stands out to other peaks."+'\n'" Click to know more", bg='white', font='Courrier')

    def on_enter_link2(self, event):
        """
        Gives a definition of prominence when the user hovers

        Parameters
        ----------
        event : string
            <Enter>.

        Returns
        -------
        None.

        """
        self.link2.configure(text="After selecting this, make sure you click the button", bg='white', font='Courrier')


    def on_leave(self, enter):
        """
        when the user leaves the label becomes ? (like in the initial state)

        Parameters
        ----------
        enter : string
            on leave.

        Returns
        -------
        None.

        """
        self.link1.configure(text="?", bg='lightblue')
        
    def on_leave_csv(self, enter):
        """
        when the user leaves the label becomes ? (like in the initial state)

        Parameters
        ----------
        enter : string
            on leave.

        Returns
        -------
        None.

        """
        self.link_tab_file.configure(text="?", bg='lightblue')
        
        
        
    def on_leave_link2(self, enter):
        """
        When the user leaves the ? is shown again

        Parameters
        ----------
        enter : str
            the action of leaving to this label.

        Returns
        -------
        None.

        """
        self.link2.configure(text="?", bg='lightblue')
        
        
    def information_knots(self):
        tkinter.messagebox.showinfo("information ", "Knots value is computed as follows: we take a moving window width of 0.4 nm then we devide the full length* in nm by 0.4."+ '\n' + "*Full length is the total displacement.")
   
# =============================================================================
# 1 st peak detection: save peaks
# =============================================================================
    def add_selected_peaks(self):
        """
        This function saves the peaks that the user selects

        Returns
        -------
        None.

        """
        try:
            self.peaks
        except:
            self.peaks=[]
        else:
            timeInS= list(self.df['timeInS'])

          #  timeInS= list(map( "{:.2e}".format, self.df['timeInS']) )
            for i in range(len(timeInS)):
                timeInS[i]= "{:.2e}".format(timeInS[i])
            #print(timeInS)
            curItem= self.tab_peak_detection.selection()
            curItems = [(self.tab_peak_detection.item(i)['values']) for i in curItem]
            user_selection=[]
            self.selection=[]
            for i in curItems:
                user_selection.append(float(i[1]))
                self.selection.append(i[-1])
            for i in range(len(user_selection)):
                
                user_selection[i]= "{:.2e}".format(user_selection[i])
                if timeInS.index(user_selection[i])not in self.peaks:
                    self.peaks.append(timeInS.index(user_selection[i]))
         
                
            tkinter.messagebox.showinfo("informations ", "Added to list successfully")

# =============================================================================
# 1st peak detection: export data  
# =============================================================================
    def export_data_1st_peak_detection(self):
        """
        This function exports the data concerning the peaks
        that the user selected

        Returns
        -------
        None.

        """
        curItem= self.tab_peak_detection.selection()
        curItems = [(self.tab_peak_detection.item(i)['values']) for i in curItem]
        data={'index':[], 'timeInS [s]':[], 'force [pN]':[]}
        for i in curItems:
            data['index'].append(i[0])
            data['timeInS [s]'].append(i[1])
            data['force [pN]'].append(i[2])
        df_data = pd.DataFrame.from_dict(data)
        export_file_path = filedialog.asksaveasfilename(defaultextension='.csv')

        df_data.to_csv( export_file_path, sep='\t',header=True, index=False, mode='w')
     #   tkinter.messagebox.showinfo("informations ", "please, check file: 'data_from_peak_detection'"+self.path)
 
        
    def plot_prominences(self):
        """
        plots the distribution of prominences

        Returns
        -------
        None.

        """

        start=time.time()
                        # remove old widgets
        if self.widget_tab2:
            self.widget_tab2.destroy()

        if self.toolbar_tab2:
            self.toolbar_tab2.destroy()

        figure = Figure(figsize=(7,5), dpi=100)
        ax= figure.add_subplot(111)
        canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_tab2)
        canvas.draw()
        self.widget_tab2=canvas.get_tk_widget()
        self.widget_tab2.grid(row=10, column=1)
        # navigation toolbar
        toolbarFrame = Frame(master= self.plot_frame_tab2)
        toolbarFrame.grid(row=11,column=1)
        self.toolbar_tab2 = NavigationToolbar2Tk(canvas, 
                                   toolbarFrame) 
        self.toolbar_tab2.update()
        ax.set_title('Prominence distribution')
        
        if self.radiobutton_g_tab2.get() ==1:
            ax.grid()
        try:
            self.df['-F']
            if self.force_tcl_tab2.get() == 1:
                if self.knots.get():
                    knots=self.knots.get()
                    prominences=find_peaks(self.df['-F'], self.df['timeInS'],  knots)[2]
                    ax.bar(range(len(prominences)), prominences)
                else:
                    peaks=signal.find_peaks(self.df['-F'])[0]
                    prominences = signal.peak_prominences(self.df['-F'], peaks)[0]
                    ax.bar(range(len(prominences)), prominences)
            else:
                if self.knots.get():
                    knots=self.knots.get()
                    prominences=find_peaks(self.df['F'], self.df['timeInS'],  knots)[2]
                    ax.bar(range(len(prominences)), prominences)
                else:
                    peaks=signal.find_peaks(self.df['F'])[0]
                    prominences = signal.peak_prominences(self.df['F'], peaks)[0]
                    ax.bar(range(len(prominences)), prominences)
        except:
            if self.knots.get():
                knots=self.knots.get()
                prominences=find_peaks(self.df['F'], self.df['timeInS'],  knots)[2]
                ax.bar(range(len(prominences)), prominences)
            else:
                peaks=signal.find_peaks(self.df['F'])[0]
                prominences = signal.peak_prominences(self.df['F'], peaks)[0]
                ax.bar(range(len(prominences)), prominences)

        self.radiobutton_g_tab2.set(0)
        end=time.time()
        print('plot prominence ', round(end-start,2) , ' seconds')
            

            
    def compute_defaults_knots(self):
        """
        Computes the default value of knots and writes
        it on the right entry

        Returns
        -------
        None.

        """
        start= time.time()

        f=self.df['F']
        displacement=np.array(self.df['displacement'])
        displacement=float(displacement[-1])
        knots=int( displacement  // 0.4 )

        if knots <=20:
            knots=50
        peaks=signal.find_peaks(f)[0]
        prominences=signal.peak_prominences(f, peaks)[0]
        default_prominence=int(np.mean(prominences))
        tkinter.messagebox.showinfo("information ", "default smoothing value of knots is: "+ str(knots)+ '\n' + 'Suggested prominence: '+ str(default_prominence))
        self.knots.set(knots)
        self.prominence.set(default_prominence)
        end= time.time()
        print('knots computing ', round(end-start,2) , ' seconds')

# =============================================================================
#     1st peak detection:    plot peaks
# =============================================================================
    def plot_peaks_detection(self):
        """
        plots peaks on force vs time curve

        Returns
        -------
        None.

        """
        start=time.time()
        if self.widget_tab2:
            self.widget_tab2.destroy()

        if self.toolbar_tab2:
            self.toolbar_tab2.destroy()
            
        figure = Figure(figsize=(7,5), dpi=100)
        ax= figure.add_subplot(111)
        ax.set_xlabel('time [s]')
        ax.set_ylabel('Force [pN]')
        canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_tab2)
        canvas.draw()
        self.widget_tab2= canvas.get_tk_widget()
        self.widget_tab2.grid(row=10, column=1)
        # navigation toolbar
        toolbarFrame = Frame(master= self.plot_frame_tab2)
        toolbarFrame.grid(row=11,column=1)
        self.toolbar_tab2 = NavigationToolbar2Tk(canvas, 
                                           toolbarFrame) 
        self.toolbar_tab2.update()
        if self.radiobutton_g_tab2.get()==1:
            ax.grid()
        p=self.prominence.get()
        
        try:
            self.df['-F']
            if self.force_tcl_tab2.get() == 1:
                if self.knots.get():
                    window=self.knots.get()
                    smooth,peaks, prominences=find_peaks(self.df['-F'], self.df['timeInS'], knots=window,prominence=p)
                    ax.plot(self.df['timeInS'],self.df['-F'])
                    ax.plot(self.df['timeInS'],smooth)
                    ax.plot(self.df['timeInS'][peaks], smooth[peaks], "or")
        
                    self.tab_peak_detection.delete(*self.tab_peak_detection.get_children())
                    
                    for i in range(len(peaks)):
                        self.tab_peak_detection.insert('' ,i ,values=(i+1, "{:.2e}".format(self.df['timeInS'][peaks[i]]), "{:.2e}".format(self.df['-F'][peaks[i]] ) ))
                    self.tab_peak_detection.grid(row=20, column=1, sticky=W+E+S+N)   
                    self.scroll_bar.grid(row=20, column=2, sticky="ns")
                    self.save_peaks.grid(row=25,column=1)
                    self.export_data_1st_detection.grid(row=30, column=1)
                        
                else:
                    peaks=signal.find_peaks(self.df['-F'], prominence=p)[0]
                    ax.plot(self.df['timeInS'], self.df['-F'])
                    ax.plot(self.df['timeInS'][peaks],self.df['-F'][peaks], "or")
                    self.tab_peak_detection.delete(*self.tab_peak_detection.get_children())
                    for i in range(len(peaks)):
                        self.tab_peak_detection.insert('' ,i ,values=(i+1, "{:.2e}".format(self.df['timeInS'][peaks[i]]), "{:.2e}".format(self.df['-F'][peaks[i]])))
                    self.tab_peak_detection.grid(row=20, column=1, sticky=W+E+S+N)   
                    self.scroll_bar.grid(row=20, column=2, sticky="ns")
                    self.save_peaks.grid(row=25,column=1)
                    self.export_data_1st_detection.grid(row=30, column=1)
            else:
                if self.knots.get():
                    window=self.knots.get()
                    smooth,peaks, prominences=find_peaks(self.df['F'], self.df['timeInS'], knots=window,prominence=p)
                    ax.plot(self.df['timeInS'],self.df['F'])
                    ax.plot(self.df['timeInS'],smooth)
                    ax.plot(self.df['timeInS'][peaks], smooth[peaks], "or")
        
                    self.tab_peak_detection.delete(*self.tab_peak_detection.get_children())
                    
                    for i in range(len(peaks)):
                        self.tab_peak_detection.insert('' ,i ,values=(i+1, "{:.2e}".format(self.df['timeInS'][peaks[i]]), "{:.2e}".format(self.df['F'][peaks[i]] ) ))
                    self.tab_peak_detection.grid(row=20, column=1, sticky=W+E+S+N)   
                    self.scroll_bar.grid(row=20, column=2, sticky="ns")
                    self.save_peaks.grid(row=25,column=1)
                    self.export_data_1st_detection.grid(row=30, column=1)
                        
                else:
                    peaks=signal.find_peaks(self.df['F'], prominence=p)[0]
                    ax.plot(self.df['timeInS'], self.df['F'])
                    ax.plot(self.df['timeInS'][peaks],self.df['F'][peaks], "or")
                    self.tab_peak_detection.delete(*self.tab_peak_detection.get_children())
                    for i in range(len(peaks)):
                        self.tab_peak_detection.insert('' ,i ,values=(i+1, "{:.2e}".format(self.df['timeInS'][peaks[i]]), "{:.2e}".format(self.df['F'][peaks[i]])))
                    self.tab_peak_detection.grid(row=20, column=1, sticky=W+E+S+N)   
                    self.scroll_bar.grid(row=20, column=2, sticky="ns")
                    self.save_peaks.grid(row=25,column=1)
                    self.export_data_1st_detection.grid(row=30, column=1)
                
                     
        except:
            if self.knots.get():
                window=self.knots.get()
                smooth,peaks, prominences=find_peaks(self.df['F'], self.df['timeInS'], knots=window,prominence=p)
                ax.plot(self.df['timeInS'],self.df['F'])
                ax.plot(self.df['timeInS'],smooth)
                ax.plot(self.df['timeInS'][peaks], smooth[peaks], "or")
    
                self.tab_peak_detection.delete(*self.tab_peak_detection.get_children())
                
                for i in range(len(peaks)):
                    self.tab_peak_detection.insert('' ,i ,values=(i+1, "{:.2e}".format(self.df['timeInS'][peaks[i]]), "{:.2e}".format(self.df['F'][peaks[i]] ) ))
                self.tab_peak_detection.grid(row=20, column=1, sticky=W+E+S+N)   
                self.scroll_bar.grid(row=20, column=2, sticky="ns")
                self.save_peaks.grid(row=25,column=1)
                self.export_data_1st_detection.grid(row=30, column=1)
                    
            else:
                peaks=signal.find_peaks(self.df['F'], prominence=p)[0]
                ax.plot(self.df['timeInS'], self.df['F'])
                ax.plot(self.df['timeInS'][peaks],self.df['F'][peaks], "or")
                self.tab_peak_detection.delete(*self.tab_peak_detection.get_children())
                for i in range(len(peaks)):
                    self.tab_peak_detection.insert('' ,i ,values=(i+1, "{:.2e}".format(self.df['timeInS'][peaks[i]]), "{:.2e}".format(self.df['F'][peaks[i]])))
                self.tab_peak_detection.grid(row=20, column=1, sticky=W+E+S+N)   
                self.scroll_bar.grid(row=20, column=2, sticky="ns")
                self.save_peaks.grid(row=25,column=1)
                self.export_data_1st_detection.grid(row=30, column=1)
     
            
        self.radiobutton_g_tab2.set(0)
        end=time.time()
        print('peak detection: ', round(end-start, 2), ' seconds')
    
    
# =============================================================================
#       3rd tab buttons and functions  
# =============================================================================
    def filePDB(self):
        """loads pdb file and prints the path out to the user """
        
        self.pdb=askopenfilename(initialdir =  "", title = "Select A File",
                                              filetypes =(("pdb file", "*.pdb"),("pdb", "*.pdb"),("pdb","*.pdb*")) ) 
        pdb=self.pdb.split('/')
        Label(self.subframe_tab3, text=str(pdb[-1]),  font=("Courrier", 10)
                          ,width=30).grid(row=1, column=1)
        start_, end_, self.structure= e.e2e_PDB(self.pdb)
        self.entry_coords.insert(0, start_ + '-' + end_)
        self.dcd_button.grid(row=2,column=1)  


    def fileDCD(self):
        "loads dcd file and print the path to the user "
        path_dcd=askopenfilenames(initialdir =  "", title = "Select A File",
                                              filetypes =[("dcd file", "*.dcd"),("dcd", "*.dcd"),("dcd","*.dcd*") ])
        

        
        self.dcd= path_dcd[0]

               
        with open('all_dcds.dcd', 'w+b') as all_dcd: 
            for dcd in path_dcd:
                dcds= dcd.split("/")[-1]
                dcd_label="".join(dcds)
                with open(dcd, 'rb') as infile:
                    all_dcd.write(infile.read()) 
                    
        
        self.dcd= 'all_dcds.dcd'


        for dcd in path_dcd:
            dcds= dcd.split("/")[-1]
            dcd_label="".join(dcds)

        
        Label(self.subframe_tab3, text=str(dcd_label),  font=("Courrier", 10)
                          ,width=30).grid(row=3, column=1)
        
        
        
        self.compute_extensions_button.grid(row=10, column= 1)

        #compute the extension+
        self.compute_extensions()
        #plot extension vs force 
        self.plot_extension_force()
        #pack the 'compute extension button
        #plot derivative of extension
        self.plot_derivative_from_extension()


    def create_buttons_tab3(self):
        "creates uploading button for pdb file "
        yt_button = Button(self.subframe_tab3, text="1- Upload PDB file", font=("Courrier", 10)
                          ,width=30, command=self.filePDB)
        yt_button.grid(row=0,column=1, sticky=W+N)   
        

        "creates button of dcd file"
        self.dcd_button = Button(self.subframe_tab3, text="2- Upload DCD  file", font=("Courrier", 10)
                          ,width=30, command=self.fileDCD)
        
        l=Label(self.subframe_tab3, text= 'Enter residue IDs '+ "\n" +'e.g., coordinates of 2 domains:'+ "\n" + '1-83,  84-185', font=("Courrier", 8),width=35)
        l.grid(row=6, column=1)
        self.coords=StringVar()

        self.entry_coords=Entry(self.subframe_tab3, textvariable=self.coords, width=30 , font=("Courrier", 10))
        self.entry_coords.grid(row=7, column=1)
        
        self.compute_extensions_button=Button(self.subframe_tab3, text='3- Compute the extensions',font=("Courrier", 10)
                                         ,width=30, command=self.compute_extensions)
        
        self.label_help_extensions = Label(self.subframe_tab3, text="?", font=("Courrier", 10), bg='lightblue', cursor="hand2") 
        self.label_help_extensions.grid(row=7,column=2)
        self.label_help_extensions.bind("<Enter>", self.on_enter_extensions)
        self.label_help_extensions.bind("<Leave>", self.on_leave_extensions)
        
        self.export_extensions=Button(self.subframe_tab3, text='export data ',
                 font=("Courrier", 10),width=30, command=self.export_extensions_force_time)
        
        self.button_plot_force_extension=Button(self.subframe_tab3, text='plot Force vs extension ',
                 font=("Courrier", 10),width=30, command=self.plot_extension_force)
        self.button_plot_extension_time=Button(self.subframe_tab3, text='plot extension vs time [s] ',
                 font=("Courrier", 10),width=30, command=self.plot_extension_vs_time)

        
    def on_enter_extensions(self, event):
        self.label_help_extensions.configure(text="If you don't put any coordinates"+'\n'+" end to end extension will be computed ", bg='white', font='Courrier')

    def on_leave_extensions(self, enter):
        self.label_help_extensions.configure(text="?", bg='lightblue')
        
# =============================================================================
#         Computing the extension
# =============================================================================
    def compute_extensions(self):
        """
        This function gets the coordinates and computes
        the extension

        Returns
        -------
        None.

        """
        start=time.time()
        try:
            self.dcd
            self.pdb
        except:
            tkinter.messagebox.showinfo("Warning ", "Please load PDB and DCD files")
        else:
            
            self.extensions={'extensions': []}
            
        try:
            self.df['-F']
            if self.force_tcl_tab2.get()==1:
                if not self.coords.get():
                    start_, end_, self.structure= e.e2e_PDB(self.pdb)
                    self.entry_coords.insert(0, start_ + '-' + end_)
                    tkinter.messagebox.showinfo("information ", "extension from end to end will be computed"+'\n'+"Please, press 'Compute extensions' button again")
                    self.export_extensions.grid(row=45 , column=1)
                else:
                    coords1=self.coords.get()
                    coord=coords1.split('-')
                    coord=','.join(coord)
                    coord=list(coord.split(','))
                    
         
                    e2e, self.timeInS, self.force, self.coords_atoms =e.extension_domain(self.dcd, self.structure, self.df['timeInS'],self.df['-F'], str(coord[0]), str(coord[1]) )
                    for i in range(0, len(coord)-1, 2):
                        self.extensions['extensions'].append(e.extension_domain(self.dcd, self.structure, self.df['timeInS'], self.df['-F'], str(coord[i])
                                                                            , str(coord[i+1]))[0])
            else:
                if not self.coords.get():
                    start_, end_, self.structure= e.e2e_PDB(self.pdb)
                    self.entry_coords.insert(0, start_ + '-' + end_)
                    tkinter.messagebox.showinfo("information ", "extension from end to end will be computed"+'\n'+"Please, press 'Compute extensions' button again")
                    self.export_extensions.grid(row=45 , column=1)
                else:
                    coords1=self.coords.get()
                    coord=coords1.split('-')
                    coord=','.join(coord)
                    coord=list(coord.split(','))
                    
         
                    e2e, self.timeInS, self.force, self.coords_atoms =e.extension_domain(self.dcd, self.structure, self.df['timeInS'],self.df['F'], str(coord[0]), str(coord[1]) )
                    for i in range(0, len(coord)-1, 2):
                        self.extensions['extensions'].append(e.extension_domain(self.dcd, self.structure, self.df['timeInS'], self.df['F'], str(coord[i])
                                                                            , str(coord[i+1]))[0])
                
        except:

            if not self.coords.get():
                start_, end_, self.structure= e.e2e_PDB(self.pdb)
                self.entry_coords.insert(0, start_ + '-' + end_)
                tkinter.messagebox.showinfo("information ", "extension from end to end will be computed"+'\n'+"Please, press 'Compute extensions' button again")
                self.export_extensions.grid(row=45 , column=1)
            else:
                coords1=self.coords.get()
                coord=coords1.split('-')
                coord=','.join(coord)
                coord=list(coord.split(','))
                
     
                e2e, self.timeInS, self.force, self.coords_atoms =e.extension_domain(self.dcd, self.structure, self.df['timeInS'],self.df['F'], str(coord[0]), str(coord[1]) )
                for i in range(0, len(coord)-1, 2):
                    self.extensions['extensions'].append(e.extension_domain(self.dcd, self.structure, self.df['timeInS'], self.df['F'], str(coord[i])
                                                                        , str(coord[i+1]))[0])
                
                
            
            
            tkinter.messagebox.showinfo("information ", "extensions have been computed successfully")
        self.export_extensions.grid(row=45 , column=1)
        
        
        
        self.button_plot_force_extension.grid(row=15 , column=1)
        self.button_plot_extension_time.grid(row=20 , column=1)
        self.plot_extension_force()

        self.plot_derivative_from_extension()
        end=time.time()
        print('extension computing: ', round(end-start, 2), ' seconds')
    
# =============================================================================
#   Export information  
# =============================================================================
    def export_extensions_force_time(self):
        """
        exports the extension, force and time data in a csv file

        Returns
        -------
        None.

        """
        d={'extensions':[]}
        dico={'force [pN]':[], 'timeInS':[]}
        coords1=self.coords.get()
        coord=coords1.split('-')
        coord=','.join(coord)
        coord=list(coord.split(','))
        length=len(self.extensions['extensions'][0])
        for i in range(len(self.extensions['extensions'])):
            d['extensions']=self.extensions['extensions'][i]
        dico['timeInS']=self.timeInS[0:length]
        dico['force [pN]']=self.df['F'][0:length]
        df=pd.DataFrame.from_dict(d)
        df.loc[:,'timeInS'] = pd.Series(dico['timeInS'])
        df.loc[:,'force [pN]'] = pd.Series(dico['force [pN]'])
        for i in range(len(self.extensions['extensions'])):
            export_file_path = filedialog.asksaveasfilename(defaultextension='.csv')
            df.to_csv(export_file_path, sep='\t', index = False, header=True)
       
# =============================================================================
#      extension   plots
# =============================================================================
    def plot_extension_force(self):
        """
        This function plot the extension vs force

        Returns
        -------
        None.

        """
        start=time.time()

        try:
            self.extensions
        except:
            tkinter.messagebox.showinfo("Warning ", "Please, start by computing the extensions")

        else:
            if self.widget:
                self.widget.destroy()
            if self.toolbar:
                self.toolbar.destroy()
            figure = Figure(figsize=(7,5), dpi=100)
            ax= figure.add_subplot(111)
            canvas = FigureCanvasTkAgg(figure, master= self.plot_frame_tab3)
            canvas.draw()
            self.widget= canvas.get_tk_widget()
            self.widget.grid(row=10, column=1)
            # navigation toolbar
            toolbarFrame = Frame(master= self.plot_frame_tab3)
            toolbarFrame.grid(row=11,column=1)
            self.toolbar = NavigationToolbar2Tk(canvas, 
                                               toolbarFrame) 
            self.toolbar.update()
            ax.set_xlabel('extension [nm]')
            ax.set_ylabel('force [pN]')
            if self.radiobutton_g_tab2.get()==1:
                ax.grid()
            
            
            if self.coords.get():

                #print("force", len(self.force))
                li= len(self.force)
                l=len(self.extensions['extensions'][0])
                
                if l ==li :
                    coords1=self.coords.get()
                    coord= coords1.split(',')
                    colors={'colors':  ['darkblue' , 'chocolate' ,  'green' ,'red' , 'purple', 'gold', 'gray', 'salmon' ,'yellowgreen' , 'brown']}
                
                
                
                    for i in range(len(self.extensions['extensions'])):
                        
                     #   print("extension", len(self.extensions['extensions'][i]))

                        ax.scatter( self.extensions['extensions'][i], self.force, s=10, color= colors['colors'][i], label= 'Coords  '+str(coord[i]))
                   
                    ax.legend() 
                    
                else:
                    coords1=self.coords.get()
                    coord= coords1.split(',')
                    colors={'colors':  ['darkblue' , 'chocolate' ,  'green' ,'red' , 'purple', 'gold', 'gray', 'salmon' ,'yellowgreen' , 'brown']}
                
                
                
                    for i in range(len(self.extensions['extensions'])):
                        l=len(self.extensions['extensions'][i])

                       # print("extension", len(self.extensions['extensions'][i]))

                        ax.scatter( self.extensions['extensions'][i], self.force[0: l], s=10, color= colors['colors'][i], label= 'Coords  '+str(coord[i]))
                   
                    ax.legend()
                    
                    
                    

            self.radiobutton_g_tab2.set(0)

        end= time.time()
        print('plot extension vs force ', round(end-start, 2) , ' seconds')
    
    def plot_extension_vs_time(self):
        """
        This function plot the extension vs time
        
        Returns
        -------
        None.

        """        
        start=time.time()
        try:
            self.extensions
        except:
            tkinter.messagebox.showinfo("Warning ", "Please start by computing the extensions ")
        else:
            if self.widget:
                self.widget.destroy()
            if self.toolbar:
                self.toolbar.destroy()
                
            figure = Figure(figsize=(7,5), dpi=100)
            ax= figure.add_subplot(111)
            canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_tab3)
            canvas.draw()
            self.widget= canvas.get_tk_widget()
            self.widget.grid(row=10, column=1)
            # navigation toolbar
            toolbarFrame = Frame(master= self.plot_frame_tab3)
            toolbarFrame.grid(row=11,column=1)
            self.toolbar = NavigationToolbar2Tk(canvas, 
                                           toolbarFrame) 
            self.toolbar.update()
            ax.set_xlabel('time [s]')
            ax.set_ylabel('extension [nm]')
            
            if self.radiobutton_g_tab2.get()==1:
                ax.grid()
            if self.coords.get():
                colors={'colors':  ['darkblue' , 'chocolate' ,  'green' ,'red' , 'purple', 'gold', 'gray', 'salmon' ,'yellowgreen' , 'brown']}
                coords1=self.coords.get()
                coord= coords1.split(',')
                l=len(self.extensions['extensions'][0])
                
                
             #   step=int(len(self.timeInS)/len(self.extensions['extensions']))
             #   if step ==0:
               #     step=1
              #  for i in range(0, len(self.timeInS), step):
                    
                for i in range(len(self.extensions['extensions'])):
                    ax.plot(self.timeInS[0 : l], self.extensions['extensions'][i], color=colors['colors'][i], label='Coords'+str(coord[i]))
                ax.legend(loc= "upper left")
            self.radiobutton_g_tab2.set(0)
        end=time.time()
        print('extension vs timeStep ', round(end-start, 2), ' seconds ')
        
# =============================================================================
#  tab:     peak detection from the extension ==> buttons and functions 
# =============================================================================
    def create_buttons_detection_from_extension(self):
        """
        Creates all the buttons of the 'peak detection from 
        extension' tab

        Returns
        -------
        None.

        """
        self.height=DoubleVar()
        self.smoothing_extension=IntVar()
        self.number=IntVar()


        label=Label(self.subframe_tab_2nd_detection, text='Give value for smoothing window', font=("Courrier", 10),width=30)  
        label.grid(row=2, column=1)
        entry_window=Entry(self.subframe_tab_2nd_detection, textvariable=self.smoothing_extension, width=30, font=("Courrier", 10))  
        entry_window.grid(row=3, column=1)
        
        label=Label(self.subframe_tab_2nd_detection, text='Give height threshold', font=("Courrier", 10),width=30)  
        label.grid(row=4, column=1)
                
        entry_height=Entry(self.subframe_tab_2nd_detection, textvariable=self.height,width=30, font=("Courrier", 10))  
        entry_height.grid(row=5, column=1)
        
        entry=Entry(self.subframe_tab_2nd_detection, textvariable=self.number,font=("Courrier", 10),width=30)
        entry.grid(row=7, column=1)
        label=Label(self.subframe_tab_2nd_detection, text= 'or  Give N number of peaks',font=("Courrier", 10),width=30)
        label.grid(row=6, column=1)

        b=Button(self.subframe_tab_2nd_detection, text='1- plot derivative of extension',  font=("Courrier", 10),width=30,
                 command=self.plot_derivative_from_extension)
        b.grid(row=9, column=1)
        
        # button=Button(self.subframe_tab_2nd_detection, text='See RMS of each derivative', font=("Courrier", 10),width=30, command=self.give_RMS_derivative)
        # button.grid(row=8, column=1)
        

       # self.check_rms_button=Button(self.subframe_tab_2nd_detection, text='Plot the RMS threshold', font=("Courrier", 10),width=30, command=self.check_rms_level)

        self.button_detect_peaks_from_extension=Button(self.subframe_tab_2nd_detection, text='2- Detect peaks', font=("Courrier", 10),width=30,
                 command=self.plot_peaks_derivative)

        reset=Button(self.subframe_tab_2nd_detection, text= 'Reset', font=("Courrier", 10),width=30, command=self.delete_extension_peaks_table)
        reset.grid(row=10, column=1)
        self.label_help_tab=Label(self.subframe_tab_2nd_detection, text='?',  font=("Courrier", 10),width=30, bg='lightblue')

        self.add_peak_button=Button(self.subframe_tab_2nd_detection, text='Save selected peaks', font=("Courrier", 10),width=30, command=self.add_peak_to_list)

        self.plot_selected_peaks=Button(self.subframe_tab_2nd_detection, text='3- Plot selected peaks on force-time curve', font=("Courrier", 10),width=30,
                 command=self.keep_selected_peaks)

        self.button_checklist=Button(self.subframe_tab_2nd_detection, text='Check list of all peaks', width=30, font=("Courrier", 10), command=self.list_total_peaks)
        self.link_checklist = Label(self.subframe_tab_2nd_detection, text="?", font=("Courrier", 10), bg='lightblue', cursor="hand2") 
        self.link_checklist.bind("<Enter>", self.on_enter_checklist)
        self.link_checklist.bind("<Leave>", self.on_leave_checklist)


        self.delete_from_list=Button(self.subframe_tab_2nd_detection, text= 'Delete selected peak(s)', width=30, font=("Courrier", 10), command=self.delete_from_total_list)
        
        self.button_export_2nd_detection=Button(self.subframe_tab_2nd_detection, text='export data' , font=("Courrier", 10),width=30, command=self.export_slopes_second_detection)


    def delete_extension_peaks_table(self):
        """
        This function resets the treeview of peak detection
        from the extension

        Returns
        -------
        None.

        """
        self.tab.delete(*self.tab.get_children())
        self.number.set(0)
        self.smoothing_extension.set(0)
        self.height.set(0)
        
        
    def on_enter_tab(self, event):
        """
        Information appear when the user hovers over the label '?'

        Parameters
        ----------
        event : string
            Enter event (when the user hovers over the label).

        Returns
        -------
        None.

        """
        self.label_help_tab.configure(text="Click to select peaks"+'\n'+ 'Click on colums to sort ', bg='white', font='Helvetica 8 bold')

    def on_leave_tab(self, enter):
        """
        The label '?' appears again once the user moves the mouse

        Parameters
        ----------
        enter : TYPE
            DESCRIPTION.

        Returns
        -------
        None.

        """
        self.label_help_tab.configure(text="?", bg='lightblue')

        
    def treeview_sort_column(self, tv, col, reverse):
        """
        This function sorts the values of each column of the 
        treeview

        Parameters
        ----------
        tv : treeview
            the treeview of peaks from 1st detection, 2nd detection
            and refolding detection .
        col : treeview column

        reverse : bool
            false if ascending sort.

        Returns
        -------
        None.

        """
        l = [(tv.set(k, col), k) for k in tv.get_children('')]
        l.sort(reverse=reverse)
        for index, (val, k) in enumerate(l):
            tv.move(k, '', index)
   
    
   
# =============================================================================
#         plot the derivative of extension
# =============================================================================
        
    def plot_derivative_from_extension(self):
        """
        plots the derivative of the extension
        - The function that calculates the derivative is called
        from extension.py file

        Returns
        -------
        None.

        """
        start=time.time()
        if not self.coords.get():
            tkinter.messagebox.showinfo("Warning ", "Please set coordinates ")
        try:
            self.extensions
        except:
            tkinter.messagebox.showinfo("Warning ", "Please, go to 'extensions' tab and press the button 'compute the extensions' ")
        else:
            if self.widget_2nd_detection:
                self.widget_2nd_detection.destroy()

            if self.toolbar_2nd_detection:
                self.toolbar_2nd_detection.destroy()
                

            figure = Figure(figsize=(6,5), dpi=100)
            ax= figure.add_subplot(111)
            canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_2nd_detection)
            canvas.draw()
            self.widget_2nd_detection= canvas.get_tk_widget()
            self.widget_2nd_detection.grid(row=10, column=1)
            # navigation toolbar
            toolbarFrame = Frame(master= self.plot_frame_2nd_detection)
            toolbarFrame.grid(row=11,column=1)
            self.toolbar_2nd_detection = NavigationToolbar2Tk(canvas, 
                                           toolbarFrame) 
            self.toolbar_2nd_detection.update()
            ax.set_xlabel('time [s]')
            ax.set_ylabel('height')
            

            
            if self.radiobutton_g_tab2.get()==1:
                ax.grid()
            if not self.smoothing_extension.get():

                colors={'colors':  ['darkblue' , 'chocolate' ,  'green' ,'red' , 'purple', 'gold', 'gray', 'salmon' ,'yellowgreen' , 'brown']}
                self.derivative={'derivative':[]}
                self.x, derivative, ts, self.factor=e.compute_derivative(self.extensions['extensions'][0], self.timeInS)
                length_extensions_list=len(self.extensions['extensions'])
                
                coords1=self.coords.get()
                coord= coords1.split(',')
                
                for i in range(length_extensions_list):

                    self.derivative['derivative'].append(e.compute_derivative(self.extensions['extensions'][i], self.timeInS)[1])

                l=len(self.derivative['derivative'][0])
                li=len(self.x)

                for i in range(len(self.derivative['derivative'])):
                    ax.plot(self.x, self.derivative['derivative'][i][0:li], color=colors['colors'][i], label='Coords '+coord[i])
                ax.legend()

                
            else:
                colors={'colors':  ['darkblue' , 'chocolate' ,  'green' ,'red' , 'purple', 'gold', 'gray', 'salmon' ,'yellowgreen' , 'brown']}
                window=self.smoothing_extension.get()
                self.derivative={'derivative':[]}
                d={ 'smooth': []}
                self.x, derivative, ts, self.factor=e.compute_derivative(self.extensions['extensions'][0], self.timeInS)
                length_extensions_list=len(self.extensions['extensions'])
                
                coords1=self.coords.get()
                coord= coords1.split(',')
                for i in range(length_extensions_list):

                    self.derivative['derivative'].append(e.compute_derivative(self.extensions['extensions'][i], self.timeInS)[1])
                for i in self.derivative['derivative']:
                    d['smooth'].append(gaussian_filter(i, window))

                l=len(self.derivative['derivative'][0])
                li=len(self.x)
                
                for i in range(len(d['smooth'])):
                    ax.plot(self.x,d['smooth'][i][0:li], color=colors['colors'][i], label='Coords '+str(coord[i]))
               #     print(coord[i])
            #pack plot peak of the derivative button
            self.button_detect_peaks_from_extension.grid(row=12 , column=1)
            self.plot_selected_peaks.grid(row=13 , column=1)
            ax.legend()
            self.radiobutton_g_tab2.set(0)
            end=time.time()
            print('plot derivative of extension ', round(end-start, 2), ' seconds')
# =============================================================================
#     plot selected peaks 
# =============================================================================
       
    def keep_selected_peaks(self):
        """
        gets selected peaks from the treeview and plots 
        them on force -time curve


        Returns
        -------
        None.

        """
        try:
            self.peaks
        except:
           # tkinter.messagebox.showinfo("Warning ", "Please, it is advisable to do a first peak detection before this step "+'\n'+ "if you do not want to do a first detection click again on the button")
            self.peaks=[]
        else:
            if self.widget_2nd_detection:
                self.widget_2nd_detection.destroy()
                
            if self.toolbar_2nd_detection:
                self.toolbar_2nd_detection.destroy()
                
            figure = Figure(figsize=(6,5), dpi=100)
            ax= figure.add_subplot(111)
            canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_2nd_detection)
            canvas.draw()
            self.widget_2nd_detection= canvas.get_tk_widget()
            self.widget_2nd_detection.grid(row=10, column=1)
            
            # navigation toolbar
            toolbarFrame = Frame(master=  self.plot_frame_2nd_detection)
            toolbarFrame.grid(row=11,column=1)
            self.toolbar_2nd_detection= NavigationToolbar2Tk(canvas, 
                                           toolbarFrame) 
            self.toolbar_2nd_detection.update()
            ax.set_xlabel('time [s]')
            ax.set_ylabel('force [pN]')
            if self.radiobutton_g_tab2.get() == 1:
                ax.grid()
            colors={1: 'darkblue' , 2: 'chocolate' , 3: 'green' , 4 :'red' , 5:'purple', 6:'gold', 7: 'gray', 8:'salmon' , 9:'yellowgreen' , 10:'brown'}
            coords1=self.coords.get()
            coord= coords1.split(',')

            ts= list(self.df['timeInS'])
            for i in range(len(ts)):
                ts[i]= "{:.2e}".format(ts[i])
                ts[i]=float(ts[i])

            curItem= self.tab.selection()
            curItems = [(self.tab.item(i)['values']) for i in curItem]
        #    ts=self.df['timeInS']
            try:
                if self.force_tcl_tab2.get() ==1:
                    self.df['-F']
                    f=self.df['-F']
                    d={'real_peak':[], 'selected_peaks': [], 'timeInS': [], 'F': [], 'Residue selection':[]} 
                    selection=[]
                    sel=[]
                    for i in curItems:
                        d['Residue selection'].append(i[2])
                        selection.append(float(i[0]))
        
                    for i in range(len(selection)):
                        sel.append(np.array(ts).flat[np.abs( np.array(ts) - selection[i]).argmin()])
                        d['selected_peaks'].append(list(ts).index(sel[i]))
        
                    ax.plot(ts, f)
        
                    for i in range(len(d['selected_peaks'])):
                        ax.plot(ts[int(d['selected_peaks'][i])], f[int(d['selected_peaks'][i])], 'o',  mew=5, ms=6, label='Coords '+str(coord[int(d['Residue selection'][i]-1)]), color=colors[d['Residue selection'][i]])
                        
                    handles, labels = figure.gca().get_legend_handles_labels()
                    by_label = dict(zip(labels, handles))
                    ax.legend(by_label.values(), by_label.keys())
                else:
                    f=self.df['F']
                    d={'real_peak':[], 'selected_peaks': [], 'timeInS': [], 'F': [], 'Residue selection':[]} 
                    selection=[]
                    sel=[]
                    for i in curItems:
                        d['Residue selection'].append(i[2])
                        selection.append(float(i[0]))
        
                    for i in range(len(selection)):
                        sel.append(np.array(ts).flat[np.abs( np.array(ts) - selection[i]).argmin()])
                        d['selected_peaks'].append(list(ts).index(sel[i]))
        
                    ax.plot(ts, f)
        
                    for i in range(len(d['selected_peaks'])):
                        ax.plot(ts[int(d['selected_peaks'][i])], f[int(d['selected_peaks'][i])], 'o',  mew=5, ms=6, label='Coords '+str(coord[int(d['Residue selection'][i]-1)]), color=colors[d['Residue selection'][i]])
                        
                    handles, labels = figure.gca().get_legend_handles_labels()
                    by_label = dict(zip(labels, handles))
                    ax.legend(by_label.values(), by_label.keys())
                    
            except:
                f=self.df['F']
                d={'real_peak':[], 'selected_peaks': [], 'timeInS': [], 'F': [], 'Residue selection':[]} 
                selection=[]
                sel=[]
                for i in curItems:
                    d['Residue selection'].append(i[2])
                    selection.append(float(i[0]))
    
                for i in range(len(selection)):
                    sel.append(np.array(ts).flat[np.abs( np.array(ts) - selection[i]).argmin()])
                    d['selected_peaks'].append(list(ts).index(sel[i]))
    
                ax.plot(ts, f)
    
                for i in range(len(d['selected_peaks'])):
                    ax.plot(ts[int(d['selected_peaks'][i])], f[int(d['selected_peaks'][i])], 'o',  mew=5, ms=6, label='Coords '+str(coord[int(d['Residue selection'][i]-1)]), color=colors[d['Residue selection'][i]])
                    
                handles, labels = figure.gca().get_legend_handles_labels()
                by_label = dict(zip(labels, handles))
                ax.legend(by_label.values(), by_label.keys())
            
                
                
            
            self.peaks=list(self.peaks)
            self.radiobutton_g_tab2.set(0)


# =============================================================================
# RMS
# =============================================================================
    def give_RMS_derivative(self):
        """
        This function calculates the RMS level of the derivative
        of extension, shows it to the user with in a pop up window
        The user either sets the RMS level or cancels this step

        Returns
        -------
        None.

        """
        start=time.time()
       # self.check_rms_button.grid(row=14, column=1)
        if not self.coords.get():
            tkinter.messagebox.showinfo("Warning ", "Please set coordinates ")
        try:
            self.extensions
        except:
            tkinter.messagebox.showinfo("Warning ", "Please, go to 'extensions' tab and press the button 'compute the extensions' ")
        else:
            if self.widget_2nd_detection:
                self.widget_2nd_detection.destroy()
            if self.toolbar_2nd_detection:
                self.toolbar_2nd_detection.destroy()
                
            figure = Figure(figsize=(6,5), dpi=100)
            ax= figure.add_subplot(111)
            canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_2nd_detection)
            canvas.draw()
            self.widget_2nd_detection= canvas.get_tk_widget()
            self.widget_2nd_detection.grid(row=10, column=1)
            # navigation toolbar
            toolbarFrame = Frame(master= self.plot_frame_2nd_detection)
            toolbarFrame.grid(row=11,column=1)
            self.toolbar_2nd_detection = NavigationToolbar2Tk(canvas, 
                                           toolbarFrame) 
            self.toolbar_2nd_detection.update()
            ax.set_xlabel('time [s]')
            ax.set_ylabel('height')
            if self.radiobutton_g_tab2.get() == 1:
                ax.grid()

            if not self.smoothing_extension.get():
                colors={'colors':  ['darkblue' , 'chocolate' ,  'green' ,'red' , 'purple', 'gold', 'gray', 'salmon' ,'yellowgreen' , 'brown']}
                self.derivative={'derivative':[]}
                self.x, derivative, ts, self.factor=e.compute_derivative(self.extensions['extensions'][0], self.timeInS)
                coords1=self.coords.get()
                coord= coords1.split(',')
                
                length_extensions_list=len(self.extensions['extensions'])
                for i in range(length_extensions_list):
                    self.derivative['derivative'].append(e.compute_derivative(self.extensions['extensions'][i], self.timeInS)[1])
                    
                self.d={'coeff_RMS_threshold':[], 'RMS':[], 'avg':[]}
                coef_rms=len(self.derivative['derivative'][0])* 0.05
                for i in range(len(self.derivative['derivative'])):
                    self.d['RMS'].append(get_RMS(self.derivative['derivative'][i][0:int(coef_rms)]))
                    self.d['avg'].append(np.mean(self.derivative['derivative'][i]))

                l=len(self.derivative['derivative'][0])
                
                for i in range(len(self.derivative['derivative'])):
                    ax.plot(self.x[0:l], self.derivative['derivative'][i], color=colors['colors'][i], label='Coords '+str(coord[i]))
                    ax.legend()
                    self.d['coeff_RMS_threshold'].append(simpledialog.askstring(title="Factor", prompt="RMS + Average =  "+ str(round(self.d['RMS'][i]+self.d['avg'][i],2))+ '\n'+ 'Please, give a value for the coefficient'+'\n'+'RMS* Coefficient+ Average', initialvalue='1'))
                
                for i in range(len(self.d['coeff_RMS_threshold'])):
                    if self.d['coeff_RMS_threshold'][i]== None:
                        self.d['coeff_RMS_threshold'][i]=1
               

                
            else:
                colors={'colors':  ['darkblue' , 'chocolate' ,  'green' ,'red' , 'purple', 'gold', 'gray', 'salmon' ,'yellowgreen' , 'brown']}
                
                coords1=self.coords.get()
                coord= coords1.split(',')
                window=self.smoothing_extension.get()
                self.derivative={'derivative':[]}
                self.d={ 'smooth': [], 'coeff_RMS_threshold':[], 'RMS':[], 'avg':[]}
                self.x, derivative, ts, self.factor=e.compute_derivative(self.extensions['extensions'][0], self.timeInS)
                length_extensions_list=len(self.extensions['extensions'])
                for i in range(length_extensions_list):

                    self.derivative['derivative'].append(e.compute_derivative(self.extensions['extensions'][i], self.timeInS)[1])
                for i in self.derivative['derivative']:
                    self.d['smooth'].append(gaussian_filter(i, window))
                rms_coeff=int(len(self.d['smooth'][0]*0.05))
                for i in range(len(self.d['smooth'])):
                    self.d['avg'].append(np.mean(self.d['smooth'][i]))
                    self.d['RMS'].append(get_RMS(self.d['smooth'][i][0:rms_coeff]))
                
                l=len(self.derivative['derivative'][0])
                for i in range(len(self.d['smooth'])):
                    ax.plot(self.x[0:l],self.d['smooth'][i], color=colors['colors'][i], label='Coords '+str(coord[i]))
                    ax.legend()
                    self.d['coeff_RMS_threshold'].append(simpledialog.askstring(title="Factor", prompt="RMS + Average =  "+ str(round(self.d['RMS'][i]+self.d['avg'][i],2))+ '\n'+ 'Please, give a value for the coefficient'+'\n'+'RMS* Coefficient+ Average', initialvalue='1'))

                for i in range(len(self.d['coeff_RMS_threshold'])):
                    if self.d['coeff_RMS_threshold'][i]== None:
                        self.d['coeff_RMS_threshold'][i]=1
               
            self.radiobutton_g_tab2.set(0)
        end=time.time()
        print('RMS  ', round(end-start, 2), ' seconds')
        
        
# =============================================================================
#   plots the peaks of the derivative    
# =============================================================================
    def plot_peaks_derivative(self):
        """
        plots the detected peaks on the derivative of extension

        Returns
        -------
        None.

        """
        start=time.time()
        if not self.coords.get() :
            tkinter.messagebox.showinfo("Warning ", "Please set coordinates of selections ")
        try:
            self.derivative
        except:

            tkinter.messagebox.showinfo("Warning ", "Please follow the steps in the right order, Start by plotting the derivative")
        else:
            if self.widget_2nd_detection:
                self.widget_2nd_detection.destroy()
            if self.toolbar_2nd_detection:
                self.toolbar_2nd_detection.destroy()
            
            li=len(self.x)

            figure = Figure(figsize=(6,5), dpi=100)
            ax= figure.add_subplot(111)
            canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_2nd_detection)
            canvas.draw()
            self.widget_2nd_detection= canvas.get_tk_widget()
            self.widget_2nd_detection.grid(row=10, column=1)
            # navigation toolbar
            toolbarFrame = Frame(master=  self.plot_frame_2nd_detection)
            toolbarFrame.grid(row=11,column=1)
            self.toolbar_2nd_detection = NavigationToolbar2Tk(canvas, 
                                           toolbarFrame) 
            self.toolbar_2nd_detection.update()
            ax.set_xlabel('time [s]')
            ax.set_ylabel('Height')
            li= len(self.x)
            if self.radiobutton_g_tab2== 1:
                ax.grid()
            if not self.number.get():
                colors={'colors':  ['darkblue' , 'chocolate' ,  'green' ,'red' , 'purple', 'gold', 'gray', 'salmon' ,'yellowgreen' , 'brown']}

                height=self.height.get()
                window=self.smoothing_extension.get()
                     
                d={ 'derivative':[], 'smooth': [], 'y':[], 'idx' : []}
                for i in self.derivative['derivative']:
                    
                    d['smooth'].append(gaussian_filter(i, window))
                peaks={'peaks':[] , 'domain': []  }
                properties=[]
                l=len(self.derivative['derivative'][0])
                start_peak=[]
                coords1=self.coords.get()
                coord= coords1.split(',')
            #find peaks
                for i in range(len(d['smooth'])):
                    ax.plot(self.x,d['smooth'][i][0:li], color=colors['colors'][i], label='Coords '+str(coord[i]))
                    peaks['peaks'].append( signal.find_peaks(d['smooth'][i], height=height)[0])
                    peaks['domain'].append(i+1)

                list_all_heights=[]
                for i in range(len(peaks['peaks'])):
                    for j in range(len(peaks['peaks'][i])):
                        list_all_heights.append(d['smooth'][i][peaks['peaks'][i][j]])

                halves=[]
                for height in list_all_heights:
                    halves.append(height/2)
                self.tab.delete(*self.tab.get_children())
                self.tab.heading('#2', text='time [s]')
                self.tab.heading('#3', text='derivative at peak') 
                self.x = np.array(self.x) 
                
                peaks['peaks'] = [x for l in peaks['peaks'] for x in l]
                peaks['peaks']= sorted(peaks['peaks'])
                peaks['peaks']= np.append(0, peaks['peaks'])
                for i in range(len(peaks['peaks'])):
                    for j in range(len(d['smooth'])):
                        half = d['smooth'][j][peaks['peaks'][i]] / 2 
                        d['smooth'][j]= np.array(d['smooth'][j])

                        roots=find_roots(self.x[peaks['peaks'][i-1]:peaks['peaks'][i]], d['smooth'][j][peaks['peaks'][i-1]:peaks['peaks'][i]] - half)
                        if len(roots) > 0 and half in halves:
                            ax.scatter(roots[-1], half, color='red')
                            self.tab.insert('',i ,values=("{:.2e}".format(roots[-1]), "{:.2e}".format(half), peaks['domain'][j])) 

                for col in self.colnames:
                    self.tab.heading(col, text=col, command=lambda _col=col: self.treeview_sort_column(self.tab, _col, False))
                self.tab.grid(row=20, column=1, sticky=W+E+S+N)
                self.s.grid(row=20, column=1, sticky=E)
                self.label_help_tab.grid(row=20, column=1, sticky= E)
                self.label_help_tab.bind("<Enter>", self.on_enter_tab)
                self.label_help_tab.bind("<Leave>", self.on_leave_tab)
        
                ax.legend()
                
            elif self.number.get():
                colors={'colors':  ['darkblue' , 'chocolate' ,  'green' ,'red' , 'purple', 'gold', 'gray', 'salmon' ,'yellowgreen' , 'brown']}
                coords1=self.coords.get()
                coord= coords1.split(',')
                number=self.number.get()
                window=self.smoothing_extension.get()
                coords1=self.coords.get()
                coord= coords1.split(',')
                d={ 'derivative':[], 'smooth': []}
                for i in self.derivative['derivative']:
                    
                    d['smooth'].append(gaussian_filter(i, window))
                peaks={'peaks':[] , 'domain': [] }
                l=len(self.derivative['derivative'][0])

                properties=[]
            
            #find peaks: USER GIVES N number of highest peaks he want to detect
                for i in range(len(d['smooth'])):
                    ax.plot(self.x,d['smooth'][i][0:li], color=colors['colors'][i],label='Coords '+str(coord[i]))
                    peaks['domain'].append(i+1)
                    properties.append(signal.find_peaks(d['smooth'][i], height=0)[1])
                    
                for i in range(len(d['smooth'])):
                   
                    peaks['peaks'].append(signal.find_peaks(d['smooth'][i], height=0)[0])
                    print("all peaks", peaks['peaks'])
                
                list_all_heights=[]
                for i in range(len(properties)):
                    for j in range(len(properties[i]['peak_heights'])):
                        list_all_heights.append(properties[i]['peak_heights'][j])
                        
                peak={'peak':[], 'domain': [], 'derivative':[]}
                list_all_heights=sorted(list_all_heights)
                list_all_heights= list_all_heights[-number:]
                
# =============================================================================
#                 highest peaks
# =============================================================================
                print('highest selected peaks: ',list_all_heights  )
                # for peak_hight in list_all_heights :
                #     ax.scatter(peak_hight, color='black')

                theset = frozenset(list_all_heights)
                theset = sorted(theset, reverse=True)
                
                #to avoid IndexError
                if len(list_all_heights) < number:
                    number= len(list_all_heights)
                    tkinter.messagebox.showinfo('information' ,' Only '+ str(number) + ' peaks were detected')

                thedict = {}
                for j in range(number):
                    positions = [i for i, x in enumerate(list_all_heights) if x == theset[j]]
                    thedict[theset[j]] = positions
                    print('thedict ', thedict)
                    for i in range(len(properties)):
                        try: 
                            list(properties[i]['peak_heights']).index(theset[j])
                            
                          #  print('1st = ' + str(theset[j]) + ' at ' + str(peaks['peaks'][i][list(properties[i]['peak_heights']).index(theset[j])]))
                            peak['peak'].append(peaks['peaks'][i][list(properties[i]['peak_heights']).index(theset[j])])
                            print('peak index ', peak['peak'])
                          #  ax.scatter(self.x[peak['peak']], self.df['F'][peak['peak']], color='black')

                            peak['domain'].append(i+1)
                        except:
                            l=[]

            #plot peaks
                self.tab.delete(*self.tab.get_children())
                self.tab.heading('#2', text='time [s]')
                self.tab.heading('#3', text='derivative at peak')
                self.x= np.array(self.x)
                
                halves=[]
                
                for height in list_all_heights:
                    if height > 200:
                        halves.append(height/2)
                    else: 
                        halves.append(height)

                peak['peak']= sorted(peak['peak'])
                peak['peak']= np.append(0, peak['peak'])
                for i in range(len(peak['peak'])):
                    for j in range(len(d['smooth'])):
                        half = d['smooth'][j][peak['peak'][i]] / 2 
                        d['smooth'][j]= np.array(d['smooth'][j])

                        roots=find_roots(self.x[peak['peak'][i-1]:peak['peak'][i]], d['smooth'][j][peak['peak'][i-1]:peak['peak'][i]] - half)
                        print('roots ', roots)
                        if len(roots) > 0 and half in halves:
                            ax.scatter(roots[-1], half, color='crimson')
                            self.tab.insert('',i ,values=("{:.2e}".format(roots[-1]), "{:.2e}".format(half), peaks['domain'][j]))


                for col in self.colnames:
                    self.tab.heading(col, text=col, command=lambda _col=col: self.treeview_sort_column(self.tab, _col, False))

                self.tab.grid(row=20, column=1, sticky=W+E+S+N)
                self.s.grid(row=20, column=1, sticky=E)
                self.label_help_tab.grid(row=20, column=1, sticky=E)
                self.label_help_tab.bind("<Enter>", self.on_enter_tab)
                self.label_help_tab.bind("<Leave>", self.on_leave_tab)
                ax.legend()
            self.radiobutton_g_tab2.set(0)
        self.add_peak_button.grid(row=25,column=1)
        self.button_checklist.grid(row=26, column=1)
        self.link_checklist.grid(row=26, column=1, sticky= E)
        self.delete_from_list.grid(row=27,column=1)
        end=time.time()
        print('peak detection ', round(end-start, 2), ' s')
# =============================================================================
# add peaks to list
# =============================================================================
    
    def add_peak_to_list(self):
        """
        puts the selected peak from the 2nd detection (extension approach)
        in a list to save them

        Returns
        -------
        None.

        """
        try:
            self.peaks
        except:
            self.peaks=[]
        else:
            ts= list(self.df['timeInS'])
            for i in range(len(ts)):
                ts[i]= "{:.2e}".format(ts[i])
                ts[i]=float(ts[i])

            sel =[]
            curItem= self.tab.selection()
            curItems = [(self.tab.item(i)['values']) for i in curItem]
            user_selection=[]
            self.selection=[]
            for i in curItems:
                user_selection.append(float(i[0]))
                self.selection.append(i[-1])


            for i in range(len(user_selection)):
                sel.append(np.array(ts).flat[np.abs( np.array(ts) - user_selection[i]).argmin()]) 
                if list(ts).index(sel[i]) not in self.peaks:
                    self.peaks.append(list(ts).index(sel[i]))
                
            tkinter.messagebox.showinfo("informations ", "Added to list successfully")

        self.button_export_2nd_detection.grid(row=30, column=1)
    
    def on_enter_checklist(self, event):
        """
        Gives information about the use of the checklist when the user hovers

        Parameters
        ----------
        event : string
            <Enter>.

        Returns
        -------
        None.

        """
        self.link_checklist.configure(text="When you save the peaks a pop up window will appear"+'\n'+"If the window does not appear click again on the 'save selected peaks' button"+'\n'+"To modify/delete some peaks click on 'Check the list ' button ", bg='white', font='Courrier')

    def on_leave_checklist(self, enter):
        """
        when the user leaves the label becomes ? (like in the initial state)

        Parameters
        ----------
        enter : string
            on leave.

        Returns
        -------
        None.

        """
        self.link_checklist.configure(text="?", bg='lightblue')

    def export_slopes_second_detection(self):
        """
        This function puts the loading rates from the second peak detection 
        (the detection from the extension) in a csv file 

        Returns
        -------
        None.

        """
        tkinter.messagebox.showinfo('information' ,'Please, make sure you save the peaks that you want to export')
        try:
            self.peaks
        except:
            tkinter.messagebox.showinfo('information' ,'Please, save peaks first')
        else:
            dico , slope, intercept=compute_slopes_second_detection(self.path ,self.peaks, self.selection)
            export_file_path = filedialog.asksaveasfilename(defaultextension='.csv')
            dico.to_csv (export_file_path, sep='\t', index = False, header=True)


# =============================================================================
#     Check/delete list of peaks
# =============================================================================

    def list_total_peaks(self):
        """
        This function inserts all the SAVED peaks in the Treeview
        so that the user can check his list
        

        Returns
        -------
        None.

        """
        try:
            self.peaks
        except:
            tkinter.messagebox.showinfo("Warning ", "Please, Save peaks first ")
        else:
            self.ts= self.df['timeInS']
            self.f= self.df['F']
            self.tab.delete(*self.tab.get_children())
            self.tab.heading('#1', text='time [s]')
            self.tab.heading('#2', text='force [pN]')
            self.tab.heading('#3', text='index in total list')

            for i in range(len(self.peaks)):
                self.tab.insert('',i ,values=( "{:.2e}".format( self.ts[self.peaks[i]]), "{:.2e}".format(self.f[self.peaks[i]]), self.peaks[i]))
            self.tab.grid(row=20, column=1, sticky=W+E+S+N)
            self.s.grid(row=20, column=2, sticky="ns")
            
    def delete_from_total_list(self):
        """
        This function deletes the peak selected by the user
        from the total list of peaks

        Returns
        -------
        None.

        """
        curItem= self.tab.selection()
        curItems = [(self.tab.item(i)['values']) for i in curItem]
        for i in curItems:
            self.peaks.remove(i[2])
        self.tab.delete(*self.tab.get_children())
        self.tab.heading('#1', text='time [s]')
        self.tab.heading('#2', text='force [pN]')
        for i in range(len(self.peaks)):
            self.tab.insert('',i ,values=(  "{:.2e}".format(self.ts[self.peaks[i]]), "{:.2e}".format(self.f[self.peaks[i]]), self.peaks[i]))
        self.tab.grid(row=20, column=1, sticky=W+E+S+N)
        self.s.grid(row=20, column=2, sticky="ns")



# =============================================================================
#    plot the RMS crossline 
# =============================================================================
    def check_rms_level(self):
        """
        This function plots the RMS threshold set by 
        the user on the derivative curve.
        Intersection pts appear on the table
        The user can select/save them or plot them
        on the force vs time curve

        Returns
        -------
        None.

        """
        if not self.coords.get():
            tkinter.messagebox.showinfo("Warning ", "Please set coordinates ")
        try:
            self.extensions
        except:
            tkinter.messagebox.showinfo("Warning ", "Please, go to 'extensions' tab and press the button 'compute the extensions' ")
        else:
            if self.widget_2nd_detection:
                self.widget_2nd_detection.destroy()

            if self.toolbar_2nd_detection:
                self.toolbar_2nd_detection.destroy()

            figure = Figure(figsize=(6,5), dpi=100)
            ax= figure.add_subplot(111)
            canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_2nd_detection)
            canvas.draw()
            self.widget_2nd_detection= canvas.get_tk_widget()
            self.widget_2nd_detection.grid(row=10, column=1)
                # navigation toolbar
            toolbarFrame = Frame(master= self.plot_frame_2nd_detection)
            toolbarFrame.grid(row=11,column=1)
            self.toolbar_2nd_detection = NavigationToolbar2Tk(canvas, toolbarFrame)
            self.toolbar_2nd_detection.update()
            ax.set_xlabel('time [s]')
            ax.set_ylabel('height')
            if self.radiobutton_g_tab2.get() == 1:
                ax.grid()
            colors={'colors':  ['darkblue' , 'chocolate' ,  'green' ,'red' , 'purple', 'gold', 'gray', 'salmon' ,'yellowgreen' , 'brown']}
            window=self.smoothing_extension.get()
            l=len(self.derivative['derivative'][0])
            d={ 'derivative':[], 'smooth': [], 'y':[], 'idx':[]} #idx pts d 'intersection
            coords1=self.coords.get()
            coord= coords1.split(',')
            for i in range(len(self.derivative['derivative'])):
                d['y'].append(np.zeros(len(self.derivative['derivative'][0]))+float(self.d['RMS'][i])*(float(self.d['coeff_RMS_threshold'][i]))+float(self.d['avg'][i]))

            self.tab.delete(*self.tab.get_children())
            self.tab.heading('#2', text='time [s]')
            self.tab.heading('#3', text='derivative at peak')    
            
                
            for i in self.derivative['derivative']:
                d['smooth'].append(gaussian_filter(i, window))
            for i in range(len(d['smooth'])):
                ax.plot(self.x[0:l],d['smooth'][i], color=colors['colors'][i], label='Coords '+str(coord[i]))
                ax.plot(self.x[0:l], d['y'][i])
                d['idx'].append(np.argwhere(np.diff(np.sign(d['y'][i]-d['smooth'][i]))).flatten())
                ax.plot(self.x[d['idx'][i]], d['y'][i][d['idx'][i]], 'ko')
                #print(d['idx'][i][0])
                ax.legend()                
                
                for j in range(len(d['idx'][i])):
                    if j% 2 ==  0:          #to take only intersection on the left side 
                        self.tab.insert('',i ,values=( "{:.2e}".format(self.x[d['idx'][i][j]]), "{:.2e}".format(d['y'][i][d['idx'][i][j]]), i+1))

            for col in self.colnames:
                self.tab.heading(col, text=col, command=lambda _col=col: self.treeview_sort_column(self.tab, _col, False))
            
            self.tab.grid(row=20, column=1, sticky=W+E+S+N)
            self.s.grid(row=20, column=2, sticky="ns")
            self.label_help_tab.grid(row=20, column=1, sticky=E)
            self.label_help_tab.bind("<Enter>", self.on_enter_tab)
            self.label_help_tab.bind("<Leave>", self.on_leave_tab)
            
            for i in range(len(d['idx'])):
                self.d['coeff_RMS_threshold'][i]=simpledialog.askstring(title="Factor", prompt="RMS + Average =  "+ str(round(self.d['RMS'][i]+self.d['avg'][i],2))+ '\n'+ 'Please, if you want to keep the same coefficient at selection '+str(i+1)+' press "Cancel"' +'\n'+'RMS* Coefficient+ Average', initialvalue=str(self.d['coeff_RMS_threshold'][i]))

            for i in range(len(d['idx'])):
                if len(d['idx'][i]) > 40:
                    self.d['coeff_RMS_threshold'][i]=simpledialog.askstring(title="Warning", prompt="RMS + Average =  "+ str(round(self.d['RMS'][i]+self.d['avg'][i],2))+ '\n'+ 'Please, give a heigher coefficient, you have too many peaks at selection '+str(i+1)+'\n'+'RMS* Coefficient+ Average', initialvalue='1')
                    tkinter.messagebox.showinfo("Information ", "Please, click again on 'plot the threshold RMS level' button ")
            for i in range(len(self.d['coeff_RMS_threshold'])):
                if self.d['coeff_RMS_threshold'][i]== None:
                    self.d['coeff_RMS_threshold'][i]=1
            self.radiobutton_g_tab2.set(0) 
        self.add_peak_button.grid(row=25,column=1)

# =============================================================================
#  tab Refolding peaks buttons and function                 
# =============================================================================
    def create_buttons_refolding(self):
        """
        Creates all the buttons of the 'refolding peaks' tab

        Returns
        -------
        None.
            
        """
        self.smoothing_value= IntVar()
        self.height_value= DoubleVar()
        self.number_peaks_refolding=IntVar()
    
        label=Label(self.subframe_tab_refolding, text='Give smoothing window (Gaussian filter)', width=30)
        label.grid(row=0, column= 0)
        
        entry=Entry(self.subframe_tab_refolding, textvariable=self.smoothing_value, width=30, font=("Courrier", 10))
        entry.grid(row=1, column= 0)
        
        label_height=Label(self.subframe_tab_refolding, text='Give height threshold', width=30, font=("Courrier", 10))
        label_height.grid(row=2, column= 0)
        entry_height=Entry(self.subframe_tab_refolding, textvariable=self.height_value, width=30, font=("Courrier", 10))
        entry_height.grid(row=3, column= 0)
        
        label_n_selection=Label(self.subframe_tab_refolding, text='or Give N number of peaks', width=30, font=("Courrier", 10))
        label_n_selection.grid(row=4, column= 0)
        entry_n_selection=Entry(self.subframe_tab_refolding, textvariable=self.number_peaks_refolding, width=30, font=("Courrier", 10))
        entry_n_selection.grid(row=5, column= 0)
        
        b=Button(self.subframe_tab_refolding, text='1- Detect refolding',font=("Courrier", 10),width=30, command=self.refolding_detection )
        b.grid(row=6 , column= 0)
        
        b=Button(self.subframe_tab_refolding, text='2- plot on force vs time curve',  width=30,
                 font=("Courrier", 10), command=self.plot_refoldng_peaks_F_vs_time)
        b.grid(row=7, column=0)

            
    def plot_refoldng_peaks_F_vs_time(self):
        """
        This function plots the selected refolding peaks
        on the force vs time curve 

        Returns
        -------
        None.

        """
        start=time.time()
        # remove old widgets
        if self.widget_refolding:
            self.widget_refolding.destroy()

        if self.toolbar_refolding:
            self.toolbar_refolding.destroy()
        
        figure = Figure(figsize=(6,5), dpi=100)
        ax=figure.add_subplot(111)
        ax.set_xlabel('time [s]')
        ax.set_ylabel('force [pN]')
        canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_refolding)
        canvas.draw()
        self.widget_refolding= canvas.get_tk_widget()
        self.widget_refolding.grid(row=10, column=1)
        # navigation toolbar
        toolbarFrame = Frame(master= self.plot_frame_refolding)
        toolbarFrame.grid(row=11,column=1)
        self.toolbar_refolding = NavigationToolbar2Tk(canvas, 
                                           toolbarFrame) 
        figure.tight_layout()
        if self.radiobutton_g_tab2.get()==1:
            ax.grid()
            
        coords1=self.coords.get()
        coord= coords1.split(',')
        
        ts= list(self.df['timeInS'])
        for i in range(len(ts)):
            ts[i]= "{:.2e}".format(ts[i])
            ts[i]=float(ts[i])


        f=self.df['F']
        d={'selected_peaks': [], 'F' : [], 'timeInS': [], 'Residue selection': []} #selection is domain in our case
        curItem= self.tab_refolding.selection()
        curItems = [(self.tab_refolding.item(i)['values']) for i in curItem]
        user_selection=[]
        for i in curItems:
            user_selection.append(float(i[0]))
            d['Residue selection'].append(i[2])
        for i in range(len(user_selection)):
            d['selected_peaks'].append(ts.index(user_selection[i]))

        ax.plot(ts, f)
        colors={1: 'darkblue' , 2: 'chocolate' , 3: 'green' , 4 :'red' , 5:'purple', 6:'gold', 7: 'gray', 8:'salmon' , 9:'yellowgreen' , 10:'brown'}
        for i in range(len(d['selected_peaks'])):
            ax.plot(ts[int(d['selected_peaks'][i])], f[int(d['selected_peaks'][i])], 'o',  mew=5, ms=6, label='Coords '+str(coord[d['Residue selection'][i]-1]), color=colors[d['Residue selection'][i]])
            
        handles, labels = figure.gca().get_legend_handles_labels()
        by_label = dict(zip(labels, handles))
        ax.legend(by_label.values(), by_label.keys())

        self.radiobutton_g_tab2.set(0)
        end=time.time()
        print('refolding peaks plotting ', round(end-start, 2), ' seconds ')

        

    def refolding_detection(self):
        """
        This function detects the refolding peaks. It get the height or number of peaks
        set by the user in the entries and detects peaks according to these
        values

        Returns
        -------
        None.

        """
        start=time.time()
        if not self.coords.get():
            tkinter.messagebox.showinfo("Warning ", "Please, set coordinates")
        try:
            self.derivative

        except:
            tkinter.messagebox.showinfo("Warning ", "Please follow the steps in the right order, Start by plotting the derivative ")
        else:
            if self.widget_refolding:
                self.widget_refolding.destroy()

            if self.toolbar_refolding:
                self.toolbar_refolding.destroy()
            
            figure = Figure(figsize=(6,5), dpi=100)
            ax=figure.add_subplot(111)
            ax.set_xlabel('time [s]')
            ax.set_ylabel('Height')
            canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_refolding)
            canvas.draw()
            self.widget_refolding= canvas.get_tk_widget()
            self.widget_refolding.grid(row=10, column=1)
            # navigation toolbar
            toolbarFrame = Frame(master= self.plot_frame_refolding)
            toolbarFrame.grid(row=11,column=1)
            self.toolbar_refolding = NavigationToolbar2Tk(canvas, 
                                           toolbarFrame) 
            figure.tight_layout()
            if self.radiobutton_g_tab2.get() == 1:
                ax.grid()
            if not self.number_peaks_refolding.get():
                colors={'colors':  ['darkblue' , 'chocolate' ,  'green' ,'red' , 'purple', 'gold', 'gray', 'salmon' ,'yellowgreen' , 'brown']}

                window=self.smoothing_value.get()
                coords1=self.coords.get()
                height=self.height_value.get()
                coords1=self.coords.get()
                coord= coords1.split(',')
                d={ 'derivative':[], 'smooth': []}
            
                for i in self.derivative['derivative']:
                    d['smooth'].append(gaussian_filter(i, window))
                peaks={'peaks':[] , 'domain': [] }
                
                l=len(self.derivative['derivative'][0])
                li=len(self.x)
                
                self.tab_refolding.delete(*self.tab_refolding.get_children())
                #find peaks
                for i in range(len(d['smooth'])):
                    ax.plot(self.x,d['smooth'][i][0:li], color=colors['colors'][i], label='Coords '+str(coord[i]))
                    peaks['peaks'].append(signal.find_peaks(-d['smooth'][i], height=height)[0])
                    peaks['domain'].append(i+1)

                for i in range(len(peaks['peaks'])):
                    for j in range(len(peaks['peaks'][i])):
                        ax.plot(self.x[peaks['peaks'][i][j]], d['smooth'][i][peaks['peaks'][i][j]], 'or')
                        self.tab_refolding.insert('',i ,values=("{:.2e}".format(self.x[peaks['peaks'][i][j]]), "{:.2e}".format(d['smooth'][i][peaks['peaks'][i][j]]), peaks['domain'][i]))
                for col in self.colnames:
                    self.tab_refolding.heading(col, text=col, command=lambda _col=col: self.treeview_sort_column(self.tab_refolding, _col, False))
                self.tab_refolding.grid(row=20, column=0, sticky=W+E+S+N)   
                self.scroll.grid(row=20, column=0, sticky=E)
         
                ax.legend()
            elif self.number_peaks_refolding.get():
                number=self.number_peaks_refolding.get()
                window=self.smoothing_value.get()
                colors={'colors':  ['darkblue' , 'chocolate' ,  'green' ,'red' , 'purple', 'gold', 'gray', 'salmon' ,'yellowgreen' , 'brown']}
                coords1=self.coords.get()
                coord= coords1.split(',')
                d={ 'derivative':[], 'smooth': []}
                for i in self.derivative['derivative']:
                    
                    d['smooth'].append(gaussian_filter(i, window))
                peaks={'peaks':[] , 'domain': [] } 
                
                l=len(self.derivative['derivative'][0])
                li=len(self.x)
               
                properties=[]
            
            #find peaks
                for i in range(len(d['smooth'])):
                    ax.plot(self.x,d['smooth'][i][0:li], color=colors['colors'][i], label='Coords '+str(coord[i]))
                    peaks['domain'].append(i+1)
                    properties.append(signal.find_peaks(-d['smooth'][i], height=0)[1])  #height is equal to 0 because we want all heights of all peaks (no height criteria)
                    
                for i in range(len(d['smooth'])):
                   
                    peaks['peaks'].append(signal.find_peaks(-d['smooth'][i], height=0)[0])
                
                list_all_heights=[]
                for i in range(len(properties)):
                    for j in range(len(properties[i]['peak_heights'])):
                        list_all_heights.append(properties[i]['peak_heights'][j])
                          
                peak={'peak':[], 'domain': [], 'derivative':[]}
                list_all_heights=sorted(list_all_heights)

                list_all_heights= list_all_heights[-number:]
                
                theset = frozenset(list_all_heights)
                theset = sorted(theset, reverse=True)
                thedict = {}
             
                #to avoid IndexError : make sure that the total number of heights is not less than the number that the user set
                if len(list_all_heights) < number:
                    number= len(list_all_heights)
                    tkinter.messagebox.showinfo('information' ,' Only '+ str(number) + ' peaks were detected')
   
                for j in range(number):
                    positions = [i for i, x in enumerate(list_all_heights) if x == theset[j]]
                    thedict[theset[j]] = positions
                    for i in range(len(properties)):
                        try: 
                            list(properties[i]['peak_heights']).index(theset[j])
                          #  print('1st = ' + str(theset[j]) + ' at ' + str(peaks['peaks'][i][list(properties[i]['peak_heights']).index(theset[j])]))
                            peak['peak'].append(peaks['peaks'][i][list(properties[i]['peak_heights']).index(theset[j])])
                            peak['domain'].append(i+1)
                        except:
                            l=[]
                #plot peaks and insert them in the table
                self.tab_refolding.delete(*self.tab_refolding.get_children())
                
                for i in range(len(peak['peak'])):
                    self.tab_refolding.insert('',i ,values=( "{:.2e}".format(self.x[peak['peak'][i]]), "{:.2e}".format(d['smooth'][peak['domain'][i]-1][peak['peak'][i]]), peak['domain'][i]))
                    ax.plot(self.x[peak['peak'][i]], d['smooth'][peak['domain'][i]-1][peak['peak'][i]], 'or')
                
                for col in self.colnames:
                    self.tab_refolding.heading(col, text=col, command=lambda _col=col: self.treeview_sort_column(self.tab_refolding, _col, False))
                self.tab_refolding.grid(row=20, column=0, sticky=W+E+S+N)
                self.scroll.grid(row=20, column=0, sticky=E)

                ax.legend()
            self.radiobutton_g_tab2.set(0)
            end=time.time()
            print('refolding peaks detection ', round(end-start, 2), ' seconds ')
    
# =============================================================================
# tab: slopes buttons and functions 
# =============================================================================
    def create_buttons_slopes(self):
        """
        This function creates all the buttons of the slopes tab

        Returns
        -------
        None.

        """
        self.distance=DoubleVar()
        label=Label(self.subframe_tab_slopes, text= 'Set distance in nm')
        label.grid(row=0, column=1)
        ent= Entry(self.subframe_tab_slopes, textvariable=self.distance, width=30, font=("Courrier", 10))
        ent.grid(row=1,column=1)

        b=Button(self.subframe_tab_slopes, text='plot slopes for this distance ',
                 font=("Courrier", 10),width=30, command=self.plot_slopes_at_dist)
        b.grid(row=7 , column=1)

        b=Button(self.subframe_tab_slopes, text='export data ',
                 font=("Courrier", 10),width=30, command=self.export_slopes)
        b.grid(row=10 , column=1)


    def plot_slopes_at_dist(self):
        """
        plots the slopes at the distance
        that the user gives 

        Returns
        -------
        None.

        """
        start=time.time()
        distance=self.distance.get()
        prominence=self.prominence.get()
        try:
            self.peaks
        except:
            tkinter.messagebox.showinfo("Warning ", "Please,  save peaks before plotting the slopes ")
        else:
            if len(self.peaks)==0:
                tkinter.messagebox.showinfo("Warning ", "Your list is empty ! ")

            if self.widget_slopes:
                self.widget_slopes.destroy()

            if self.toolbar_slopes:
                self.toolbar_slopes.destroy()

            figure = Figure(figsize=(6,5), dpi=100)
            ax= figure.add_subplot(111)
            canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_slopes)
            canvas.draw()
            self.widget_slopes= canvas.get_tk_widget()
            self.widget_slopes.grid(row=10, column=1)
            # navigation toolbar
            toolbarFrame = Frame(master= self.plot_frame_slopes)
            toolbarFrame.grid(row=11,column=1)
            self.toolbar_slopes = NavigationToolbar2Tk(canvas, 
                                           toolbarFrame) 
            self.toolbar_slopes.update()
            ax.set_ylabel('Force [pN]')
            ax.set_xlabel('time [s]')
            if self.radiobutton_g_tab2.get()==1:
                ax.grid()
  
            self.peaks=sorted(self.peaks)
            displacement= self.df['displacement']
            ts= self.df['timeInS']
            f= self.df['F']

            d, slope,intercept=get_slopes(displacement, ts, f, self.peaks, distance)
              
            ax.plot(ts, f)

            for i in range(len(d['peaks'])):
                ax.plot(ts[d['index_dis2'][i]:d['peaks'][i]], slope[i]*ts[d['index_dis2'][i]:d['peaks'][i]]+intercept[i],'r')
            
            for i in range(1, len(d['peaks'])):
                if d['index_dis2'][i] < d['peaks'][i-1] :
                    tkinter.messagebox.showinfo("Warning ", "You have two overlapping peaks, please set a small distance")      
            
            
            self.radiobutton_g_tab2.set(0)
        end=time.time()
        print('slopes ', round(end-start, 2), ' seconds')
        
            
    def export_slopes(self):
        """
        This function exports the slopes in the slopes tab

        Returns
        -------
        None.

        """
        distance=self.distance.get()
        displacement= self.df['displacement']
        ts= self.df['timeInS']
        f= self.df['F']
        timeStep= self.df['timeStep']
        knots=self.knots.get()
        df=slopes_at_peaks(displacement, ts, f, self.peaks, distance, timeStep)
        export_file_path = filedialog.asksaveasfilename(defaultextension='.csv')
        df.to_csv(export_file_path, sep='\t',header=True, index=False, mode='w')

    def create_buttons_correlation(self):
            """
            creates all buttons of the last tab: compute correlations between pairs of residues

            Returns
            -------
            None.

            """

            calculate_button = Button(self.subframe_tab_correlation, text="Calculate Correlations", font=("Courrier", 8)
                              ,width=60, command= self.calculate_correlations)
            calculate_button.grid(row=10,column=2)
            
            self.cutoff= IntVar()
            entry_cutoff=Entry(self.subframe_tab_correlation, textvariable=self.cutoff, width=30, font=("Courrier", 10))
            entry_cutoff.delete(0, END)

            entry_cutoff.insert(0, 13)

            entry_cutoff.grid(row=2, column= 2)
            
            label_cutoff = Label(self.subframe_tab_correlation, text="Distance cutoff in angstroms", font=("Courrier", 8)
                              ,width=30)
            label_cutoff.grid(row=1,column=2)
            
            export_button = Button(self.subframe_tab_correlation, text="Export correlation values", font=("Courrier", 8)
                              ,width=30, command=self.export_correlations)
            export_button.grid(row=20,column=2)
            
            
            

            
            
            label_residues = Label(self.subframe_tab_correlation, text="RESIDUES", font=("Courrier", 12)
                               ,width=30)
            label_residues.grid(row=4,column=2)
              
              
            label_residues_start = Label(self.subframe_tab_correlation, text="FROM", font=("Courrier", 8)
                               ,width=30)
            label_residues_start.grid(row=5,column=1)
        
        
            label_residues_end = Label(self.subframe_tab_correlation, text="TO", font=("Courrier", 8)
                               ,width=30)
            label_residues_end.grid(row=5,column=3)
              

                   
            self.residues_start= IntVar()
            entry_residues_start=Entry(self.subframe_tab_correlation, textvariable=self.residues_start, width=30, font=("Courrier", 10))
            entry_residues_start.delete(0, END)
            

            entry_residues_start.grid(row=6, column= 1)
            
                        
                   
            self.residues_end= IntVar()
            entry_residues_end=Entry(self.subframe_tab_correlation, textvariable=self.residues_end, width=30, font=("Courrier", 10))
            entry_residues_end.delete(0, END)


            entry_residues_end.grid(row=6, column= 3)
            
            
            
            
                       
            
            label_frames = Label(self.subframe_tab_correlation, text="FRAMES:", font=("Courrier", 12)
                               ,width=30)
            label_frames.grid(row=7,column=2)
              
              
            label_frames_start = Label(self.subframe_tab_correlation, text="FROM", font=("Courrier", 8)
                               ,width=30)
            label_frames_start.grid(row=8,column=1)
        
        
            label_frames_end = Label(self.subframe_tab_correlation, text="TO", font=("Courrier", 8)
                               ,width=30)
            label_frames_end.grid(row=8,column=3)
              

                   
            self.frames_start= IntVar()
            entry_frames_start=Entry(self.subframe_tab_correlation, textvariable=self.frames_start, width=30, font=("Courrier", 10))
            entry_frames_start.grid(row=9, column= 1)
            
                        
                   
            self.frames_end= IntVar()
            entry_frames_end=Entry(self.subframe_tab_correlation, textvariable=self.frames_end, width=30, font=("Courrier", 10))

            entry_frames_end.grid(row=9, column= 3)
            
            
            
    def calculate_correlations(self):   
        """
        This functions calls the covrk function from residrk file 
        to calculate the correlations between the pairs of residues
        it measures the covariance between the force and distance
        whenever there is a change in the distance between a pair of residue
        how does that impact the force, positive correlations are colored in blue
        and negative correlations are colored in red

        Returns
        -------
        None.

        """
        
    #    sns.set(style="white")

        if self.widget_correlations:
            self.widget_correlations.destroy()

        if self.toolbar_correlations:
            self.toolbar_correlations.destroy()
        
        self.Resnames= self.structure.getResnames()

        self.res_nums= self.structure.getResnums() 
        print(self.res_nums)


        xpos_atom=  self.coords_atoms[:, :, 0]
        ts, atoms= xpos_atom.shape
        xpos_atom= np.reshape(xpos_atom, (atoms, ts))
        
        ypos_atom= self.coords_atoms[:, :, 1]
        ypos_atom= np.reshape(ypos_atom, ( atoms, ts))
        
        zpos_atom= self.coords_atoms[:, :, 2]
        zpos_atom= np.reshape(zpos_atom, (atoms, ts))
        
        force= self.df["F"]
        
        self.res_atoms={}
        
        counter= np.unique(self.res_nums, return_counts=True)
        
        
        #somme=1
        somme =0
        for i in range( len(counter[0])):
            
            self.res_atoms[i]= list(range( somme,    somme+counter[1][i]))

         #   self.res_atoms[i+1]= list(range( somme,    somme+counter[1][i]))
            somme += counter[1][i]
        
        print(self.res_atoms)
        
        self.correlations= corr.covrk(self.res_atoms, xpos_atom, ypos_atom, zpos_atom, force[0: ts], self.cutoff.get(), list(range(self.residues_start.get(), self.residues_end.get())), self.frames_start.get(), self.frames_end.get())
        
        self.data_frame= pd.DataFrame(self.correlations)
        
        
        self.data = self.data_frame.pivot(index=0, columns=1, values=2) 
        print(self.data)
        
                
        resnames= self.structure.getResnames()
        res = [i[0] for i in groupby(resnames)]
                
        
        figure = Figure(figsize=(6,5), dpi=100)
        ax= figure.add_subplot(111)
        canvas = FigureCanvasTkAgg(figure, master=self.plot_frame_correlation)
        canvas.draw()

        # sns.heatmap(self.data, center=0, annot= True,
        #             cmap="RdYlBu", 
        #             linewidths=2.75, 
        #             ax=ax
        #           )
        
        s= sns.heatmap(self.data, center=0,
                    cmap="RdYlBu", 
                    ax=ax )
        
        # s.set_xticks(self.data.index)
        # s.set_xticklabels(self.data.index)

        self.widget_correlations= canvas.get_tk_widget()
        self.widget_correlations.grid(row=2, column=1)
        # navigation toolbar
        toolbarFrame = Frame(master= self.plot_frame_correlation)
        toolbarFrame.grid(row=3,column=1)
        self.toolbar_correlations = NavigationToolbar2Tk(canvas, 
                                       toolbarFrame) 
        self.toolbar_correlations.update()


        ax.set_xlabel('Residues')
        ax.set_ylabel("Residues")

    def export_correlations(self):
        """
        This functions exports the correlations values 

        Returns
        -------
        None.

        """
        export_file_path = filedialog.asksaveasfilename(defaultextension='.csv')
        self.data_frame.to_csv(export_file_path, sep='\t',header=True, index=False, mode='w')


                
        
            
            


app = App()

app.window.mainloop()
