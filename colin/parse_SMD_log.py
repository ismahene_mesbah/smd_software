# -*- coding: utf-8 -*-
"""
Created on Fri Apr 10 19:18:26 2020

@author: Ismahene
"""
import os
#from io import StringIO
import numpy as np
import csv
import pandas as pd
import re



def read_data_csv(csv_file):
    """
    

    Parameters
    ----------
    csv_file : txt tabulated file
        the headers of the file have to be the following: 
            timeStep : time step
            F(x): smd force
            F(-x): tcl force
            velocityInums   : velocity
            direction       : direction of pulling
            kInpNnm: spring contant in pN/nm


    Returns
    -------
    df : pandas df
        dataframe of all information.

    """
    df = pd.read_csv (csv_file, sep='\t')
    return df
    
def extract_info_from_smd_log (log_file):
    
    output_file='log.txt'
    output_energies= 'energies.txt'
    dico={}
    force_tcl=[]
    energy_parms={}
    F_tcl=[]
    with open(log_file,"r") as file_one , open(output_file, "w") as output,  open(output_energies, "w") as outputE :

        patrn = "SMD  "

        for line in file_one:

            if re.search(patrn, line):

                output.write(line)
                
                
            if  re.search('SMD DIRECTION ', line):
                direction=line.split(' ')
                direction=np.array([float(direction[5]),float(direction[6]),float(direction[7])])
                #reshape matrix to be able to do the dot product later
                direction.reshape(3,1)

            if re.search('ENERGY:', line) and 'Info:' not in line:
                outputE.write(line)

            if re.search('SMD VELOCITY', line ):
                velocity=line.split()
                smd_vel=float(velocity[3]) / 2 *10**15*10**-10   #smd velocity in nm/ns

                
                #extract spring constant and convert it 
            if re.search('SMD K' , line ) and 'K2' not in line :
                 smd_k_namd=line.split()
                
                 smd_k=float(smd_k_namd[3])*69.479*10 #convert pN/nm
            
            if re.search('TCL', line) and ('Suspending') not in line and ('Running') not in line and ('ACTIVE') not in line and ('FILE') not in line :
                F_tcl= line.split()[-1]
                force_tcl.append(float(F_tcl)*-69.479) #add force of pulling into dictionnary of information

    # if the log file does not contain TCL forces (for those who pull from both ends)
    # if len(dico["-F"]) == 0:
    #     del dico['-F']


                
    raw_df= pd.read_csv(output_file,sep=' ', header=None)

    raw_df_energies= pd.read_csv(output_energies, skipinitialspace=True, sep=' ', engine='python', header=None)

    F= np.dot(raw_df.iloc[:, [6,7,8]] ,direction)

    dico["F"]=F
    dico['timeStep']= raw_df.iloc[:, 2]
    dico['timeInS']= dico['timeStep'] * 2 * 10**-15  #two fem to seconds
    dico['displacement']=  dico['timeInS'] *10**9 *smd_vel #time in ns multiplied by velocity (nm/ns)
    
  

    print(len(dico['F']))
   # print(len(dico['-F']))
          
    
    
    df = pd.DataFrame.from_dict(dico)
    if len(force_tcl) != 0:
        df.loc[:,'-F'] = pd.Series(force_tcl)
      
      
    df.loc[:,'velocityInums'] = pd.Series(round(smd_vel,4))
    df.loc[:,'direction'] = pd.Series(direction)

    df.loc[:,'kInpNnm'] = pd.Series(smd_k)

        
    energy_parms['timeStep']= raw_df_energies.iloc[:,1]
    energy_parms['BOND']= raw_df_energies.iloc[:,2]
    energy_parms['ANGLE']= raw_df_energies.iloc[:,3]
    energy_parms['DIHED']= raw_df_energies.iloc[:,4]
    energy_parms['IMPRP']= raw_df_energies.iloc[:,5]
    energy_parms['ELECT']= raw_df_energies.iloc[:,6]
    energy_parms['VDW']= raw_df_energies.iloc[:,7]
    energy_parms['BOUNDARY']= raw_df_energies.iloc[:,8]
    energy_parms['MISC']= raw_df_energies.iloc[:,9]
    energy_parms['KINETIC']= raw_df_energies.iloc[:,10]
    energy_parms['TOTAL']= raw_df_energies.iloc[:,11]
    energy_parms['TEMP']= raw_df_energies.iloc[:,12]
    energy_parms['POTENTIAL']= raw_df_energies.iloc[:,13]
    energy_parms['TOTAL3']= raw_df_energies.iloc[:,14]
    energy_parms['TEMPAVG']= raw_df_energies.iloc[:,15]
    energy_parms['PRESSURE']= raw_df_energies.iloc[:,16]
    energy_parms['GPRESSURE']= raw_df_energies.iloc[:,17]
    energy_parms['VOLUME']= raw_df_energies.iloc[:,18]
    energy_parms['PRESSAVG']= raw_df_energies.iloc[:,19]
    energy_parms['GPRESSAVG']= raw_df_energies.iloc[:,20]

    energy_parms['timeInS']= energy_parms['timeStep'] * 2* 10**-15


    df_eparms = pd.DataFrame.from_dict(energy_parms)

   
    return df_eparms, df


# def extract_info_from_smd_log (input_file):
#     """
#     This function extracts information from the SMD log file of simulation

#     Parameters
#     ----------
#     input_file : log file  
#         The output log file of SMD simulation.

#     Returns
#     -------
#     - df_energy_parms: data frame of energy parameters
#     - df : data frame of all other information(velocity, direction of pulling, force, timeSteps, timeInSeconds...etc)
    
#     """
#     #open input file of intrest
#     log_file=open(input_file, 'r')   
    
#     #get all lines of the file , puts them on a list (only way to get info later )
#     lines=log_file.readlines()  
    
    
#     #These lists contain few elements that need to be converted later: using list is necessary here to split later
#     xyz=[]       #list of direction (cste) (will containt only a vector of direction (x,y,z))
#     smd_k_namd=[]       #list of cste K(spring constant, 1 element)
#     velocity=[]    #list of velocity (cste) (one element: vector of direction)
#     ts_currentpos_force=[]   #list of timeStep, current position (x,y,z) , force (x,y,z)
#     TS=[]   #time step

#     #dico of all information that will be exported to file later
#     d={'timeStep':[], 'timeInS':[], 'displacement':[], 'F':[], '-F':[]}
#     energy_parms={'timeStep':[],'timeInS':[], 'BOND':[],'ANGLE':[], 'DIHED':[],  'IMPRP':[], 'ELECT':[], 'VDW':[],
#   'BOUNDARY':[],'MISC':[], 'KINETIC':[], 'TOTAL':[], 'TEMP':[], 'POTENTIAL':[],  'TOTAL3':[], 
#   'TEMPAVG':[], 'PRESSURE':[], 'GPRESSURE':[], 'VOLUME':[], 'PRESSAVG':[], 'GPRESSAVG':[]} #dictionnary of all info

#     #dico of constants
#     d_const={}
    
#     #loop line by line over all lines
#     for line in lines:
#         line=line.replace('\n','') 

#         if 'SMD DIRECTION' in line:
#             xyz=line.split()    #vector of direction
#             #put vector in dictionnary of constants: it will be exported to csv, direction is constant (x,y,z)
#             d_const['direction']=[(float(xyz[3]),float(xyz[4]),float(xyz[5]))] 
            
#             #put direction in a matrix : this will be used later to compute force of pulling
#             direction=np.array([float(xyz[3]),float(xyz[4]),float(xyz[5])])
#             #reshape matrix to be able to do the dot product later
#             direction.reshape(3,1)

#         #extract spring constant and convert it 
#         if 'SMD K' in line and 'K2' not in line :
#             smd_k_namd=line.split()
            
#             smd_k=float(smd_k_namd[3])*69.479*10 #convert pN/nm
#             #put constant of spring in dictionnary
#             d_const['kInpNnm']=[round(smd_k,4)]
        
#         #extract velocity
#         if 'SMD VELOCITY' in line :
#             velocity=line.split()
#             smd_velocity=float(velocity[3])   #smd velocity
#             deltatime=2
#             #convert velocity
#             smd_vel=smd_velocity/ deltatime *10**15*10**-4  # in um/s
#             #put velocity in dico
#             d_const['velocityInums']=[round(smd_vel,4)]

#         #get all lines that contain SMD info
        
#         if line.startswith('SMD') and ('SMDTITLE') not in line:
#             ts_currentpos_force=line.split()
#             ts_currentpos_force.remove('SMD')
#             t='\t'.join(ts_currentpos_force)
#             mat_ts_currentpos_force=np.genfromtxt(StringIO(t), delimiter = '\t',dtype=float) #necessary for dot product computing later 
#             d['timeStep'].append(mat_ts_currentpos_force[0]) #put time step in dictionnary
#             #compute force of pulling using dot product
#             F=np.dot(mat_ts_currentpos_force[4:7],direction) 
            
#             F=np.around(F,decimals=4)  #round values to 4 numbers 
#             d['F'].append(F) #add force of pulling into dictionnary of information
#             TS.append(mat_ts_currentpos_force[0]) #timestep : 1 TS = 2 femto second
            
            
#         #if the file contains TCL forces: pulling from both ends
#         if line.startswith('TCL') and ('Suspending') not in line and ('Running') not in line:
#             ts_currentpos_force_tcl=line.split()
#             t='\t'.join(ts_currentpos_force_tcl)
#             #put info in matrix
#             mat_ts_currentpos_force_tcl=np.genfromtxt(StringIO(t), delimiter = '\t',dtype=float) #necessary for dot product computing later 
#             #take last column that contains the force
#             #the force is in kcal/mol/angstrom**2 to convert it we need to multiply by 69.479 pN/angtrom
#             d['-F'].append(mat_ts_currentpos_force_tcl[-1]*-69.479) #add force of pulling into dictionnary of information
            
        
        
#         #get all energy values and put them into dictionnary
#         if line.startswith('ENERGY:') :
#             energy_parameters=line.split()
#             energy_parameters.remove('ENERGY:')
#             e='\t'.join(energy_parameters)
#             energy_parameters=np.genfromtxt(StringIO(e), delimiter = '\t',dtype=float)
#             energy_parms['timeStep'].append(energy_parameters[0])
#             energy_parms['BOND'].append(energy_parameters[1])
#             energy_parms['ANGLE'].append(energy_parameters[2])
#             energy_parms['DIHED'].append(energy_parameters[3])
#             energy_parms['IMPRP'].append(energy_parameters[4])
#             energy_parms['ELECT'].append(energy_parameters[5])
#             energy_parms['VDW'].append(energy_parameters[6])
#             energy_parms['BOUNDARY'].append(energy_parameters[7])
#             energy_parms['MISC'].append(energy_parameters[8])
#             energy_parms['KINETIC'].append(energy_parameters[9])
#             energy_parms['TOTAL'].append(energy_parameters[10])
#             energy_parms['TEMP'].append(energy_parameters[11])
#             energy_parms['POTENTIAL'].append(energy_parameters[12])
#             energy_parms['TOTAL3'].append(energy_parameters[13])
#             energy_parms['TEMPAVG'].append(energy_parameters[14])
#             energy_parms['PRESSURE'].append(energy_parameters[15])
#             energy_parms['GPRESSURE'].append(energy_parameters[16])
#             energy_parms['VOLUME'].append(energy_parameters[17])
#             energy_parms['PRESSAVG'].append(energy_parameters[18])
#             energy_parms['GPRESSAVG'].append(energy_parameters[19])
            
#     log_file.close()
# #conversions
#     #convert time step to seconds 
#     for i in TS:
#         d['timeInS'].append(i* deltatime *10**-15)#in seconds
#         energy_parms['timeInS'].append(i* deltatime *10**-15)#in seconds
#     #compute displacment
#     for i in d['timeInS']:
#         d['displacement'].append(i*smd_vel*10**3)

#     #if the log file does not contain TCL forces (for those who pull from both ends)
#     if len(d["-F"]) == 0:
#         del d['-F']

#     #we want data frame of columns: 'timestep' 'kInpNnm' 'timeIns' 'velocityInums' 'displacementInum' 'ForceInpN'
#     #creation of data frame df of all SMD info
#     df = pd.DataFrame.from_dict(d)
#     df.loc[:,'velocityInums'] = pd.Series(d_const['velocityInums'])
#     df.loc[:,'direction'] = pd.Series(d_const['direction'])
#     df.loc[:,'kInpNnm'] = pd.Series(d_const['kInpNnm'])
#     #creation of data frame of all energy info
#     # df_energy_parms=pd.DataFrame.from_dict(energy_parms) #data frame with diffrent energy parms
#     return energy_parms, df


# input_file= "/home/mesbah/smd_software/mmcdh23EC17-25_3Ca/smda/mergedlogs.log"

# log_file = "/home/mesbah/smd_software/mmcdh23EC17-25_3Ca/smda/mmcdh23EC17-25_3Ca_smd10.log"


# e, df= extract_info_from_smd_log (log_file)


# import matplotlib.pylab as plt
# plt.plot(df['timeInS'], df['F'])
# plt.plot(df['timeInS'], df['-F'])


