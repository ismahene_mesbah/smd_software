% Plot the rmsd data output by rmsd.tcl
fname = 'EC17-25_0CA';
RMSD_Table = readtable(['mmCDH23' fname '_rmsd.dat'],'Delimiter',' ');

% Extract the data for plotting
%  Convert time to nanoseconds
time = RMSD_Table.Time_fs_(:)/10^6;
rmsd = RMSD_Table.RMSD_A_(:);

% Plot the figure
F = figure();
plot(time,rmsd,'b','LineWidth',2);

F.CurrentAxes.Title.String = [fname(1:7) '\_' fname(9:11)];
F.CurrentAxes.XLabel.String = 'Time(ns)';
F.CurrentAxes.YLabel.String = 'RMSD(A)';
F.CurrentAxes.FontName = 'Times New Roman';
F.CurrentAxes.FontSize = 16;

L = legend;
L.Location = 'southeast';
L.String{1} = 'EC19-20 Backbone';
L.FontSize  = 16;

% Print the figure to eps
print([fname '_rmsdplt.eps'],'-depsc');
clear F fname L rmsd RMSD_Table time