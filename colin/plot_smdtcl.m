% Specify the systems you are loading
prefix = 'mmcdh23EC19-20_';
fnames = {%'0CA',...
          %'1CA',...
          %'2CA',...
          %'3CA',...
          'CASite1'%,...
          %'CASite2',...
          %'CASite21',...
          %'CASite31'...
          };
letter = {'c'};%'a','b'};
flag_xaxis = [true true]; % First flag is whether to plot force against time
			  % Second flag is whether to plot force against extension

nfiles= length(fnames);
nletters = length(letter);

for i=1:nfiles
	if flag_xaxis(1) == true
		F = figure;
        subplot(1,nletters,1);
	end
	if flag_xaxis(2) == true
		G = figure;
        subplot(1,nletters,1);
	end
	for j=1:nletters
		fname = fnames{i};
		% Read the SMD and TCL data
		SMDTable = readtable([prefix fname '_smd' letter{j} '.csv'],'Delimiter',' ');
		TCLTable = readtable([prefix fname '_tcl' letter{j} '.csv'],'Delimiter',' ');
		
		% Extract the data columns for plotting
		SMD   = SMDTable.Fx_pN_(:);
		%  Units of kcal/mol/A to pN and direction inversion
		TCL   = TCLTable.Fx_69_479pN_(:)*-69.479;
	
		% Plot Force vs Time		
		if flag_xaxis(1) == true
			%  Units of fs to ns. But multiply by 2 because NAMD tstep was 2 fs
			tstep = 2*SMDTable.Timestep(:)/10^6;
		
			% Plot the figure
			figure(F);
			subplot(1,nletters,j);hold on;
            plot(tstep,SMD,'b');
            plot(tstep,TCL,'r');
            axis tight
            
            %  If c plot make it transparent and do running average
            if strcmp(letter{j},'c')
                % Set transparency
                maskv = [min(tstep) min(min(SMD),min(TCL));...
                         max(tstep) min(min(SMD),min(TCL));...
                         max(tstep) max(max(SMD),max(TCL));...
                         min(tstep) max(max(SMD),max(TCL))];
                     
                mask = patch(maskv(:,1),maskv(:,2),'w');
                mask.FaceAlpha = .85;
                mask.EdgeColor = 'none';
                
                % Do the running average
                %  Find the values within +-0.5ns of current value and
                %  compute the averages, did by unique(diff(tstep))
                progress = 0:.02:1;
                pointer  = 1;
                idx_k = length(SMD):-floor(length(SMD)/1e4):1;
                for k=idx_k
                    % For c speed the average diff was 2e-5ns, so there
                    % are 50e3 such steps in 1ns so center about that
                    %  Compute the running average
                    lend = max(k-24999,1);
                    rend = min(k+24999,length(tstep));
                    ram = (lend:rend);
                    
                    SMD_avg(k) = mean(SMD(ram));
                    TCL_avg(k) = mean(TCL(ram));
                    
                    if (1-k/length(SMD)) > progress(pointer)
                        disp([num2str(progress(pointer))...
                              ' proportion complete']);
                        pointer = pointer + 1;
                    end
                        
                end
                
                % Plot the running average
                plot(tstep(fliplr(idx_k)),SMD_avg(fliplr(idx_k)),'b')
                plot(tstep(fliplr(idx_k)),TCL_avg(fliplr(idx_k)),'r')
                
            end

			% Format the figure
            L = legend;
			L.String = {'SMD','TCL'};
			L.FontSize = 16;
            
			F.CurrentAxes.XLabel.String = 'Time (ns)';
		    F.CurrentAxes.YLabel.String = 'Force (pN)';
			F.CurrentAxes.Title.String  = [fname letter{j}...
                		               ,' Pulling Forces'];

			F.CurrentAxes.Children(1).LineWidth = 2;
			F.CurrentAxes.Children(2).LineWidth = 2;
            %  Note: Legends also count as children
            %        findall(F,'type','axes') can be used to return handles
            %        to all axes objects in figure
			F.Children(2).FontSize = 22;
			F.Children(2).FontName = 'Times New Roman';
			F.CurrentAxes.LineWidth = 1;
		

        end	
		
		if  flag_xaxis(2) == true
			%  Extract Length in nm
			Length = abs(SMDTable.COMx_A_(:) - TCLTable.COMx_A_(:))/10;

		        % Plot the figure
		        figure(G);
		        subplot(1,nletters,j);hold on;
		        plot(Length,SMD,'b');
		        plot(Length,TCL,'r');
                axis tight
                
                % If subplot c plot the transparency and running average
                if strcmp(letter{j},'c')
                    % Set transparency
                    maskv = [min(Length) min(min(SMD),min(TCL));...
                             max(Length) min(min(SMD),min(TCL));...
                             max(Length) max(max(SMD),max(TCL));...
                             min(Length) max(max(SMD),max(TCL))];
                     
                    mask = patch(maskv(:,1),maskv(:,2),'w');
                    mask.FaceAlpha = .85;
                    mask.EdgeColor = 'none';
                    
                    % Plot the running average
                    plot(Length(fliplr(idx_k)),SMD_avg(fliplr(idx_k)),'b')
                    plot(Length(fliplr(idx_k)),TCL_avg(fliplr(idx_k)),'r')
                
                end

		        % Format the figure
                L = legend;
		        L.String = {'SMD','TCL'};
		        L.FontSize = 16;
                
		        G.CurrentAxes.XLabel.String = 'End-End (nm)';
		        G.CurrentAxes.YLabel.String = 'Force (pN)';
		        G.CurrentAxes.Title.String  = [fname letter{j}...
			 			      ,' Pulling Forces'];

		        G.CurrentAxes.Children(1).LineWidth = 2;
		        G.CurrentAxes.Children(2).LineWidth = 2;
		        G.Children(2).FontSize = 22;
		        G.Children(2).FontName = 'Times New Roman';
		        G.CurrentAxes.LineWidth = 1;
	        
        end
		
	end
	% Save figures for exporting
	%F.Units = 'inches';
    %G.Units = 'inches';
    
    F.PaperOrientation = 'landscape';
    G.PaperOrientation = 'landscape';
    
    F.PaperSize = [13.75 7.5];
    G.PaperSize = [13.75 7.5];
    
    %F.PaperPositionMode = 'manual';
    %G.PaperPositionMode = 'manual';
    
    F.PaperPosition = [.5 .6 13.3333 6.3264];
    G.PaperPosition = [.5 .6 13.3333 6.3264];
    
    %F.Units = 'pixels';
    %G.Units = 'pixels';
    
    print(F,[prefix fname '_time.pdf'],'-dpdf');
    print(G,[prefix fname '_EndEnd.pdf'],'-dpdf');
    saveas(F,[prefix fname '_time']);
    saveas(G,[prefix fname '_EndEnd']);
    
end
clear
