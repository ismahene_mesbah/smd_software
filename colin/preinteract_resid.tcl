# Extract data from VMD needed to determine which residues are most influential for drop
#  in force peak

##### Initialize Inputs #####
# Give ranges for protein residues checking
set resid_beg 1928
set resid_end 2143

# Get the Ca2+ ions for extracting index
set Ca [atomselect top "resname CAL"]
##############################

##### Main Body #####
# Find the maximum number of atoms in a residue
set resid_now [atomselect top "resid $resid_beg"]
set natoms [$resid_now num]
for {set i $resid_beg} {$i <= $resid_end} {incr i} {
	set resid_now [atomselect top "resid $i"]
	if { [$resid_now num] > $natoms } {
		set natoms [$resid_now num]
	   }
    }
puts [concat "Found max number of atoms in a residue ... " $natoms] 
# Open file for printing residue ids
set outfile [open Res_Atoms.csv w]

# Construct and build header
set HEADER "RESID A1"
for {set i 2} {$i <= $natoms} {incr i} {
	append HEADER " A" $i
    }
puts $outfile $HEADER
puts "Printed Header"
# Write residue indices to file
for {set i $resid_beg} {$i <= $resid_end} {incr i} {
	set resid_now [atomselect top "resid $i"]
	set atom_ids [concat R$i [$resid_now get serial]]

	set szlist [expr [llength $atom_ids] - 1]
        if {$szlist < $natoms} {
		for {set j 1} {$j <= [expr $natoms - $szlist]} {incr j} {
			append atom_ids " NaN"
	            }
	   }

	puts $outfile $atom_ids
    }
puts "Wrote residues to file"
# Write Ca2+ ions to file
set Ca_list [$Ca get serial]

for {set i 0} {$i < [$Ca num]} {incr i} {
	set Ca_ids [concat Ca$i [lindex $Ca_list $i]]
	for {set j 2} {$j <= $natoms} {incr j} {
		append Ca_ids " NaN"
            }
	puts $outfile $Ca_ids
    }
puts "Wrote Ca2+ to file"
# Close file
close $outfile
