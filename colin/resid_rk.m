% Convert Resid_Rank.csv into ranking by residue
%% Input
% Residue ranking by csv
fname = 'Resid_Rank.csv';

% Number of Calciums in simulation
n_Ca = 1;

% Note that lead with R and Ca
resid_beg = 1928; shift = resid_beg-1;
resid_end = 2143;


%% Load the csv and prepare array for manipulating
T = readtable(fname);

% For storing the residue rank among its best pair
margrk = (resid_beg:1:resid_end)';
margrk = repmat(margrk,1,2);

% Process T into R and Ca
for i=length(T.First_Resid):-1:1
    ram = T.Second_Resid{i};
    RType(i,2) = {ram(1)};
    Rid(i,2) = {ram(2:end)};
    
    ram = T.First_Resid{i};
    RType(i,1) = {ram(1)};
    Rid(i,1) = {ram(2:end)};
end

% Compute the relative ranking 
[~,relrk] = sort(T.RMS_err);
RelRk = table(relrk);
T = [T RelRk];

%% Marginalize the pairwise ranks for residues
for i=length(RType):-1:1
    R_flag(i,2) = strcmp(RType{i,2},'R');
    R_flag(i,1) = strcmp(RType{i,1},'R');
end

% Get numeric id's for residues
Rresid = NaN(size(Rid));
for i=flipud(find(R_flag))'
    [idi,idj] = ind2sub(size(R_flag),i);
    Rresid(idi,idj) = str2double(Rid{i});
end

for i=resid_beg:resid_end
    % First column
    flag_res = (Rresid(:,1) == i);
    ram = T.relrk(flag_res);
    
    % Second column
    flag_res = (Rresid(:,2) == i);
    ram = [ram;T.relrk(flag_res)]; %#ok<*AGROW>
    
    % Compute the best
    margrk(i-shift,2) = min(ram);
end

%% Marginalize the pairwise ranks for Ca2+
% My code zero-index Ca's
shift = -1;
for i=n_Ca:-1:1    
    ram = [];
    % Loop over all Ca pairs
    for j=flipud(find(~R_flag))'
        [idi,idj] = ind2sub(size(R_flag),j);
        check = Rid{idi,idj};
        check = check(end); % Assuming Ca# was how output in csv
        check = str2double(check);
        
        if check == (i+shift)
            ram = [ram;relrk(idi)];
        end    
    end
    Camargrk(i,:) = [i+shift min(ram)];
end 

%% Write the table
% Write residue table
RESID = margrk(:,1); RANK = margrk(:,2);
T = table(RESID,RANK);
writetable(T,'Resid_BstRk.csv');

% Write Ca2+ table
CA = Camargrk(:,i); RANK = Camargrk(:,2);
T = table(CA,RANK);
writetable(T,'Ca_BstRk.csv');