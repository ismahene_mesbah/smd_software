# -*- coding: utf-8 -*-


import numpy as np

def covrk(res_atom, xpos_atom, ypos_atom, zpos_atom, frc, cutoff, residues_of_interest, start_frame, end_frame):
#def covrk(res_atom, xpos_atom, ypos_atom, zpos_atom, frc, cutoff, residues_of_interest):

    """
    This routine uses trajectory, smd, and residue data
    to rank pairs of residues according to their 
    change in distance correlation with the force peak. 
    
    NOTE: This routine assumes that smd and pos_atom have
    already been adjusted to common tstep values

    Parameters
    ----------
    res_atom : Dictionary of lists
        Each key is the resid index and it maps to the atom
        indices belonging to that residue. The atom indices
        should match with the rows of pos_atom
    xpos_atom : numpy array of size natoms x ntsteps
        It stores the x-position of each atom across the dcd
        trajectory. Atoms match rows and frame tsteps match
        columns
    ypos_atom : similar as xpos_atom but for the y coords
    zpos_atom : similar as xpos_atom but for the z coords
    frc : np vector with smd forces at each tstep
    cutoff : number
        This is the threshold beyond which residues are not
        supposed to interact
    residues_of_interest : list
        A list of residue indices that specifies the residues
        to consider for the calculation.

    Returns
    -------
    corr : numpy array of size nCk(nresid, 2) x 3
        The values represent the correlation coefficient
        of distances and forces conditioned on distances
        falling below the cutoff threshold.

    """
    # Initialize
    reskeys = [key for key in res_atom.keys() if key in residues_of_interest]
    nresid = len(reskeys) 
    natoms, ntpts = np.shape(xpos_atom)
    
    # Compute center of mass trajectories for each residue
    # xcom = np.zeros((nresid, ntpts))
    # ycom = np.zeros((nresid, ntpts))
    # zcom = np.zeros((nresid, ntpts))
    
    xcom = np.zeros((nresid, end_frame-1))
    ycom = np.zeros((nresid, end_frame-1))
    zcom = np.zeros((nresid, end_frame-1))
    
    
    for k, reskey in enumerate(reskeys):
        for atom_index in res_atom[reskey]:
            xcom[k,:] += xpos_atom[atom_index, start_frame: end_frame]
            ycom[k,:] += ypos_atom[atom_index, start_frame: end_frame]
            zcom[k,:] += zpos_atom[atom_index, start_frame: end_frame]
            
            
            
            # xcom[k,:] += xpos_atom[atom_index, :]
            # ycom[k,:] += ypos_atom[atom_index, :]
            # zcom[k,:] += zpos_atom[atom_index, :]
            
            
        xcom[k,:] *= 1/len(res_atom[reskey])
        ycom[k,:] *= 1/len(res_atom[reskey])
        zcom[k,:] *= 1/len(res_atom[reskey])
    
    # Loop over all ordered residue pairs and compute the correlation coefficient
    npairs = nresid * (nresid-1) // 2
    corr = np.zeros((npairs, 3))
    pos = 0
    
    for i in range(nresid):
        for j in range(i):
            corr[pos, 0] = reskeys[i]        
            corr[pos, 1] = reskeys[j]
            
            # Compute the distance between com's across times for this pair
            dx = xcom[i, :] - xcom[j, :]
            dy = ycom[i, :] - ycom[j, :]
            dz = zcom[i, :] - zcom[j, :]
            
            d = np.sqrt(dx**2 + dy**2 + dz**2)
            
            # Enforce the cutoff so distances beyond threshold don't contribute
            d_res = d[d <= cutoff]
            frc_res = frc[start_frame-1 : end_frame-1][d <= cutoff]
           # frc_res = frc[d <= cutoff]

            # Compute the correlation coeff
            if len(d_res) != 0:
                corr[pos, 2] = np.corrcoef(d_res, frc_res)[0, 1]
            else:
                corr[pos, 2]=None
          
            # Move to the next position
            pos += 1
        
    return corr


# from prody import * 
# import matplotlib.pylab as plt
# import pandas as pd
# import parse_SMD_log as ps

# from itertools import groupby


# # log_file="/media/mesbah/ADATA HD330/simulations_tcl/wt/I27/R1/smdb/titinwbismd01.log"



# # e, df= ps.extract_info_from_smd_log(log_file)

# pdb_file="/media/mesbah/ADATA HD330/simulations_tcl/wt/I27/I27wbi.pdb"
# dcd_file= "/media/mesbah/ADATA HD330/simulations_tcl/wt/I27/R1/smdb/titinwbismd01.dcd"



# #corr = covrk(res_atom, xpos_atom, ypos_atom, zpos_atom, frc, cutoff, residues_of_interest)



# traj= Trajectory(dcd_file)



# structure = parsePDB(pdb_file) #load pdb file
# traj.link(structure)    

# traj.setCoords(structure)    



# resnames= structure.getResnames()
# res = [i[0] for i in groupby(resnames)]
# print(len(res))
     
# res_nums= structure.getResnums() 



# #set the coords of the atoms on the selection 
# coords=traj.getCoordsets()



# # # =============================================================================
# # # MY WORKING EXAMPLE
# # # =============================================================================
# xipos_atom=  coords[:, :, 0]
# ts, atoms= xipos_atom.shape

# xipos_atom= np.reshape(xipos_atom, (atoms, ts))
# #x_selection= xipos_atom[:30, :5]

# yipos_atom= coords[:, :, 1]
# #y_selection= yipos_atom[:30, :5]
# yipos_atom= np.reshape(yipos_atom, ( atoms, ts))



# zipos_atom= coords[:, :, 2]
# zipos_atom= np.reshape(zipos_atom, (atoms, ts))
# #z_selection= zipos_atom[:30, :5]


# force= df["F"]

# res_atoms={}

# ts, atoms= xipos_atom.shape

# #counter= dict((int(i), list(res_nums).count(i)) for i in list(res_nums))
# counter= np.unique(res_nums, return_counts=True)


# somme=0
# for i in range(len(counter[0])):
    
    
#     res_atoms[i]= list(range( somme,    somme+counter[1][i]))
#     somme += counter[1][i]

# #print(res_atoms)
# cov = covrk(res_atoms,
#           xipos_atom,yipos_atom,zipos_atom,
#           df['F'][0:ts],
#           13, [69, 84, 86])

# print(cov)
# # #print(calcCenter(selection))

# # =============================================================================
# # # =============================================================================
# # # # working example with Colin
# # # =============================================================================
# # xpos_atom=  np.random.rand(5, 100)
# # ypos_atom= np.random.rand(5, 100)
# # zpos_atom=  np.random.rand(5, 100)
# # 
# # f= np.random.rand(100)
# # res_atom = { 0: [0,2], 1 : [3,2,1], 2: [1, 4],  3: [1, 3], 4:[1, 2]}
# # 
# # 
# # # cov= covrk(res_atom,
# # #           xpos_atom,ypos_atom,zpos_atom,
# # #           f,
# # #           12)
# # # print(cov)
# # 
# # =============================================================================
# # =============================================================================
# # HeatMap
# # =============================================================================

# import matplotlib.pylab as plt
# import seaborn as sns
# import pandas as pd
# import parse_SMD_log as smd


# data= pd.DataFrame(cov[:100, :])
# data = data.pivot(index=0, columns=1, values=2) 
# #print(data)

# fig, ax = plt.subplots(figsize=(13,10)) 
# sns.heatmap(data, center=0, annot= True,
#             cmap="RdYlBu", 
#             linewidths=2.75, 
#             ax=ax
#           )


# ax.set_xlabel('Residues')
# ax.set_ylabel("Residues")
