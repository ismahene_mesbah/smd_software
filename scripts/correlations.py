#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  26 00:29:48 2023

@author: Ismahene Mesbah
"""

import prody as prd
import numpy as np


def calculate_distance(coord1, coord2):
    return np.linalg.norm(coord1 - coord2)

def calculate_center_of_mass_trajectory(pdb_path, dcd_path, residue_ids):
    # Load the protein structure from the PDB file
    structure = prd.parsePDB(pdb_path)

    # Select only protein atoms
    protein = structure.select('protein')
    
    residue_names = []
    for atom in protein:
        residue_names.append(atom.getResname())

    # Load the trajectory from the DCD file
    trajectory = prd.parseDCD(dcd_path)

    com_trajectories = {}

    for residue_id in residue_ids:
        # Select the residue of interest
        residue = protein.select(f'resid {residue_id}')

        # Get the atom indices of the residue
        atom_indices = residue.getIndices()

        # Calculate the center of mass trajectory
        com_trajectory = []
        for frame in trajectory:
            coords = frame.getCoords()
            residue_coords = coords[atom_indices]
            com = np.mean(residue_coords, axis=0)
            com_trajectory.append(com)

        com_trajectories[residue_id] = com_trajectory

    return com_trajectories, residue_names

def calculate_distances_between_residues(pdb_path, dcd_path, residue_ids, start_frame, end_frame):
    # Calculate the center of mass trajectory for each residue
    com_trajectories, residue_names = calculate_center_of_mass_trajectory(pdb_path, dcd_path, residue_ids)

    # Generate pairs of residues without redundancy
    residue_pairs = []
    for i in range(len(residue_ids)):
        for j in range(i+1, len(residue_ids)):
            residue_pairs.append((residue_ids[i], residue_ids[j]))

    distances = {}

    for residue_pair in residue_pairs:
        res1, res2 = residue_pair
        com_trajectory1 = com_trajectories[res1]
        com_trajectory2 = com_trajectories[res2]

        # Calculate the distance between center of mass for each frame within the specified range
        distance_trajectory = []
        for frame_num in range(start_frame-1, end_frame):
            com1 = com_trajectory1[frame_num]
            com2 = com_trajectory2[frame_num]
            distance = calculate_distance(com1, com2)
            distance_trajectory.append(distance)

        distances[residue_pair] = distance_trajectory

    return distances, residue_names





def calculate_correlations(distances, force_values, threshold):
    """
    Calculate correlations between distances and force values for pairs of residues that exceed the threshold.

    Parameters:
    distances (dictionary): Dictionary where keys are residue pairs and values are distance trajectories.
    force_values (numpy array): Array of shape (n_frames,) representing the force values.
    threshold (float): Threshold value to filter the pairs of residues.

    Returns:
    correlations (dictionary): Dictionary where keys are residue pairs that exceed the threshold and values are the correlations between distances and force values.
    """
    correlations = {}

    for pair, distance_trajectory in distances.items():
        distance_trajectory = np.array(distance_trajectory)  # Convert distance trajectory to NumPy array
        if np.any(distance_trajectory < threshold):
            min_length = min(len(distance_trajectory), len(force_values))
            distance_trajectory = distance_trajectory[:min_length]
            force_values_residue = force_values[:min_length]
            correlation = np.corrcoef(distance_trajectory, force_values_residue)[0, 1]
            correlations[pair] = correlation

    return correlations


import parse_SMD_log as prs

# log_file="/home/mesbah/Téléchargements/mmcdh23EC19-20_3CA_smd03a.log"
# pdb_file="/home/mesbah/Téléchargements/mmcdh23EC19-20_3CA_wbi_noh20.pdb"
# dcd_file="/home/mesbah/Téléchargements/mmcdh23EC19-20_3CA_smd03a_noh20.dcd"

log_file="/home/mesbah/smd_software/inputs/D1D5_1000mms_1_0-70ns.log"
pdb_file="/home/mesbah/smd_software/inputs/D1D5_eq1_dry.pdb"
dcd_file="/home/mesbah/smd_software/inputs/D1D5_1000mms_1_0-70ns_dry.dcd"


e,df =  prs.extract_info_from_smd_log(log_file)

ts= df["timeStep"]
f=df['F']


residue_ids = list(range(1 ,20)) # Example list of residue IDs





# Specify the frames of interest
start_frame = 1
end_frame = 10

# Calculate the distances between pairs of residues using their center of mass within the specified frames
distances, residue_names = calculate_distances_between_residues(pdb_file, dcd_file, residue_ids, start_frame, end_frame)

# Loop over residue pairs and print the distance at each frame
# for residue_pair, distance_trajectory in distances.items():
#     res1, res2 = residue_pair
#     print(f"Distance between Residue {res1} and Residue {res2}:")
#     for frame_num, distance in enumerate(distance_trajectory):
#         frame = start_frame + frame_num
#         print(f"Frame {frame}: Distance: {distance}")



     
n =  len(df['F']) // end_frame
window = ( 1/ n) * np.ones(n,)
frc = np.convolve(f, window, mode='valid')[::n]
#print(frc)

# Calculate the correlations between the distances and force values
correlations = calculate_correlations(distances, frc, 13)

# # Print the correlation for each residue pair
# for residue_pair, correlation in correlations.items():
#     res1, res2 = residue_pair
#     print(f"Correlation between Distance of Residue {res1} and Residue {res2} and Force Values: {correlation}")
    
    
    
    
    
    
    
#calculate distance function that takes two coordinate arrays and computes the Euclidean distance between them using np.linalg.norm(). 
#Then, I've introduced the calculate_distance_between_residues() function that iterates over pairs of residue IDs and calculates the 
#Euclidean distance between their center of mass using the calculate_distance() function. The distances are stored in a dictionary where 
# the keys are residue pairs (tuples) and the values are their corresponding Euclidean distances.
    
#correlations function:
# It takes three parameters: distances, force_values, and threshold.

# distances is a dictionary where the keys are residue pairs and the values are distance trajectories.
# force_values is a NumPy array of shape (n_frames,) representing the force values.
# threshold is a float value that serves as the threshold to filter the pairs of residues.
# It initializes an empty dictionary correlations to store the calculated correlations.

# It iterates over each pair and its corresponding distance trajectory in the distances dictionary.

# For each pair, it converts the distance trajectory to a NumPy array using np.array().

# It checks if there is any value in the distance trajectory that is less than the specified threshold using np.any(distance_trajectory < threshold).

# This condition filters out pairs that do not exceed the threshold.
# If the condition is met, it calculates the correlation between the distance trajectory and the force values.

# It takes the minimum length between the distance trajectory and the force values to ensure they have the same length.
# Then, it calculates the correlation coefficient using np.corrcoef(distance_trajectory, force_values_residue)[0, 1].
# Finally, it adds the pair and its correlation to the correlations dictionary.

# Once all pairs have been processed, it returns the correlations dictionary.


# =============================================================================
# # correlation definition in this context
# =============================================================================
# In this context, the term "correlation" refers to the statistical measure that quantifies the relationship between two variables the distances between residue pairs and the force values. It indicates the strength and direction of the linear relationship between these variables.

# The correlation coefficient, often denoted as "r," ranges from -1 to 1. A positive correlation value indicates a positive linear relationship, meaning that as one variable (distances) increases, the other variable (force values) also tends to increase. A negative correlation value indicates an inverse relationship, where as one variable increases, the other tends to decrease. A correlation value of 0 indicates no linear relationship between the variables.

# By calculating the correlation between distances and force values for pairs of residues, you can assess how closely related or dependent these variables are. This information can help identify any patterns or associations between the structural distances and the corresponding force values in the system under study.

import matplotlib.pyplot as plt
def plot_correlation_heatmap(correlations):
    # Extract the residue indices from the correlations dictionary
    residues = list(set([residue for pair in correlations.keys() for residue in pair]))
    
    # Create an empty correlation matrix
    correlation_matrix = np.zeros((len(residues), len(residues)))
    
    # Fill the correlation matrix with the correlation coefficients
    for pair, correlation in correlations.items():
        residue1, residue2 = pair
        idx1 = residues.index(residue1)
        idx2 = residues.index(residue2)
        correlation_matrix[idx1, idx2] = correlation
        correlation_matrix[idx2, idx1] = correlation
    
    # Plot the correlation matrix as a heatmap
    plt.imshow(correlation_matrix, cmap='hot', interpolation='nearest')
    plt.colorbar(label='Correlation Coefficient')
    plt.xticks(range(len(residues)), residues, rotation='vertical')
    plt.yticks(range(len(residues)), residues)
    plt.xlabel('Residue')
    plt.ylabel('Residue')
    plt.title('Correlation Heatmap')
    plt.show()

plot_correlation_heatmap(correlations)


    
    
# import seaborn as sns


# # Create an empty correlation matrix with NaN values
# correlation_matrix = np.empty((len(residue_names), len(residue_names)))
# correlation_matrix[:] = np.nan

# # Populate the correlation matrix with available correlation values
# for i in range(len(residue_names)):
#     for j in range(len(residue_names)):
#         residue_i = residue_names[i]
#         residue_j = residue_names[j]

#         if (residue_i, residue_j) in correlations:
#             correlation_matrix[i, j] = correlations[(residue_i, residue_j)]

# # Plot the heatmap with residue names as labels
# plt.figure(figsize=(10, 8))
# sns.heatmap(correlation_matrix, cmap='RdYlBu_r', annot=True, xticklabels=residue_names, yticklabels=residue_names)
# plt.xlabel('Residue')
# plt.ylabel('Residue')
# plt.title('Correlation Heatmap')
# plt.xticks(rotation=90)
# plt.yticks(rotation=0)
# plt.show()