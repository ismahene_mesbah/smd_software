# -*- coding: utf-8 -*-
"""
Created on Mon May 11 15:54:21 2020

@author: Ismahene
"""

from prody import *
import numpy as np
from parse_SMD_log import *
import matplotlib.pyplot as plt
import scipy.signal as signal


def extension_domain(dcd_file, pdb_file, log_file, coord1, coord2):
    """
    parameters:
        - DCD file of trajectory: it is the file that contains all atom positions at each time step
        - PDB file of structure: downloadale from PDB (Protein Data Bank)
        - Log file of SMD simulation
        - coord1 (string parameter) is the start coordinate of the selection (e.g. start of domain )
        - coord2 (string parameter) is the end coordinate of the selection (e.g end of domain)
        
    returns:
        This function returns 3 parameters:
            - e2e (type: numpy array) : extension from element to element in nanometer.
            - ts: time in seconds
            -f: force in pN
    """
    
    #extract force and time in seconds from log file
    e,df=extract_info_from_smd_log(log_file) 
    f=df['F']
    ts=df['timeInS']

    #load dcd file of trajectory
    traj = Trajectory(dcd_file) 

    #load the structure
    structure = parsePDB(pdb_file) #load pdb file

    #initialize the extension matrix
    e2e = np.zeros(traj.numFrames()) 
    
    #link pdb file of structure to dcd
    traj.link(structure)     
    
     #set coordinates of pdb to dcd
    traj.setCoords(structure)    
    
    #get first coord of residue
    nter = structure.select('name CA and resnum '+coord1)   
    #get seconds coord of residue
    cter = structure.select('name CA and resnum '+coord2) 

    #loop over frames from dcd
    for i, frame in enumerate(traj): 
        e2e[i] = calcDistance(nter, cter) #compute euclidean distance between residues 
    
    return e2e*10**-1, ts, f  #return : extension in nm 



def compute_derivative(e2e, ts):

    """"
    parameters:
        -  e2e (matrix) : extension from element to element in nanometer
        - ts (matrix): time in seconds
    
    
    returns derivative of extension (from element2element) and x-axis (time in seconds)
    """
    np.seterr(divide='ignore')
    factor=1
    l=len(e2e)
    ts=ts[0:l]
    derivative = np.zeros(l) 
    h=0.001

    for i in range(2,l-2):

        derivative[i]=(float(1*e2e[i-2]-8*e2e[i-1]+0*e2e[i+0]+8*e2e[i+1]-1*e2e[i+2])/float(1*12*h))
    x = (ts[:-1] + ts[1:]) / 2
    return x, derivative, ts, factor #returns :x-axis of derivative (timeinseconds), the derivative, original time in seconds



def get_heights_peaks(derivative):
    
    """
    Parameters
    ----------
    derivative : numpy.array
        The derivative of extension

    Returns
    -------
    heights : numpy.array
        Heights of all peaks of the derivative of extension
    """

    selected_peaks=[]
    index_=[]

    for i in range(len(derivative)):
        peaks, properties=signal.find_peaks(derivative[i], height=0)
    heights=sorted(list(properties['peak_heights']))

    return heights