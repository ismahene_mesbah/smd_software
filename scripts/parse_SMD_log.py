# -*- coding: utf-8 -*-
"""
Created on Fri Apr 10 19:18:26 2020

@author: Ismahene
"""
import os
from io import StringIO
import numpy as np
import csv
import pandas as pd

def extract_info_from_smd_log(input_file):
    """
    This function extracts information from the SMD log file of simulation

    Parameters
    ----------
    input_file : log file  
        The output log file of SMD simulation.

    Returns
    -------
    - df_energy_parms: data frame of energy parameters
    - df : data frame of all other information(velocity, direction of pulling, force, timeSteps, timeInSeconds...etc)
    
    """
    #open input file of intrest
    log_file=open(input_file, 'r')   
    
    #get all lines of the file , puts them on a list (only way to get info later )
    lines=log_file.readlines()  
    
    
    #These lists contain few elements that need to be converted later: using list is necessary here to split later
    xyz=[]       #list of direction (cste) (will containt only a vector of direction (x,y,z))
    smd_k_namd=[]       #list of cste K(spring constant, 1 element)
    velocity=[]    #list of velocity (cste) (one element: vector of direction)
    ts_currentpos_force=[]   #list of timeStep, current position (x,y,z) , force (x,y,z)
    TS=[]   #time step

    #dico of all information that will be exported to file later
    d={'timeStep':[], 'timeInS':[], 'displacement':[], 'F':[]}
    energy_parms={'timeStep':[],'timeInS':[], 'BOND':[],'ANGLE':[], 'DIHED':[],  'IMPRP':[], 'ELECT':[], 'VDW':[],
  'BOUNDARY':[],'MISC':[], 'KINETIC':[], 'TOTAL':[], 'TEMP':[], 'POTENTIAL':[],  'TOTAL3':[], 
  'TEMPAVG':[], 'PRESSURE':[], 'GPRESSURE':[], 'VOLUME':[], 'PRESSAVG':[], 'GPRESSAVG':[]} #dictionnary of all info

    #dico of constants
    d_const={}
    
    #loop line by line over all lines
    for line in lines:
        line=line.replace('\n','') 

        if 'SMD DIRECTION' in line:
            xyz=line.split()    #vector of direction
            #put vector in dictionnary of constants: it will be exported to csv, direction is constant (x,y,z)
            d_const['direction']=[(float(xyz[3]),float(xyz[4]),float(xyz[5]))] 
            
            #put direction in a matrix : this will be used later to compute force of pulling
            direction=np.array([float(xyz[3]),float(xyz[4]),float(xyz[5])])
            #reshape matrix to be able to do the dot product later
            direction.reshape(3,1)

        #extract spring constant and convert it 
        if 'SMD K' in line and 'K2' not in line :
            smd_k_namd=line.split()
            
            smd_k=float(smd_k_namd[3])*69.479*10 #convert pN/nm
            #put constant of spring in dictionnary
            d_const['kInpNnm']=[round(smd_k,4)]
        
        #extract velocity
        if 'SMD VELOCITY' in line :
            velocity=line.split()
            smd_velocity=float(velocity[3])   #smd velocity
            deltatime=2
            #convert velocity
            smd_vel=smd_velocity/ deltatime *10**15*10**-4  # in um/s
            #put velocity in dico
            d_const['velocityInums']=[round(smd_vel,4)]

        #get all lines that contain SMD info
        
        if line.startswith('SMD') and ('SMDTITLE') not in line:
            ts_currentpos_force=line.split()
            ts_currentpos_force.remove('SMD')
            t='\t'.join(ts_currentpos_force)
            mat_ts_currentpos_force=np.genfromtxt(StringIO(t), delimiter = '\t',dtype=float) #necessary for dot product computing later 
            d['timeStep'].append(mat_ts_currentpos_force[0]) #put time step in dictionnary
            #compute force of pulling using dot product
            F=np.dot(mat_ts_currentpos_force[4:7],direction) 
            F=np.around(F,decimals=4)  #round values to 4 numbers 
            d['F'].append(F) #add force of pulling into dictionnary of information
            TS.append(mat_ts_currentpos_force[0]) #timestep : 1 TS = 2 femto second
        
        #get all energy values and put them into dictionnary
        if line.startswith('ENERGY:') :
            energy_parameters=line.split()
            energy_parameters.remove('ENERGY:')
            e='\t'.join(energy_parameters)
            energy_parameters=np.genfromtxt(StringIO(e), delimiter = '\t',dtype=float)
            energy_parms['timeStep'].append(energy_parameters[0])
            energy_parms['BOND'].append(energy_parameters[1])
            energy_parms['ANGLE'].append(energy_parameters[2])
            energy_parms['DIHED'].append(energy_parameters[3])
            energy_parms['IMPRP'].append(energy_parameters[4])
            energy_parms['ELECT'].append(energy_parameters[5])
            energy_parms['VDW'].append(energy_parameters[6])
            energy_parms['BOUNDARY'].append(energy_parameters[7])
            energy_parms['MISC'].append(energy_parameters[8])
            energy_parms['KINETIC'].append(energy_parameters[9])
            energy_parms['TOTAL'].append(energy_parameters[10])
            energy_parms['TEMP'].append(energy_parameters[11])
            energy_parms['POTENTIAL'].append(energy_parameters[12])
            energy_parms['TOTAL3'].append(energy_parameters[13])
            energy_parms['TEMPAVG'].append(energy_parameters[14])
            energy_parms['PRESSURE'].append(energy_parameters[15])
            energy_parms['GPRESSURE'].append(energy_parameters[16])
            energy_parms['VOLUME'].append(energy_parameters[17])
            energy_parms['PRESSAVG'].append(energy_parameters[18])
            energy_parms['GPRESSAVG'].append(energy_parameters[19])
            
    log_file.close()
#conversions
    #convert time step to seconds 
    for i in TS:
        d['timeInS'].append(i* deltatime *10**-15)#in seconds
        energy_parms['timeInS'].append(i* deltatime *10**-15)#in seconds
    #compute displacment
    for i in d['timeInS']:
        d['displacement'].append(i*smd_vel)

    #we want data frame of columns: 'timestep' 'kInpNnm' 'timeIns' 'velocityInums' 'displacementInum' 'ForceInpN'
    #creation of data frame df of all SMD info
    df = pd.DataFrame.from_dict(d)
    df.loc[:,'velocityInums'] = pd.Series(d_const['velocityInums'])
    df.loc[:,'direction'] = pd.Series(d_const['direction'])
    df.loc[:,'kInpNnm'] = pd.Series(d_const['kInpNnm'])
    #creation of data frame of all energy info
    df_energy_parms=pd.DataFrame.from_dict(energy_parms) #data frame with diffrent energy parms
    return df_energy_parms, df  