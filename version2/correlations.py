#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May  26 00:29:48 2023

@author: Ismahene Mesbah
"""

import prody as prd
import numpy as np


def calculate_distance(coord1, coord2):
    """
    

    Parameters
    ----------
    coord1 : numpy matrix of  x , y , z coordinates of first residue
        DESCRIPTION.
    coord2 : numpy matrix of coordinates of second residue
        DESCRIPTION.

    Returns
    -------
    TYPE
        matrix numpy of eucleadian distance between the two residues

    """
    #Eucleadian distance between two coordinates of two atoms
    return np.linalg.norm(coord1 - coord2)

def calculate_center_of_mass_trajectory(pdb_path, dcd_path, residue_ids):
    """
    

    Parameters
    ----------
    pdb_path : str
        path to pdb
    dcd_path : str
        path to dcd
    residue_ids : list
        residue ids

    Returns
    -------
    com_trajectories : list 
        center of mass of all residues at all frames
    residue_names : list
        list of the names of all residues

    """
    # Load the protein structure from the PDB file
    structure = prd.parsePDB(pdb_path)

    # Select only PROTEIN atoms, NO IONS , NO WATER
    protein = structure.select('protein')
    calcium = structure.select('resname CAL')
    
    protein= protein + calcium
   # print(protein.getResindices())

 #   CA= structure.select("name CA")
  #  protein= protein  + CA
  #  protein = structure.select('protein or name CA') #to try later to see if also calcium is sleected

    
    residue_names = []
    #store residue names : useful for x-y labels later
    for atom in protein:
        residue_names.append(atom.getResname())

    # Load the trajectory from the DCD file
    trajectory = prd.parseDCD(dcd_path)
    
    com_trajectories = {}
    #loop over residues
    for residue_id in residue_ids:
        # Select the residue of interest
        residue = protein.select(f'resid {residue_id} or resname CAL')
       # residue = protein.select(f'resid {residue_id}')
        


        # Get the atom indices of the residue
        atom_indices = residue.getIndices()

        # Calculate the center of mass trajectory
        com_trajectory = []
        # loop over frame (trajectory)
        for frame in trajectory:
            #get the coordinates at each frame
            coords = frame.getCoords()
            #store the coords of all the atoms each residue
            residue_coords = coords[atom_indices]
            #caculate the center of mass of the residue
            com = np.mean(residue_coords, axis=0)
            #append the com of this residue at all frames in com_trajectory list
            com_trajectory.append(com)
        # center of mass of all residues at all frames (list of list)
        com_trajectories[residue_id] = com_trajectory

    return com_trajectories, residue_names

def calculate_distances_between_residues(pdb_path, dcd_path, residue_ids, start_frame, end_frame):
    """
    

    Parameters
    ----------
    pdb_path : str
        path to pdb
    dcd_path :  str
        path to dcd
    residue_ids : list
        list of residue ids
    start_frame : int
        index of frame at which you want to start calculation
    end_frame : int
        index of frame at which you want to end calculation

    Returns
    -------
    distances : dctionary of distances between pairs of residues
        key is tuple: pair of residue
        value is distance in angstrom
    residue_names : list
        list of residue names

    """
    
    
    # Calculate the center of mass trajectory for each residue
    com_trajectories, residue_names = calculate_center_of_mass_trajectory(pdb_path, dcd_path, residue_ids)

    # Generate pairs of residues
    residue_pairs = []
    for i in range(len(residue_ids)):
        for j in range(i+1, len(residue_ids)):
            #store pairs in list: residue i and residue i-1
            residue_pairs.append((residue_ids[i], residue_ids[j]))
    #init dict of distances 
    distances = {}
    # loop over resdiue pairs
    for residue_pair in residue_pairs:
        # get number (id) of res1 and res2 from tuple
        res1, res2 = residue_pair
        # get center of mass of residue 1
        com_trajectory1 = com_trajectories[res1]
        #get center of mass of residue 2
        com_trajectory2 = com_trajectories[res2]

        # Calculate the distance between center of mass for each frame within the specified range
        distance_trajectory = []
        # loop over selected frames
        for frame_num in range(start_frame, end_frame):
            #get center of mass at that specefic frame for both residues
            com1 = com_trajectory1[frame_num]
            com2 = com_trajectory2[frame_num]
            #calculate eucleadian distance between the two centers of mass
            distance = calculate_distance(com1, com2)
            #store the found distance at each frame in distance_trajectory list
            distance_trajectory.append(distance)

        #fill the dict in the following way: key is pair of residues (tuple) and the distances at each frame for that pair
        distances[residue_pair] = distance_trajectory

    return distances, residue_names



def calculate_correlations(distances, force_values, threshold):
    """
    Calculate correlations between distances and force values for pairs of residues that exceed the threshold.

    Parameters:
    distances (dictionary): Dictionary where keys are residue pairs and values are distance trajectories.
    force_values (numpy array): Array of shape (n_frames,) representing the force values.
    threshold (float): Threshold value to filter the pairs of residues.

    Returns:
    correlations (dictionary): Dictionary where keys are residue pairs that exceed the threshold and values are the correlations between distances and force values.
    """
    #init dict of correlations
    correlations = {}
    #loop over distances  dict  : pairs of residues are tuples and value are distances
    for pair, distance_trajectory in distances.items():
        distance_trajectory = np.array(distance_trajectory)  #  distance trajectory to NumPy array
        
       # print(distances[(1928, 1930)][0:20]) #distances between  1928 and 1930 from 1st to 20
        
        #discard distances that  do not respect threshold
        if np.any(distance_trajectory < threshold):
            
        #    print(min(len(distance_trajectory),len(force_values) ))
            #vector of force that we extract from log contains all the data
            #we want the vectors to have the same size before correlation; 
            min_length = min(len(distance_trajectory), len(force_values))
            distance_trajectory = distance_trajectory[:min_length]
            force_values_residue = force_values[:min_length]
            
            
            #calculate correlations using np.corrcoef 
            # The function will return a symmetric matrix of correlation coefficients, 
            #with the diagonal elements being 1 (since each variable is perfectly correlated with itself).
            
            correlation = np.corrcoef(distance_trajectory, force_values_residue)[0, 1] #[0,1 ] to avoid getting the 1 of perfect correlation of viariable with itself
        
         #   print(np.corrcoef(distance_trajectory, force_values_residue)[0,1])
            correlations[pair] = correlation

    return correlations



import subprocess
def launch_vmd(pdb_path, dcd_path):
    try:
        subprocess.run(["vmd", "-pdb", pdb_path, "-dcd", dcd_path])
        print("VMD launched successfully!")
    except FileNotFoundError:
        print("VMD executable not found. Make sure it is installed and in the system's PATH.")
# # Specify the paths to your DCD and PDB files
# pdb_file="/home/mesbah/Téléchargements/mmcdh23EC19-20_3CA_wbi_noh20.pdb"
# dcd_file="/home/mesbah/Téléchargements/mmcdh23EC19-20_3CA_smd03a_noh20.dcd"



# launch_vmd(dcd_file, pdb_file)






# import parse_SMD_log as prs

# log_file="/home/mesbah/Téléchargements/mmcdh23EC19-20_3CA_smd03a.log"
# pdb_file="/home/mesbah/Téléchargements/mmcdh23EC19-20_3CA_wbi_noh20.pdb"
# dcd_file="/home/mesbah/Téléchargements/mmcdh23EC19-20_3CA_smd03a_noh20.dcd"

# # # log_file="/home/mesbah/smd_software/inputs/D1D5_1000mms_1_0-70ns.log"
# # # pdb_file="/home/mesbah/smd_software/inputs/D1D5_eq1_dry.pdb"
# # # dcd_file="/home/mesbah/smd_software/inputs/D1D5_1000mms_1_0-70ns_dry.dcd"


# e,df =  prs.extract_info_from_smd_log(log_file)

# ts= df["timeStep"]
# f=df['F']


# residue_ids = list(range(1928 ,1940)) # Example list of residue IDs




# # Specify the frames of interest
# start_frame = 1
# end_frame = 10



# structure = prd.parsePDB(pdb_file)
# protein = structure.select('protein')
# CA = structure.select('element CA')

# calcium_residue_ids = np.unique(CA.getResindices())


# # Calculate the distances between pairs of residues using their center of mass within the specified frames
# distances, residue_names = calculate_distances_between_residues(pdb_file, dcd_file, residue_ids, start_frame, end_frame)
# # # Calculate the correlations between the distances and force values
# correlations = calculate_correlations(distances, df['F'], 13)

#     # Load the protein structure from the PDB file
    
    


# import matplotlib.pyplot as plt
# plt.hist(df['F'], edgecolor='black', bins=20)




# #Loop over residue pairs and print the distance at each frame
# for residue_pair, distance_trajectory in distances.items():
#     res1, res2 = residue_pair
#     print(f"Distance between Residue {res1} and Residue {res2}:")
#     for frame_num, distance in enumerate(distance_trajectory):
#         frame = start_frame + frame_num
#         print(f"Frame {frame}: Distance: {distance}")



     
# n =  len(df['F']) // end_frame
# window = ( 1/ n) * np.ones(n,)
# frc = np.convolve(f, window, mode='valid')[::n]

# n_frames=200 #TS[-1]/DCD_frq= total number of frames


#print(frc)

# Calculate the correlations between the distances and force values
#correlations = calculate_correlations(distances, frc, 13)

# # Print the correlation for each residue pair
# for residue_pair, correlation in correlations.items():
#     res1, res2 = residue_pair
#     print(f"Correlation between Distance of Residue {res1} and Residue {res2} and Force Values: {correlation}")
    
    
    
    
    
    
    
#calculate distance function that takes two coordinate arrays and computes the Euclidean distance between them using np.linalg.norm(). 
#Then, I've introduced the calculate_distance_between_residues() function that iterates over pairs of residue IDs and calculates the 
#Euclidean distance between their center of mass using the calculate_distance() function. The distances are stored in a dictionary where 
# the keys are residue pairs (tuples) and the values are their corresponding Euclidean distances.
    
#correlations function:
# It takes three parameters: distances, force_values, and threshold.

# distances is a dictionary where the keys are residue pairs and the values are distance trajectories.
# force_values is a NumPy array of shape (n_frames,) representing the force values.
# threshold is a float value that serves as the threshold to filter the pairs of residues.
# It initializes an empty dictionary correlations to store the calculated correlations.

# It iterates over each pair and its corresponding distance trajectory in the distances dictionary.

# For each pair, it converts the distance trajectory to a NumPy array using np.array().

# It checks if there is any value in the distance trajectory that is less than the specified threshold using np.any(distance_trajectory < threshold).

# This condition filters out pairs that do not exceed the threshold.
# If the condition is met, it calculates the correlation between the distance trajectory and the force values.

# It takes the minimum length between the distance trajectory and the force values to ensure they have the same length.
# Then, it calculates the correlation coefficient using np.corrcoef(distance_trajectory, force_values_residue)[0, 1].
# Finally, it adds the pair and its correlation to the correlations dictionary.

# Once all pairs have been processed, it returns the correlations dictionary.


# =============================================================================
# # correlation definition in this context
# =============================================================================
# In this context, the term "correlation" refers to the statistical measure that quantifies the relationship between two variables the distances between residue pairs and the force values. It indicates the strength and direction of the linear relationship between these variables.

# The correlation coefficient, often denoted as "r," ranges from -1 to 1. A positive correlation value indicates a positive linear relationship, meaning that as one variable (distances) increases, the other variable (force values) also tends to increase. A negative correlation value indicates an inverse relationship, where as one variable increases, the other tends to decrease. A correlation value of 0 indicates no linear relationship between the variables.

# By calculating the correlation between distances and force values for pairs of residues, you can assess how closely related or dependent these variables are. This information can help identify any patterns or associations between the structural distances and the corresponding force values in the system under study.

import matplotlib.pyplot as plt
def plot_correlation_heatmap(correlations):
    # Extract the residue indices from the correlations dictionary
    residues = sorted(list(set([residue for pair in correlations.keys() for residue in pair])))
    
    # Create an empty correlation matrix
    correlation_matrix = np.zeros((len(residues), len(residues)))
    
    # Fill the correlation matrix with the correlation coefficients
    for pair, correlation in correlations.items():
        residue1, residue2 = pair
        idx1 = residues.index(residue1)
        idx2 = residues.index(residue2)
        correlation_matrix[idx1, idx2] = correlation
        correlation_matrix[idx2, idx1] = correlation
    return correlation_matrix, residues
    

import numpy as np


def plot_correlation_heatmap_multiple(correlations1, correlations2={}):
    # Extract the residue indices from the correlations dictionaries
    residues1 = sorted(list(set([residue for pair in correlations1.keys() for residue in pair])))
    residues2 = sorted(list(set([residue for pair in correlations2.keys() for residue in pair])))

    # Combine the residue indices from both matrices
    residues = sorted(list(set(residues1 + residues2)))

    # Create empty correlation matrix
    correlation_matrix = []

    # Fill the correlation matrix with the correlation coefficients
    for i, residue1 in enumerate(residues):
        row = []
        for j, residue2 in enumerate(residues):
            pair = (residue1, residue2)
            pair_symmetric = (residue2, residue1)

            if i == j:
                row.append(1.0)  # Set diagonal elements to 1.0
            elif i < j:
                if pair in correlations1:
                    row.append(correlations1[pair])
                elif pair in correlations2:
                    row.append(correlations2[pair])
                else:
                    row.append(0.0)
            else:
                # Fill the bottom side of the matrix with symmetric values
                if pair_symmetric in correlations1:
                    row.append(correlations1[pair_symmetric])
                elif pair_symmetric in correlations2:
                    row.append(correlations2[pair_symmetric])
                else:
                    row.append(0.0)
        correlation_matrix.append(row)

    # Convert correlation matrix to numpy array
    correlation_matrix = np.array(correlation_matrix, dtype=float)
    
    return correlation_matrix, residues





# import parse_SMD_log as prs

# log_file="/home/mesbah/Téléchargements/mmcdh23EC19-20_3CA_smd03a.log"
# pdb_file="/home/mesbah/Téléchargements/mmcdh23EC19-20_3CA_wbi_noh20.pdb"
# dcd_file="/home/mesbah/Téléchargements/mmcdh23EC19-20_3CA_smd03a_noh20.dcd"

# # # log_file="/home/mesbah/smd_software/inputs/D1D5_1000mms_1_0-70ns.log"
# # # pdb_file="/home/mesbah/smd_software/inputs/D1D5_eq1_dry.pdb"
# # # dcd_file="/home/mesbah/smd_software/inputs/D1D5_1000mms_1_0-70ns_dry.dcd"


# e,df =  prs.extract_info_from_smd_log(log_file)

# ts= df["timeStep"]
# f=df['F']


# residue_ids = list(range(1928 ,1935)) # Example list of residue IDs


# res= list(range(1950 ,1958)) 

# # Specify the frames of interest
# start_frame = 1
# end_frame = 10

# # Calculate the distances between pairs of residues using their center of mass within the specified frames
# dist1, residue_names = calculate_distances_between_residues(pdb_file, dcd_file, residue_ids, start_frame, end_frame)
# # # Calculate the correlations between the distances and force values

# cor1 = calculate_correlations(dist1, df['F'], 13)
# print("len cor1", len(cor1))



#     # Load the protein structure from the PDB file
# # Calculate the distances between pairs of residues using their center of mass within the specified frames
# dist2, residue_names = calculate_distances_between_residues(pdb_file, dcd_file, res, start_frame, end_frame)
# # # Calculate the correlations between the distances and force values

# cor2 = calculate_correlations(dist2, df['F'], 13)
# print('len cor2' , len(cor2))

# corr, residues= plot_correlation_heatmap_multiple(cor1)


# # Plot the correlation heatmap
# fig, ax = plt.subplots()
# im = ax.imshow(corr, cmap='coolwarm')

# # Set ticks and labels for both axes
# ax.set_xticks(range(len(residues)))
# ax.set_yticks(range(len(residues)))
# ax.set_xticklabels(residues, rotation=45, ha="right")
# ax.set_yticklabels(residues)

# # Add colorbar
# cbar = ax.figure.colorbar(im, ax=ax)

# # Set colorbar label
# cbar.ax.set_ylabel('Correlation')

# # Set title
# ax.set_title('Correlation Heatmap')

# # Show the plot
# plt.show()

# correlation_matrix, residues= plot_correlation_heatmap(correlations)
# # Plot the correlation matrix as a heatmap
# plt.imshow(correlation_matrix, cmap='hot', interpolation='nearest')
# plt.colorbar(label='Correlation Coefficient')

   


# plt.xticks(range(len(residues)), residues, rotation='vertical')
# plt.yticks(range(len(residues)), residues)
# plt.xlabel('Residue')
# plt.ylabel('Residue')
# plt.title('Correlation Heatmap')
# plt.show()

    # # Determine the step size for tick labels
    # step_size = max(1, len(residues) // 10)  # Adjust the denominator to change the step size

    # # Set the tick positions and labels
    # tick_positions = range(0, len(residues), step_size)
    # tick_labels = [residues[i] for i in tick_positions]

    # # Set the tick positions and labels for both x and y axes
    # ax.set_xticks(tick_positions) 
    
        
# import seaborn as sns


# # Create an empty correlation matrix with NaN values
# correlation_matrix = np.empty((len(residue_names), len(residue_names)))
# correlation_matrix[:] = np.nan

# # Populate the correlation matrix with available correlation values
# for i in range(len(residue_names)):
#     for j in range(len(residue_names)):
#         residue_i = residue_names[i]
#         residue_j = residue_names[j]

#         if (residue_i, residue_j) in correlations:
#             correlation_matrix[i, j] = correlations[(residue_i, residue_j)]

# # Plot the heatmap with residue names as labels
# plt.figure(figsize=(10, 8))
# sns.heatmap(correlation_matrix, cmap='RdYlBu_r', annot=True, xticklabels=residue_names, yticklabels=residue_names)
# plt.xlabel('Residue')
# plt.ylabel('Residue')
# plt.title('Correlation Heatmap')
# plt.xticks(rotation=90)
# plt.yticks(rotation=0)
# plt.show()