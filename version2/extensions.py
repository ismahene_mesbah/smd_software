# -*- coding: utf-8 -*-
"""
Created on Mon May 11 15:54:21 2020

@author: Ismahene
"""

from prody import *
import numpy as np
from parse_SMD_log import *
import matplotlib.pyplot as plt
import scipy.signal as signal


def extension_domain(dcd_file, structure, ts, f, coord1, coord2, **kwargs):
    """
    parameters:
        - DCD file of trajectory: it is the file that contains all atom positions at each time step
        - PDB file of structure: downloadale from PDB (Protein Data Bank)
        - Log file of SMD simulation
        - coord1 (string parameter) is the start coordinate of the selection (e.g. start of domain )
        - coord2 (string parameter) is the end coordinate of the selection (e.g end of domain)
        
    returns:
        This function returns 3 parameters:
            - e2e (type: numpy array) : extension from element to element in nanometer.
            - timeInS: time in seconds
            -f: force in pN
    """
    
    #extract force and time in seconds from log file
   # e,df=extract_info_from_smd_log(log_file) 
    
    dcd1 = kwargs.get('additional dcd file 1', None)
    dcd2 = kwargs.get('additional dcd file 2', None)
    dcd3 = kwargs.get('additional dcd file 3', None)
    dcd4 = kwargs.get('additional dcd file 4', None)

    
    # f=df['F']
    # ts=df['timeInS']

    #load dcd file of trajectory
    traj = Trajectory(dcd_file) 

    #load the structure
   # structure = parsePDB(pdb_file) #load pdb file
    
   # alpha_carbons = structure.select('calpha')
  #  print(type(str(alpha_carbons.getResnums()[0])))

    #initialize the extension matrix
    e2e = np.zeros(traj.numFrames()) 
    
    #link pdb file of structure to dcd
    traj.link(structure)    
    
    if dcd1 != None :
        traj.add(dcd1)
        
    if dcd2 !=None : 
        traj.add(dcd2)

    if dcd3 !=None : 
        traj.add(dcd3)
        
    if dcd4 !=None : 
        traj.add(dcd4)
    
     #set coordinates of pdb to dcd
    traj.setCoords(structure)    
    coords=traj.getCoordsets()

    
    
    #get first coord of residue
    nter = structure.select('name CA and resnum '+coord1)   
    #get seconds coord of residue
    cter = structure.select('name CA and resnum '+coord2) 

    #loop over frames from dcd
    for i, frame in enumerate(traj): 
        e2e[i] = calcDistance(nter, cter) #compute euclidean distance between residues 


    
    n =  len(ts) // len(e2e)
    window = ( 1/ n) * np.ones(n,)
    timeInS = np.convolve(ts, window, mode='valid')[::n]
    
        
    n =  len(f) // len(e2e)
    window = ( 1/ n) * np.ones(n,)
    force = np.convolve(f, window, mode='valid')[::n]
    
    # timeInS= []

    # for i in range(0, len(ts), int(len(ts)/len(e2e))):
    #     timeInS.append(ts[i])

    # timeInS= pd.Series(timeInS)
    
    return e2e*10**-1 , timeInS , force , coords # return extension in nm



def e2e_PDB(pdb_file):
    """
    Parameters
    ----------
    pdb_file : pdb file of structure
        

    Returns
    -------
    start : int
        start coordinate.
    end : int
       end coordinate.

    """
    #load the structure
    structure = parsePDB(pdb_file) #load pdb file
    
    alpha_carbons = structure.select('calpha')
    
   # structure=alpha_carbons
   
    
    start= str(alpha_carbons.getResnums()[0])
    
    end=  str(alpha_carbons.getResnums()[-1])


    return start, end, structure


def compute_derivative(e2e, ts):

    """"
    parameters:
        -  e2e (matrix) : extension from element to element in nanometer
        - ts (matrix): time in seconds
    
    
    returns derivative of extension (from element2element) and x-axis (time in seconds)
    """
    np.seterr(divide='ignore')
    factor=1
    l=len(e2e)
    ts=ts[0:l]
    derivative = np.zeros(l) 
    h=0.001

    for i in range(2,l-2):

        derivative[i]=(float(1*e2e[i-2]-8*e2e[i-1]+0*e2e[i+0]+8*e2e[i+1]-1*e2e[i+2])/float(1*12*h))
    x = (ts[:-1] + ts[1:]) / 2
    return x, derivative, ts, factor #returns :x-axis of derivative (timeinseconds), the derivative, original time in seconds



def get_heights_peaks(derivative):
    
    """
    Parameters
    ----------
    derivative : numpy.array
        The derivative of extension

    Returns
    -------
    heights : numpy.array
        Heights of all peaks of the derivative of extension
    """

    selected_peaks=[]
    index_=[]

    for i in range(len(derivative)):
        peaks, properties=signal.find_peaks(derivative[i], height=0)
    heights=sorted(list(properties['peak_heights']))

    return heights