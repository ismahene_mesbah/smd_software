matplotlib==3.7.0
numpy==1.23.5
pandas==1.5.3
ProDy==2.4.0
scikit_learn==1.2.1
scipy==1.10.0
seaborn==0.12.2
